import { Metadata } from "next";

export const metadata: Metadata = {
  title: {
    default: "Delivery",
    template: "%s | Shopper ",
  },
  description: "Delivery's Shopper eCommerce Website",
};

export default async function DeliveryLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <>{children}</>;
}
