import Link from "next/link";
import React from "react";
import EmptyOrderAnimate from "src/assets/json/order-empty.json";
import LoadingAnimate from "src/components/LoadingAnimate";
import { PATH } from "src/constants/path";

const MyCartEmpty = () => {
  return (
    <div className="flex flex-col gap-4 items-center">
      <LoadingAnimate path={EmptyOrderAnimate} height="300px" />
      <p className="mb-0">There are no products in your shopping cart.</p>
      <Link
        href={PATH.products}
        className="btn btn-dark min-w-[300px] text-center"
      >
        Continue shopping
      </Link>
    </div>
  );
};

export default MyCartEmpty;
