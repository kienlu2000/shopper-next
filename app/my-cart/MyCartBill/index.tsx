import { Spin } from "antd";
import classNames from "classnames";
import Link from "next/link";
import LoaderSvg from "src/assets/img/loader";
import { PATH } from "src/constants/path";
import { useAppSelector } from "src/hooks/app-hooks";
import {
  selectIsPreCheckoutLoading,
  selectPreCheckoutResponse,
} from "src/store/cart";
import { currency } from "src/utils/currency";

const MyCartBill = () => {
  const data = useAppSelector(selectPreCheckoutResponse);
  const isLoading = useAppSelector(selectIsPreCheckoutLoading);

  return (
    <div className="col-12 col-md-5 col-lg-4 offset-lg-1">
      <div className="product-card card mb-7 bg-light">
        <Spin indicator={<LoaderSvg />} spinning={isLoading}>
          <div className="card-body">
            <ul className="list-group list-group-sm list-group-flush-y list-group-flush-x">
              <li className="list-group-item d-flex">
                <span>Subtotal</span>{" "}
                <span className="ml-auto font-size-sm">
                  {currency(data.subTotal)} VND
                </span>
              </li>
              <li className="list-group-item d-flex">
                <span>Promotion</span>{" "}
                <span className="ml-auto font-size-sm">
                  - {currency(data.promotion?.discount || 0)} VND
                </span>
              </li>
              <li className="list-group-item d-flex">
                <span>Tax</span>{" "}
                <span className="ml-auto font-size-sm">
                  {currency(data.tax) || 0} VND
                </span>
              </li>
              <li className="list-group-item d-flex font-size-lg font-weight-bold">
                <span>Total</span>{" "}
                <span className="ml-auto font-size-sm">
                  {currency(data.total)} VND
                </span>
              </li>
              <li className="list-group-item font-size-sm text-center text-gray-500">
                Shipping price will be calculated at checkout *
              </li>
            </ul>
          </div>
        </Spin>
      </div>
      <Link
        className={classNames("btn btn-block btn-dark mb-2", {
          disabled: data.listItems.length <= 0,
        })}
        href={PATH.checkout}
      >
        Proceed to Checkout
      </Link>
      <Link className="btn btn-link btn-sm px-0 text-body" href={PATH.products}>
        <i className="fe fe-arrow-left mr-2" /> Continue Shopping
      </Link>
    </div>
  );
};

export default MyCartBill;
