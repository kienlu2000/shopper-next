import styled from "styled-components";

export const FormPromotionCartStyle = styled.div`
  display: flex;

  & > div {
    flex: 1;
    margin-right: 15px;
  }
  .ant-form-item {
    flex: 1;
  }
`;
