import { yupResolver } from "@hookform/resolvers/yup";
import { Form } from "antd";
import { SubmitHandler, useForm } from "react-hook-form";
import ButtonCustom from "src/components/Button";
import Field from "src/components/Field";
import { useAppDispatch, useAppSelector } from "src/hooks/app-hooks";
import {
  selectIsActionPromotionLoading,
  selectPreCheckoutResponse,
} from "src/store/cart";
import {
  addPromotionCodeAction,
  removePromotionCodeAction,
} from "src/store/cart/cart.action";
import { TypeAddPromotionForm } from "src/types/cart";
import * as yup from "yup";

import { FormPromotionCartStyle } from "./style.index";

const schema = yup.object({
  code: yup.string().trim(),
});

const MyCartPromotion = () => {
  const dispatch = useAppDispatch();

  const { promotion } = useAppSelector(selectPreCheckoutResponse);
  const isLoading = useAppSelector(selectIsActionPromotionLoading);

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypeAddPromotionForm> = async (
    data: TypeAddPromotionForm,
  ) => {
    await dispatch(
      addPromotionCodeAction({
        code: data.code as string,
      }),
    );
    reset();
  };

  const onRemovePromotion = () => {
    dispatch(removePromotionCodeAction());
  };

  return (
    <div className="row align-items-end justify-content-between mb-10 mb-md-0 max-sm:!mb-0">
      <div className="col-12 col-md-7">
        {promotion && (
          <div className="promotion-code-card mb-5">
            <div className="font-bold">{promotion.title}</div>
            <div className="text-sm">Promotion ({promotion.description})</div>
            <i className="fe fe-x close" onClick={() => onRemovePromotion()} />
          </div>
        )}
        <Form onSubmitCapture={handleSubmit(onSubmit)} className="mb-7 mb-md-0">
          <label className="font-size-sm font-weight-bold">Coupon code: </label>
          <FormPromotionCartStyle>
            <Field
              control={control}
              name="code"
              errors={errors.code?.message}
              placeholder="Enter coupon code*"
            />
            <ButtonCustom loading={isLoading}>Apply</ButtonCustom>
          </FormPromotionCartStyle>
        </Form>
      </div>
    </div>
  );
};

export default MyCartPromotion;
