"use client";

import React from "react";
import CartItem from "src/components/CartItem";
import { useAppSelector } from "src/hooks/app-hooks";
import { selectCart } from "src/store/cart";

import MyCartBill from "./MyCartBill";
import MyCartEmpty from "./MyCartEmpty";
import MyCartPromotion from "./MyCartPromotion";

const MyCartPage = () => {
  const cart = useAppSelector(selectCart);

  return (
    <section className="pt-7 pb-12">
      {cart && cart?.listItems?.length ? (
        <div className="container">
          <div className="row">
            <div className="col-12">
              <h3 className="mb-10 text-center max-sm:!mb-5">Shopping Cart</h3>
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-md-7">
              <ul className="list-group list-group-lg list-group-flush-x mb-6">
                {cart?.listItems?.map((item) => (
                  <CartItem
                    allowSelect
                    quantity={item.quantity}
                    key={item.product.id}
                    {...item.product}
                  />
                ))}
              </ul>
              <MyCartPromotion />
            </div>
            <MyCartBill />
          </div>
        </div>
      ) : (
        <MyCartEmpty />
      )}
    </section>
  );
};

export default MyCartPage;
