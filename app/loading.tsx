import React from "react";
import LoadingLayout from "src/components/LoadingLayout";

const Loading = () => {
  return <LoadingLayout />;
};

export default Loading;
