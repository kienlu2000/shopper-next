import { MetadataRoute } from "next";

export default function manifest(): MetadataRoute.Manifest {
  return {
    name: "Shopper | eCommerce Website",
    short_name: "Shopper",
    description:
      "Discover the convenience of online shopping with Shopper, where a world of products and possibilities await you at the click of a button.",
    start_url: `${process.env.DOMAIN}`,
    display: "standalone",
    background_color: "#fff",
    theme_color: "#fff",
    icons: [
      {
        src: "/favicon.ico",
        sizes: "any",
        type: "image/x-icon",
      },
    ],
  };
}
