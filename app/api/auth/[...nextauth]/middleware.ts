export { default } from "next-auth/middleware";

export const config = {
  matcher: [
    "/profile",
    "/profile/collection",
    "/profile/order",
    "/profile/address",
    "/profile/payment",
  ],
};
