import { Metadata } from "next";
import { redirect } from "next/navigation";
import { getServerSession } from "next-auth";
import React, { Suspense } from "react";
import LoadingLayout from "src/components/LoadingLayout";
import { PATH } from "src/constants/path";
import authOptions from "src/helper/authOptions";

export const metadata: Metadata = {
  title: {
    default: "Checkout",
    template: "%s | Shopper ",
  },
  description: "Checkout's Shopper eCommerce Website",
};

const LayoutCheckout = async ({ children }: { children: React.ReactNode }) => {
  const session = await getServerSession(authOptions);

  if (!session) {
    redirect(PATH.authentication);
  }

  return <Suspense fallback={<LoadingLayout />}>{children}</Suspense>;
};

export default LayoutCheckout;
