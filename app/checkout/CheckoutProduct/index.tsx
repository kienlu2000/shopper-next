import CartItem from "src/components/CartItem";
import { useAppSelector } from "src/hooks/app-hooks";
import { selectPreCheckoutResponse } from "src/store/cart";
import { currency } from "src/utils/currency";

const CheckoutProduct = () => {
  const { listItems } = useAppSelector(selectPreCheckoutResponse);

  return (
    <>
      <h6 className="mb-7">Order Items</h6>
      <hr className="mt-7 mb-0" />

      <div className="product-card">
        <div className="card-body">
          <ul className="list-group list-group-lg list-group-flush">
            {listItems?.map((item) => (
              <CartItem
                className="px-0"
                hiddenActionQuantity
                key={item.productId}
                quantity={item.quantity}
                {...item.product}
                footer={
                  <span className="text-muted">
                    x {item.quantity} = {currency(item.price)} VND
                  </span>
                }
              />
            ))}
          </ul>
        </div>
      </div>
    </>
  );
};

export default CheckoutProduct;
