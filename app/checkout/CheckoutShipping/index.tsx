import { Radio, RadioGroup } from "src/components/Radio";
import { useAppDispatch, useAppSelector } from "src/hooks/app-hooks";
import { useShippingMethod } from "src/query/cart/queryHooksCommon";
import { changeShippingMethod, selectPreCheckoutData } from "src/store/cart";
import { TypeShippingMethod } from "src/types/cart";
import { currency } from "src/utils/currency";

const CheckoutShipping = () => {
  const dispatch = useAppDispatch();

  const { data } = useShippingMethod();
  const preCheckoutData = useAppSelector(selectPreCheckoutData);

  return (
    <>
      <h6 className="mb-7">Shipping Method</h6>

      <div className="table-responsive mb-6">
        <RadioGroup
          onChange={(val) => dispatch(changeShippingMethod(val.target.value))}
          value={preCheckoutData.shippingMethod}
        >
          <table className="table table-bordered table-sm table-hover mb-0">
            <tbody>
              {data?.map((item: TypeShippingMethod) => (
                <tr key={item.code}>
                  <td className="whitespace-nowrap max-sm:whitespace-normal max-sm:!py-4 max-sm:!px-4">
                    <Radio value={item.code}>{item.title}</Radio>
                  </td>
                  <td>{item.description}</td>
                  <td>{currency(item.price)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </RadioGroup>
      </div>
    </>
  );
};

export default CheckoutShipping;
