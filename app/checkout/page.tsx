"use client";

import { yupResolver } from "@hookform/resolvers/yup";
import { useQuery } from "@tanstack/react-query";
import { Form } from "antd";
import { redirect, useRouter } from "next/navigation";
import { useEffect, useRef, useState } from "react";
import { isMobile } from "react-device-detect";
import { SubmitHandler, useForm } from "react-hook-form";
import { AddressCard } from "src/components/AddressCard";
import AddressCardLoading from "src/components/AddressCardLoading";
import AddressDrawer from "src/components/AddressDrawer";
import ButtonCustom from "src/components/Button";
import Field from "src/components/Field";
import { Radio, RadioGroup } from "src/components/Radio";
import { MESSAGE } from "src/constants/message";
import { PATH } from "src/constants/path";
import { useAppDispatch, useAppSelector } from "src/hooks/app-hooks";
import { useMutationAddAddress } from "src/mutate/address/mutationHooksCommon";
import { useMutationCheckout } from "src/mutate/checkout/mutationHooksCommon";
import { getAddressUserQuery } from "src/query/address/queryFnsCommon";
import { QueryKeysCommon } from "src/query/address/queryKeysCommon";
import {
  actionIdOrderComplete,
  selectPreCheckoutData,
  selectPreCheckoutResponse,
} from "src/store/cart";
import { TypeAddUserCheckout } from "src/types/checkout";
import { TypeAddressUser } from "src/types/user";
import { handleError } from "src/utils/handleError";
import { REGEXP } from "src/utils/RegExp";
import * as yup from "yup";

import CheckoutBill from "./CheckoutBill";
import CheckoutProduct from "./CheckoutProduct";
import CheckoutShipping from "./CheckoutShipping";

const schema = yup
  .object({
    fullName: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.SPECIAL_CHARACTER, MESSAGE.validate.specialCharacter)
      .test("emoji", MESSAGE.validate.emoji, (name: string): boolean => {
        if (!name) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(name);
      }),
    phone: yup
      .string()
      .matches(REGEXP.PHONE, MESSAGE.validate.phone)
      .required(MESSAGE.validate.required),
    email: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.EMAIL, MESSAGE.validate.email)
      .test("emoji", MESSAGE.validate.emoji, (username: string): boolean => {
        if (!username) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(username);
      }),
    province: yup.string().trim().required(MESSAGE.validate.required),
    district: yup.string().trim().required(MESSAGE.validate.required),
    address: yup.string().trim().required(MESSAGE.validate.required),
  })
  .required();

const CheckoutPage = () => {
  const noteRef = useRef("");
  const dispatch = useAppDispatch();
  const router = useRouter();

  const dataPreCheckoutResponse = useAppSelector(selectPreCheckoutResponse);
  const dataPreCheckout = useAppSelector(selectPreCheckoutData);

  const { data: dataAddressUser, isFetching } = useQuery<TypeAddressUser[]>({
    queryFn: () => getAddressUserQuery("?default=true"),
    queryKey: [QueryKeysCommon.ADDRESS_DEFAULT],
  });

  const { mutateAsync: mutateCheckout, isLoading: isLoadingCheckout } =
    useMutationCheckout();
  const { mutate: mutateAddAddress, isLoading: isLoadingAddAddressUser } =
    useMutationAddAddress();

  const [address, setAddress] = useState<TypeAddressUser>();
  const [isOpenDrawer, setIsOpenDrawer] = useState<boolean>(false);
  const [payment, setPayment] = useState<string>("money");

  const {
    handleSubmit,
    control,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypeAddUserCheckout> = async (
    data: TypeAddUserCheckout,
  ) => {
    try {
      const res = await mutateCheckout({
        listItems: dataPreCheckout.listItems,
        promotionCode: dataPreCheckout.promotionCode,
        shipping: {
          shippingMethod: dataPreCheckoutResponse.shipping.shippingMethod,
          ...data,
        },
        payment: {
          paymentMethod: payment,
        },
        note: noteRef.current,
      });
      dispatch(actionIdOrderComplete(res._id));

      router.push(PATH.orderComplete + "/" + res._id);

      if (!dataAddressUser?.[0]) {
        mutateAddAddress(data);
      }
    } catch (error: any) {
      handleError(error);
    }
  };

  useEffect(() => {
    if (dataPreCheckoutResponse.listItems.length <= 0) {
      return redirect(PATH.myCart);
    }
  }, [dataPreCheckoutResponse]);

  useEffect(() => {
    if (dataAddressUser?.[0]) {
      setAddress(dataAddressUser?.[0]);
      setValue("fullName", dataAddressUser?.[0].fullName);
      setValue("address", dataAddressUser?.[0].address);
      setValue("email", dataAddressUser?.[0].email);
      setValue("district", dataAddressUser?.[0].district);
      setValue("province", dataAddressUser?.[0].province);
      setValue("phone", dataAddressUser?.[0].phone);
    }
  }, [dataAddressUser]);

  return (
    <section className="pt-7 pb-12 max-sm:!pt-0 max-sm:!pb-8">
      <Form onSubmitCapture={handleSubmit(onSubmit)} className="container">
        <div className="row">
          <div className="col-12 text-center">
            <h3 className="mb-4">Checkout</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-md-7">
            <div className="max-w-[300px] mb-5">
              <ButtonCustom
                link={PATH.myCart}
                type="button"
                className="btn btn-outline-dark btn-xs w-full"
              >
                Return to cart
              </ButtonCustom>
            </div>
            <h6 className="mb-7">Shipping Details</h6>

            {isFetching ? (
              <AddressCardLoading />
            ) : (
              <>
                {address ? (
                  <div className="row">
                    <AddressCard
                      hiddenAction
                      action={
                        <button
                          type="button"
                          onClick={() => setIsOpenDrawer(true)}
                          className="z-50 btn btn-outline-dark btn-xs absolute top-5 right-5"
                        >
                          {isMobile ? (
                            <>Change</>
                          ) : (
                            <>Change to another address</>
                          )}
                        </button>
                      }
                      {...address}
                    />
                  </div>
                ) : (
                  <div className="row">
                    <div className="col-12">
                      <Field
                        name={"fullName"}
                        label="Full Name *"
                        placeholder="Full Name"
                        control={control}
                        errors={errors.fullName?.message}
                      />
                    </div>
                    <div className="col-12 col-md-6">
                      <Field
                        name={"phone"}
                        label="Number Phone *"
                        placeholder="Number Phone"
                        control={control}
                        errors={errors.phone?.message}
                      />
                    </div>
                    <div className="col-12 col-md-6">
                      <Field
                        name={"email"}
                        label="Address Email *"
                        placeholder="Address Email"
                        control={control}
                        errors={errors.email?.message}
                      />
                    </div>
                    <div className="col-12 col-md-6">
                      <Field
                        name={"district"}
                        label="District *"
                        placeholder="District"
                        control={control}
                        errors={errors.district?.message}
                      />
                    </div>
                    <div className="col-12 col-md-6">
                      <Field
                        name={"province"}
                        label="City *"
                        placeholder="City"
                        control={control}
                        errors={errors.province?.message}
                      />
                    </div>
                    <div className="col-12">
                      <Field
                        label="Current address *"
                        placeholder="Current address"
                        control={control}
                        errors={errors.address?.message}
                        name={"address"}
                      />
                    </div>
                  </div>
                )}
              </>
            )}

            <CheckoutShipping />
            <h6 className="mb-7">Payment</h6>
            <RadioGroup
              onChange={(value) => {
                setPayment(value.target.value);
              }}
              value={payment}
            >
              <div className="list-group list-group-sm mb-7">
                <div className="list-group-item">
                  <Radio value="card">
                    Credit Card{" "}
                    <img
                      className="ml-2"
                      src="./static/img/brands/color/cards.svg"
                      alt="..."
                    />
                  </Radio>
                </div>
                <div className="list-group-item">
                  <Radio value="money">Pay upon receipt</Radio>
                </div>
              </div>
            </RadioGroup>

            <textarea
              onChange={(ev) => (noteRef.current = ev.target.value)}
              className="form-control form-control-sm mb-9 mb-md-0 font-size-xs"
              rows={5}
              placeholder="Order Notes (optional)"
              defaultValue={""}
            />
          </div>
          <div className="col-12 col-md-5 col-lg-4 offset-lg-1">
            <CheckoutProduct />

            <CheckoutBill />

            <p className="mb-7 font-size-xs text-gray-500">
              Your personal data will be used to process your order, support
              your experience throughout this website, and for other purposes
              described in our privacy policy.
            </p>
            <ButtonCustom
              loading={isLoadingCheckout || isLoadingAddAddressUser}
              className="btn btn-block btn-dark"
            >
              Place Order
            </ButtonCustom>
          </div>
        </div>
      </Form>
      <AddressDrawer
        isOpen={isOpenDrawer}
        selected={address}
        isClose={() => setIsOpenDrawer(false)}
        onSelect={setAddress}
      />
    </section>
  );
};

export default CheckoutPage;
