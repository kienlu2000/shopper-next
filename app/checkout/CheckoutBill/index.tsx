import { Spin } from "antd";
import React from "react";
import LoaderSvg from "src/assets/img/loader";
import { useAppSelector } from "src/hooks/app-hooks";
import {
  selectIsPreCheckoutLoading,
  selectPreCheckoutResponse,
} from "src/store/cart";
import { currency } from "src/utils/currency";

const CheckoutBill = () => {
  const data = useAppSelector(selectPreCheckoutResponse);
  const isLoading = useAppSelector(selectIsPreCheckoutLoading);

  return (
    <div className="product-card card mb-9 bg-light">
      <Spin indicator={<LoaderSvg />} spinning={isLoading}>
        <div className="card-body">
          <ul className="list-group list-group-sm list-group-flush-y list-group-flush-x">
            <li className="list-group-item d-flex">
              <span>Subtotal</span>{" "}
              <span className="ml-auto font-size-sm">
                {currency(data.subTotal)} VND
              </span>
            </li>
            <li className="list-group-item d-flex">
              <span>Promotion</span>{" "}
              <span className="ml-auto font-size-sm">
                - {currency(data.promotion?.discount || 0)} VND
              </span>
            </li>
            <li className="list-group-item d-flex">
              <span>Shipping</span>{" "}
              <span className="ml-auto font-size-sm">
                {currency(data.shipping.shippingPrice)} VND
              </span>
            </li>
            <li className="list-group-item d-flex">
              <span>Tax</span>{" "}
              <span className="ml-auto font-size-sm">
                {currency(data.tax)} VND
              </span>
            </li>
            <li className="list-group-item d-flex font-size-lg font-weight-bold">
              <span>Total</span>{" "}
              <span className="ml-auto">{currency(data.total)} VND</span>
            </li>
          </ul>
        </div>
      </Spin>
    </div>
  );
};

export default CheckoutBill;
