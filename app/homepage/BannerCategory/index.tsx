import Link from "next/link";
import React from "react";
import { LIST_BANNER_CATEGORY } from "src/constants/banner-category";

const BannerCategory = () => {
  return (
    <section>
      <div
        className="row no-gutters d-block d-lg-flex flickity flickity-lg-none"
        data-flickity='{"watchCSS": true}'
      >
        {LIST_BANNER_CATEGORY.map((item) => (
          <div
            key={item.key}
            className="col-12 col-md-6 col-lg-4 d-flex flex-column bg-cover max-lg:min-w-full"
            style={{ backgroundImage: `url(${item.bgImg.src})` }}
          >
            <div
              className="card bg-dark-5 bg-hover text-white text-center"
              style={{ minHeight: "470px" }}
            >
              <div className="card-body mt-auto mb-n11 py-8">
                <h2 className="mb-0 font-weight-bolder">{item.name}</h2>
              </div>
              <div className="card-body mt-auto py-8">
                <Link className="btn btn-white stretched-link" href={item.link}>
                  {item.btnName} <i className="fe fe-arrow-right ml-2" />
                </Link>
              </div>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
};

export default BannerCategory;
