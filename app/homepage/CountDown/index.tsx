import Link from "next/link";
import ImageBGCover from "public/static/img/covers/cover-4.jpg";
import React, { useEffect, useState } from "react";
import { PATH } from "src/constants/path";
interface CountdownResult {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
}
const CountDown = () => {
  const [state, setState] = useState<CountdownResult>({
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0,
  });

  useEffect(() => {
    const interval = setInterval(() => {
      const countDownDate = new Date("Apr 1, 2024 12:00:00").getTime();
      const currentTime = new Date().getTime();
      const distance = countDownDate - currentTime;

      setState((prev) => ({
        ...prev,
        days: Math.floor(distance / (1000 * 60 * 60 * 24)),
      }));
      setState((prev) => ({
        ...prev,
        hours: Math.floor(
          (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
        ),
      }));
      setState((prev) => ({
        ...prev,
        minutes: Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
      }));
      setState((prev) => ({
        ...prev,
        seconds: Math.floor((distance % (1000 * 60)) / 1000),
      }));
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <section
      className="py-13 bg-cover max-sm:!py-10"
      style={{ backgroundImage: `url(${ImageBGCover.src})` }}
    >
      <div className="container">
        <div className="row justify-content-end">
          <div className="col-12 col-md-8 col-lg-6">
            <h3 className="mb-7">
              Giảm hơn 50% <br />
              Bộ sưu tập mùa hè
            </h3>
            <div
              className="d-flex mb-9"
              data-countdown
              data-date="Dec 5, 2023 15:37:25"
            >
              <div className="text-center">
                <div
                  className="font-size-h1 font-weight-bolder text-primary"
                  data-days
                >
                  {state.days}
                </div>
                <div className="heading-xxs text-muted">Days</div>
              </div>
              <div className="px-1 px-md-4">
                <div className="font-size-h2 font-weight-bolder text-primary">
                  :
                </div>
              </div>
              <div className="text-center">
                <div
                  className="font-size-h1 font-weight-bolder text-primary"
                  data-hours
                >
                  {state.hours}
                </div>
                <div className="heading-xxs text-muted">Hours</div>
              </div>
              <div className="px-1 px-md-4">
                <div className="font-size-h2 font-weight-bolder text-primary">
                  :
                </div>
              </div>
              <div className="text-center">
                <div
                  className="font-size-h1 font-weight-bolder text-primary"
                  data-minutes
                >
                  {state.minutes}
                </div>
                <div className="heading-xxs text-muted">Minutes</div>
              </div>
              <div className="px-1 px-md-4">
                <div className="font-size-h2 font-weight-bolder text-primary">
                  :
                </div>
              </div>
              <div className="text-center">
                <div
                  className="font-size-h1 font-weight-bolder text-primary"
                  data-seconds
                >
                  {state.seconds}
                </div>
                <div className="heading-xxs text-muted">Seconds</div>
              </div>
            </div>
            <Link className="btn btn-dark" href={PATH.products}>
              Mua ngay <i className="fe fe-arrow-right ml-2" />
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default CountDown;
