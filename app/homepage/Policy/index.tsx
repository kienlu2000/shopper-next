import React from "react";
import { LIST_POLICY } from "src/constants/Footer/list-policy";

const Policy = () => {
  return (
    <section className="pt-7">
      <div className="container">
        <div className="row pb-7 border-bottom">
          {LIST_POLICY.map((item) => (
            <div key={item.key} className="col-12 col-md-6 col-lg-3">
              <div className="d-flex mb-6 mb-lg-0">
                <i className={`${item.icon} font-size-lg text-primary`} />
                <div className="ml-6">
                  <h6 className="heading-xxs mb-1">{item.title}</h6>
                  <p className="mb-0 font-size-sm text-muted">
                    {item.describe}
                  </p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Policy;
