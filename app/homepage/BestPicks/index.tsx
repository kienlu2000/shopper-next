import Link from "next/link";
import ImageProduct_1 from "public/static/img/products/product-1.jpg";
import ImageProduct_2 from "public/static/img/products/product-2.jpg";
import ImageProduct_3 from "public/static/img/products/product-3.jpg";
import ImageProduct_4 from "public/static/img/products/product-4.jpg";
import React from "react";
import { PATH } from "src/constants/path";

const BestPicks = () => {
  return (
    <section className="pt-12 max-sm:!pt-8">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12 col-md-10 col-lg-8 col-xl-6 text-center">
            <h6 className="heading-xxs mb-3 text-gray-400">New Collection</h6>
            <h2 className="mb-4">Best Picks 2019</h2>
            <p className="mb-10 text-gray-500">
              Appear, dry there darkness they&apos;re seas, dry waters thing fly
              midst. Beast, above fly brought Very green.
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-md-5 col-lg-4 d-flex flex-column">
            <div
              className="card mb-7 text-white"
              style={{
                minHeight: "400px",
                backgroundImage: `url(${ImageProduct_1.src})`,
              }}
            >
              <div className="card-bg">
                <div
                  className="card-bg-img bg-cover"
                  style={{
                    backgroundImage: `url(${ImageProduct_1.src})`,
                  }}
                />
              </div>
              <div className="card-body my-auto text-center">
                <h4 className="mb-0">Bags Collection</h4>
                <Link
                  className="btn btn-link stretched-link text-reset"
                  href={PATH.products}
                >
                  Shop Now <i className="fe fe-arrow-right ml-2" />
                </Link>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-7 col-lg-8 d-flex flex-column">
            <div className="card mb-7 text-body" style={{ minHeight: "400px" }}>
              <div className="card-bg">
                <div
                  className="card-bg-img bg-cover"
                  style={{
                    backgroundImage: `url(${ImageProduct_2.src})`,
                  }}
                />
              </div>
              <div className="card-body my-auto px-md-10 text-center text-md-left">
                <div className="card-circle card-circle-lg card-circle-right">
                  <strong>save</strong>
                  <span className="font-size-h4 font-weight-bold">30%</span>
                </div>
                <h4 className="mb-0">Printed men’s Shirts</h4>
                <Link
                  className="btn btn-link stretched-link px-0 text-reset"
                  href={PATH.products}
                >
                  Shop Now <i className="fe fe-arrow-right ml-2" />
                </Link>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-7 col-lg-8 d-flex flex-column">
            <div
              className="card mb-7 mb-md-0 text-body"
              style={{ minHeight: "400px" }}
            >
              <div className="card-bg">
                <div
                  className="card-bg-img bg-cover"
                  style={{
                    backgroundImage: `url(${ImageProduct_3.src})`,
                  }}
                />
              </div>
              <div className="card-body my-auto px-md-10 text-center text-md-left">
                <h4 className="mb-0">Basic women’s Dresses</h4>
                <Link
                  className="btn btn-link stretched-link px-0 text-reset"
                  href={PATH.products}
                >
                  Shop Now <i className="fe fe-arrow-right ml-2" />
                </Link>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-5 col-lg-4 d-flex flex-column">
            <div className="card text-white" style={{ minHeight: "400px" }}>
              <div className="card-bg">
                <div
                  className="card-bg-img bg-cover"
                  style={{
                    backgroundImage: `url(${ImageProduct_4.src})`,
                  }}
                />
              </div>
              <div className="card-body my-auto text-center">
                <h4 className="mb-0">Sweatshirts</h4>
                <Link
                  className="btn btn-link stretched-link text-reset"
                  href={PATH.products}
                >
                  Shop Now <i className="fe fe-arrow-right ml-2" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default BestPicks;
