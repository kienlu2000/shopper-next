import Link from "next/link";
import React from "react";
import { LIST_BRAND, LIST_BRAND_PRODUCTS } from "src/constants/list-brand";

const Brand = () => {
  return (
    <section className="py-12 bg-light max-sm:!py-8">
      <div className="container">
        <div className="row">
          <div className="col-12 text-center">
            <h2 className="mb-3">@shopper</h2>
            <p className="mb-10 font-size-lg text-gray-500">
              Appear, dry there darkness they&apos;re seas, dry waters.
            </p>
          </div>
        </div>
        <div className="row mx-n1 mb-10">
          {LIST_BRAND_PRODUCTS.map((item) => (
            <div key={item.key} className="col-6 col-sm-4 col-md px-1">
              <div className="card mb-2">
                <img src={item.img} alt={item.img} className="card-img" />
                <Link
                  className="card-img-overlay card-img-overlay-hover align-items-center bg-dark-40"
                  href={item.link}
                >
                  <p className="my-0 font-size-xxs text-center text-white">
                    <i className="fe fe-heart mr-2" /> {item.like}{" "}
                    <i className="fe fe-message-square mr-2 ml-3" />
                    {item.comment}
                  </p>
                </Link>
              </div>
            </div>
          ))}
        </div>
        <div className="row">
          {LIST_BRAND.map((item) => (
            <div key={item.key} className="col-4 col-sm-3 col-md text-center">
              {/* Brand */}
              <img
                src={item.img}
                alt={item.img}
                className="img-fluid mb-7 mb-md-0"
              />
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Brand;
