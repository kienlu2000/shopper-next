import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

import { isDesktop } from "react-device-detect";
import ReviewCard from "src/components/ReviewCard";
import { ListReviewCardLoading } from "src/components/ReviewCardLoading";
import { useQueryTopReviews } from "src/query/review/queryHooksCommon";
import { TypeTopReviewItem } from "src/types/review";
import { Autoplay, Pagination } from "swiper/modules";
import { Swiper, SwiperSlide } from "swiper/react";

import { FeedBacksStyle, SwiperStyle } from "./style.index";

const FeedBacks = () => {
  const { data: dataTopReview, isLoading } = useQueryTopReviews();

  return (
    <section className="py-12 max-sm:!pt-8">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12 col-md-10 col-lg-8 col-xl-6 text-center">
            <h6 className="heading-xxs mb-3 text-gray-400">
              BUT WHAT PEOPLE WHO HAVE SHOPPED SAY ABOUT THE PRODUCT
            </h6>
            <h2 className="mb-10 max-sm:!mb-6">
              Latest reviews of top products
            </h2>
          </div>
        </div>

        {isLoading ? (
          <>
            {isDesktop ? (
              <ListReviewCardLoading item={3} />
            ) : (
              <ListReviewCardLoading item={1} />
            )}
          </>
        ) : (
          <div className="row">
            <div className="col-12">
              <SwiperStyle>
                <FeedBacksStyle>
                  <Swiper
                    breakpoints={{
                      390: {
                        slidesPerView: 1,
                      },
                      1000: {
                        slidesPerView: 3,
                      },
                    }}
                    autoplay={{
                      delay: 2500,
                      disableOnInteraction: false,
                    }}
                    loop={true}
                    modules={[Pagination, Autoplay]}
                    spaceBetween={30}
                    slidesPerView={3}
                    pagination={{ clickable: true }}
                  >
                    {dataTopReview?.map((e: TypeTopReviewItem) => (
                      <SwiperSlide key={e._id}>
                        <ReviewCard {...e} />
                      </SwiperSlide>
                    ))}
                  </Swiper>
                </FeedBacksStyle>
              </SwiperStyle>
            </div>
          </div>
        )}
      </div>
    </section>
  );
};

export default FeedBacks;
