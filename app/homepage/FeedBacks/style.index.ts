import styled from "styled-components";

export const FeedBacksStyle = styled.div`
  .swiper-pagination-bullet {
    background: #d8d8d8;
    opacity: 1;
    border-radius: 100px;
    transition: all 0.2s ease-in;
  }
  .swiper-pagination-bullet-active {
    background: #ff6f61;
    width: 50px;
    transition: all 0.2s ease-in;
  }
`;

export const SwiperStyle = styled.div`
  overflow: hidden;
  padding-bottom: 100px;
  margin-bottom: -100px;
  .swiper {
    overflow: visible;
  }
  .swiper-pagination {
    bottom: -50px;
  }
`;
