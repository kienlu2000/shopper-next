import type { TabsProps } from "antd";
import { Tabs } from "antd";
import _map from "lodash/map";
import React from "react";
import ListProductCardDiscover from "src/components/ListProductCard";

import { SellingProductStyle } from "./style.index";

const ProductSellerTabItem = {
  ELECTRON: "Electronic Device",
  HOUSEWARES: "Housewares",
  PROMOTIONAL: "Product Promotional",
};

const items: TabsProps["items"] = _map(
  [
    {
      key: ProductSellerTabItem.ELECTRON,
      label: ProductSellerTabItem.ELECTRON,
      children: (
        <ListProductCardDiscover
          query="categories=4221&limit=12"
          link="products/dien-tu-dien-lanh/4221"
        />
      ),
    },
    {
      key: ProductSellerTabItem.HOUSEWARES,
      label: ProductSellerTabItem.HOUSEWARES,
      children: (
        <ListProductCardDiscover
          query="categories=1882&limit=12"
          link="products/dien-gia-dung/1882"
        />
      ),
    },
    {
      key: ProductSellerTabItem.PROMOTIONAL,
      label: ProductSellerTabItem.PROMOTIONAL,
      children: (
        <ListProductCardDiscover
          query="sort=discount_rate.desc&limit=12"
          link="products/cua-hang?page=1&sort=discount_rate.desc"
        />
      ),
    },
  ],
  (item) => {
    return {
      ...item,
      label: <div className={"nav-link"}>{item.label}</div>,
    };
  },
);

const SellingProduct = () => (
  <SellingProductStyle className="py-12 max-sm:!py-10">
    <div className="container">
      <h2 className="mb-4 text-center">Top Product Seller</h2>
      <Tabs defaultActiveKey="1" items={items} />
    </div>
  </SellingProductStyle>
);

export default SellingProduct;
