import styled from "styled-components";

export const SellingProductStyle = styled.section`
  .ant-tabs {
    &-nav-wrap {
      justify-content: center;
    }
    &-tab-active {
      .nav-link {
        color: #ff6f61 !important;
      }
    }
    &-ink-bar {
      background-color: #ff6f61 !important;
    }
    &-tab {
      @media (max-width: 640px) {
        margin: unset !important;
        .nav-link {
          padding: 0.5rem 0.25rem;
        }
      }
    }
    @media (max-width: 640px) {
      &-nav-list {
        justify-content: space-between;
        width: 100%;
      }
    }
  }
`;
