"use client";

import { yupResolver } from "@hookform/resolvers/yup";
import { Form } from "antd";
import { useSearchParams } from "next/navigation";
import { SubmitHandler, useForm } from "react-hook-form";
import ButtonCustom from "src/components/Button";
import Field from "src/components/Field";
import { MESSAGE } from "src/constants/message";
import { useBodyClass } from "src/hooks/useBodyClass";
import { useMutationChangePasswordByCode } from "src/mutate/user/mutationHooksCommon";
import * as yup from "yup";

interface TypResetPasswordByCodeFrom {
  password: string;
  confirmPassword: string;
}

const schema = yup
  .object({
    password: yup
      .string()
      .trim()
      .min(6, MESSAGE.validate.min)
      .max(32, MESSAGE.validate.max)
      .required(MESSAGE.validate.required),
    confirmPassword: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .test(
        "empty",
        MESSAGE.validate.notMatch,
        (
          confirm_password: string | undefined,
          testContext: yup.TestContext<yup.AnyObject>,
        ): boolean => {
          if (!testContext.parent.password) return true;
          return testContext.parent.password === confirm_password;
        },
      ),
  })
  .required();

const ResetPasswordPage = () => {
  useBodyClass("bg-light");

  const searchParam = useSearchParams();

  const code = searchParam.get("code");

  const { mutate, isLoading } = useMutationChangePasswordByCode();

  const {
    handleSubmit,
    control,
    formState: { errors, isValid },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypResetPasswordByCodeFrom> = async (
    data: TypResetPasswordByCodeFrom,
  ) => {
    mutate({
      code: code as string,
      password: data.password,
    });
  };

  return (
    <section className="py-12">
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-6 offset-md-3">
            <div className="card card-lg mb-10 mb-md-0">
              <div className="card-body">
                <h6 className="mb-7">Reset Password</h6>
                <Form onSubmitCapture={handleSubmit(onSubmit)}>
                  <div className="row">
                    <div className="col-12">
                      <Field
                        control={control}
                        placeholder="Password *"
                        errors={errors.password?.message}
                        name="password"
                        type="password"
                      />
                    </div>
                    <div className="col-12">
                      <Field
                        control={control}
                        placeholder="Confirm Password *"
                        errors={errors.confirmPassword?.message}
                        name="confirmPassword"
                        type="password"
                      />
                    </div>
                    <div className="col-12">
                      <ButtonCustom disable={!isValid} loading={isLoading}>
                        Reset Password
                      </ButtonCustom>
                    </div>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ResetPasswordPage;
