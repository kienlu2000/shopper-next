import { Metadata } from "next";
import { redirect } from "next/navigation";
import { getServerSession } from "next-auth";
import React from "react";
import { PATH } from "src/constants/path";
import authOptions from "src/helper/authOptions";

export const metadata: Metadata = {
  title: {
    default: "Reset Password",
    template: "%s | Shopper ",
  },
  description: "Reset Password's Shopper eCommerce Website",
};

const ResetPasswordLayout = async ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const session = await getServerSession(authOptions);

  if (session) {
    redirect(PATH.profile.index);
  }

  return <div>{children}</div>;
};

export default ResetPasswordLayout;
