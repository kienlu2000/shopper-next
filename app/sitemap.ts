import { MetadataRoute } from "next";
import { PATH } from "src/constants/path";
import { TypeCategoriesResponse } from "src/types/categories";
import { TypeProductResponse } from "src/types/products";

export default async function sitemap(): Promise<MetadataRoute.Sitemap> {
  const responseProducts = await fetch("https://course.spacedev.vn/product");
  const responseCategories = await fetch(
    "https://course.spacedev.vn/product/categories",
  );

  const products: TypeProductResponse = await responseProducts.json();

  const categories: TypeCategoriesResponse = await responseCategories.json();

  const productEntries: MetadataRoute.Sitemap = products.data.map(
    ({ slug }) => ({
      url: `${process.env.DOMAIN}/products/${slug}`,
      priority: 1,
      lastModified: new Date(),
      changeFrequency: "daily",
    }),
  );

  const categoriesEntries: MetadataRoute.Sitemap = categories.data.map(
    ({ slug, id }) => ({
      url: `${process.env.DOMAIN}/products/${slug}/${id}`,
      priority: 1,
      lastModified: new Date(),
      changeFrequency: "daily",
    }),
  );
  return [
    {
      url: `${process.env.DOMAIN}` + PATH.products,
      lastModified: new Date(),
      changeFrequency: "hourly",
      priority: 1,
    },
    ...productEntries,
    ...categoriesEntries,
    {
      url: `${process.env.DOMAIN}` + PATH.deliveryRegulations,
      lastModified: new Date(),
      changeFrequency: "monthly",
      priority: 0.5,
    },
    {
      url: `${process.env.DOMAIN}` + PATH.FAQ,
      lastModified: new Date(),
      changeFrequency: "monthly",
      priority: 0.5,
    },
  ];
}
