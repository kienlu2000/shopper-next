import { Metadata } from "next";

export const metadata: Metadata = {
  title: {
    default: "FAQ",
    template: "%s | Shopper ",
  },
  description: "FAQ's Shopper eCommerce Website",
};

export default async function FAQLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <>{children}</>;
}
