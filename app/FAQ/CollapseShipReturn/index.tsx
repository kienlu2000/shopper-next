import { Collapse, CollapseProps } from "antd";
import _map from "lodash/map";
import React from "react";
import { CONTENT_FAQ } from "src/constants/content-faq";

import { FAQStyle } from "../style.index";

const items: CollapseProps["items"] = _map(
  [
    {
      key: "1",
      label: "1. Bring of had which their whose you're it own ?",
      children: (
        <p className="mb-0 font-size-lg text-gray-500">{CONTENT_FAQ}</p>
      ),
    },
    {
      key: "2",
      label: "2. Over shall air can't subdue fly divide him ?",
      children: (
        <p className="mb-0 font-size-lg text-gray-500">{CONTENT_FAQ}</p>
      ),
    },
    {
      key: "3",
      label: "3. Waters one you'll creeping ?",
      children: (
        <p className="mb-0 font-size-lg text-gray-500">{CONTENT_FAQ}</p>
      ),
    },
    {
      key: "4",
      label: "4. Fowl, given morning seed fruitful kind beast be ?",
      children: (
        <p className="mb-0 font-size-lg text-gray-500">{CONTENT_FAQ}</p>
      ),
    },
  ],
  (item) => {
    return {
      ...item,
      label: (
        <div className="dropdown-toggle d-block font-size-lg font-weight-bold text-reset max-sm:!whitespace-break-spaces max-sm:!flex max-sm:!items-start max-sm:!justify-between">
          {item.label}
        </div>
      ),
    };
  },
);

const CollapseShip = () => {
  return (
    <FAQStyle>
      <h5 className="mb-7">Shipping &amp; Returns:</h5>
      <ul
        className="list-group list-group-flush-x mb-9"
        id="faqCollapseParentTwo"
      >
        <Collapse size="large" accordion items={items} />
      </ul>
    </FAQStyle>
  );
};

export default CollapseShip;
