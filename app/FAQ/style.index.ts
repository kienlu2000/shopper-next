import styled from "styled-components";

export const FAQStyle = styled.div`
  .ant-collapse {
    border: unset !important;
    background-color: unset !important;
    &-header {
      padding-inline-start: 24px !important;
      padding: 18px 24px !important;
      color: rgba(0, 0, 0, 0.68) !important;
    }
    &-item {
      border-radius: unset !important;

      &:first-of-type {
        border-top: 1px solid #d9d9d9 !important;
      }
      &-active {
        .dropdown-toggle {
          &::after {
            transform: rotate(180deg);
            transition-duration: 0.45s;
          }
        }
      }
    }
    &-content {
      border-top: unset !important;
      &-box {
        padding: 0 24px 16px 24px !important;
      }
    }
    &-expand-icon {
      display: none !important;
    }
  }
`;
