"use client";

import Link from "next/link";
import { PATH } from "src/constants/path";
import { useScrollToTop } from "src/hooks/useScrollToTop";

import CollapseOrder from "./CollapseOrder";
import CollapsePayment from "./CollapsePayment";
import CollapseShip from "./CollapseShipReturn";

const FAQPage = () => {
  useScrollToTop();

  return (
    <div>
      <nav className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <ol className="breadcrumb mb-0 font-size-xs text-gray-400">
                <li className="breadcrumb-item">
                  <Link className="text-gray-400" href={PATH.home}>
                    Home
                  </Link>
                </li>
                <li className="breadcrumb-item active">FAQ</li>
              </ol>
            </div>
          </div>
        </div>
      </nav>
      <section className="pt-7 pb-12">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 col-lg-10 col-xl-8">
              <h3 className="mb-10 text-center">Frequently Asked Questions</h3>
              <CollapseOrder />
              <CollapseShip />
              <CollapsePayment />
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default FAQPage;
