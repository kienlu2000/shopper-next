import "public/static/css/fallback.css";
import "public/static/css/stylelibs.min.css";
import "src/assets/css/globals.css";

import type { Metadata } from "next";
import { Nunito_Sans } from "next/font/google";
import ErrorBoundary from "src/components/ErrorBoundary";
import Footer from "src/components/Footer";
import Header from "src/components/Header";
import NextAuthProvider from "src/components/NextAuthProvider";
import QueryProvider from "src/components/QueryProvider";
import ReduxProvider from "src/components/ReduxProvider";

const inter = Nunito_Sans({ subsets: ["latin"] });

export const metadata: Metadata = {
  metadataBase: new URL("https://e-shopper.site"),
  title: {
    default: "Shopper | eCommerce Website",
    template: "%s | Shopper ",
  },
  description:
    "Discover the convenience of online shopping with Shopper, where a world of products and possibilities await you at the click of a button.",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
      </head>
      <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#151718" />
      <meta name="msapplication-TileColor" content="#da532c" />
      <meta name="theme-color" content="#ffffff" />
      <link rel="icon" href="/static/img/logo.svg" />
      <body className={inter.className} suppressHydrationWarning={true}>
        <ErrorBoundary>
          <QueryProvider>
            <ReduxProvider>
              <NextAuthProvider>
                <Header />
                {children}
                <Footer />
              </NextAuthProvider>
            </ReduxProvider>
          </QueryProvider>
        </ErrorBoundary>
      </body>
    </html>
  );
}
