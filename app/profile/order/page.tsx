"use client";

import { Badge, Tabs, TabsProps } from "antd";
import _map from "lodash/map";
import { useMemo } from "react";
import ListOrderItem from "src/components/ListOrderItem";
import { useQueryCountOrder } from "src/query/order/queryHooksCommon";

import { LabelTabOrderStyle, OrderPageStyle } from "./style.index";

const KeyQueryOrder = {
  ALL_ORDER: "",
  PROCESSING: "pending",
  CONFIRMED: "confirm",
  SHIPPING: "shipping",
  DELIVERED: "finished",
  CANCELED: "cancel",
};

const OrderTabItems = {
  ALL_ORDER: "All Orders",
  PROCESSING: "Processing",
  CONFIRMED: "Confirmed",
  SHIPPING: "Shipping",
  DELIVERED: "Delivered",
  CANCELED: "Canceled",
};

const OrderPage = () => {
  const { data: countPending } = useQueryCountOrder(
    `?status=${KeyQueryOrder.PROCESSING}`,
  );

  const { data: countConfirm } = useQueryCountOrder(
    `?status=${KeyQueryOrder.CONFIRMED}`,
  );

  const { data: countShipping } = useQueryCountOrder(
    `?status=${KeyQueryOrder.SHIPPING}`,
  );

  const items: TabsProps["items"] = useMemo(
    () =>
      _map(
        [
          {
            key: OrderTabItems.ALL_ORDER,
            label: OrderTabItems.ALL_ORDER,
            children: <ListOrderItem status={KeyQueryOrder.ALL_ORDER} />,
          },
          {
            key: OrderTabItems.PROCESSING,
            label: (
              <LabelTabOrderStyle>
                <Badge count={countPending}>{OrderTabItems.PROCESSING}</Badge>
              </LabelTabOrderStyle>
            ),
            children: <ListOrderItem status={KeyQueryOrder.PROCESSING} />,
          },
          {
            key: OrderTabItems.CONFIRMED,
            label: (
              <LabelTabOrderStyle>
                <Badge count={countConfirm}>{OrderTabItems.CONFIRMED}</Badge>
              </LabelTabOrderStyle>
            ),
            children: <ListOrderItem status={KeyQueryOrder.CONFIRMED} />,
          },
          {
            key: OrderTabItems.SHIPPING,
            label: (
              <LabelTabOrderStyle>
                <Badge count={countShipping}>{OrderTabItems.SHIPPING}</Badge>
              </LabelTabOrderStyle>
            ),
            children: <ListOrderItem status={KeyQueryOrder.SHIPPING} />,
          },
          {
            key: OrderTabItems.DELIVERED,
            label: OrderTabItems.DELIVERED,
            children: <ListOrderItem status={KeyQueryOrder.DELIVERED} />,
          },
          {
            key: OrderTabItems.CANCELED,
            label: OrderTabItems.CANCELED,
            children: <ListOrderItem status={KeyQueryOrder.CANCELED} />,
          },
        ],
        (item) => {
          return {
            ...item,
            label: <div>{item.label}</div>,
          };
        },
      ),
    [countConfirm, countPending, countShipping],
  );
  return (
    <OrderPageStyle>
      <Tabs defaultActiveKey="all-orders" items={items} />
    </OrderPageStyle>
  );
};

export default OrderPage;
