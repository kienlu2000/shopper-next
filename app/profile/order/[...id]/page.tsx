"use client";

import moment from "moment";
import Link from "next/link";
import { useParams } from "next/navigation";
import { useMemo } from "react";
import ButtonCustom from "src/components/Button";
import OrderDetailLoading from "src/components/OrderDetailLoading";
import OrderItemStatus from "src/components/OrderItemStatus";
import ShippingOrderDetail from "src/components/ShippingOrderDetail";
import ShippingOrderTotal from "src/components/ShippingOrderTotal";
import { PATH } from "src/constants/path";
import { useShippingMethod } from "src/query/cart/queryHooksCommon";
import { useQueryOrderDetail } from "src/query/order/queryHooksCommon";
import { currency } from "src/utils/currency";
import { slugify } from "src/utils/stringToPath";

const OrderDetailPage = () => {
  const params = useParams();
  const id = params.id;

  const { data: dataDetail, isFetching } = useQueryOrderDetail(String(id));

  const { data: shippingMethod } = useShippingMethod();

  const checkoutReturn = useMemo(() => {
    if (dataDetail && dataDetail.status === "finished") {
      return moment(dataDetail.finishedDate) > moment().add(-7, "d");
    }
  }, [dataDetail]);
  const shipping = useMemo(
    () =>
      shippingMethod?.find(
        (e) => e.code === dataDetail?.shipping.shippingMethod,
      ) || null,
    [dataDetail],
  );

  if (isFetching) return <OrderDetailLoading />;

  return (
    <>
      <div className="card card-lg mb-5 border">
        <div className="card-body pb-0">
          <div className="card card-sm">
            <OrderItemStatus {...dataDetail} />
          </div>
        </div>
        <div className="card-footer">
          <h6 className="mb-7">Order Items ({dataDetail?.totalQuantity})</h6>
          <hr className="my-5" />
          <ul className="list-group list-group-lg list-group-flush-y list-group-flush-x">
            {dataDetail?.listItems.map((e) => {
              const path =
                PATH.products +
                "/" +
                slugify(e.product.name) +
                "-p" +
                e.productId;
              return (
                <li key={e.productId} className="list-group-item px-0">
                  <div className="flex align-items-center gap-2">
                    <Link className="block w-[120px]" href={path}>
                      <img
                        src={e.product.thumbnail_url}
                        alt="..."
                        className="img-fluid"
                      />
                    </Link>
                    <p className="mb-4 font-size-sm font-weight-bold">
                      <Link className="text-body" href={path}>
                        {e.product.name} {e.quantity > 1 && `x ${e.quantity}`}
                      </Link>{" "}
                      <br />
                      <span className="text-muted">{currency(e.price)}</span>
                    </p>

                    <div className="card-right-info flex flex-col gap-2 max-sm:bottom-1">
                      {["finished", "cancel"].includes(dataDetail.status) && (
                        <ButtonCustom className="btn-xs" type="outline">
                          Mua lại
                        </ButtonCustom>
                      )}
                      {dataDetail.status === "finished" && !e.review && (
                        <Link
                          className="btn btn-xs btn-block btn-outline-dark"
                          href={{
                            pathname: `${PATH.products}/${e.product.slug}`,
                            query: { orderId: dataDetail._id },
                          }}
                        >
                          Viết review
                        </Link>
                      )}
                    </div>
                  </div>
                </li>
              );
            })}
          </ul>
          {checkoutReturn && (
            <p className="text-sm text-red-400">
              Product &quot;return&quot;within 7 days of receipt
            </p>
          )}
        </div>
      </div>

      <ShippingOrderTotal {...dataDetail} />

      <ShippingOrderDetail
        {...dataDetail}
        description={shipping?.description}
        title={shipping?.title}
      />
    </>
  );
};

export default OrderDetailPage;
