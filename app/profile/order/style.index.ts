import styled from "styled-components";

export const OrderPageStyle = styled.div`
  .ant-tabs {
    &-nav-wrap {
      width: 100%;
      overflow: unset !important;
      @media (max-width: 640px) {
        overflow: hidden !important;
      }
    }
    &-nav-list {
      width: 100%;
      justify-content: space-between;
      @media (max-width: 640px) {
        width: unset;
        justify-content: unset;
      }
    }
    &-tab {
      padding: 15px 0 10px 0 !important;

      @media (max-width: 640px) {
        &:not(:first-of-type) {
          margin: 0 0 0 32px !important;
        }
      }
      font-size: 14px;
      margin: unset !important;

      &:hover {
        color: #ff6f61 !important;
      }
    }

    &-tab-active {
      .ant-tabs-tab-btn {
        color: #ff6f61 !important;
      }
    }
    &-ink-bar {
      background-color: #ff6f61 !important;
    }
  }
`;

export const LabelTabOrderStyle = styled.div`
  font-weight: 600;
  .ant-badge-count {
    right: -14px;
    top: 2px;
  }
`;
