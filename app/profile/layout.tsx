import { Metadata } from "next";
import { redirect } from "next/navigation";
import { getServerSession } from "next-auth";
import { PATH } from "src/constants/path";
import authOptions from "src/helper/authOptions";

import ProfileMenu from "./ProfileMenu";

export const metadata: Metadata = {
  title: {
    default: "Information User",
    template: "%s | Shopper ",
  },
  description: "Information User's Shopper eCommerce Website",
};

export default async function ProfileLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const session = await getServerSession(authOptions);

  if (!session) {
    redirect(PATH.authentication);
  }

  return (
    <section className="pt-7 pb-12 max-sm:!pt-0 max-sm:!pb-8">
      <div className="container">
        <div className="row">
          <ProfileMenu />
          <div className="col-12 col-md-9 col-lg-8 offset-lg-1">{children}</div>
        </div>
      </div>
    </section>
  );
}
