"use client";

import { yupResolver } from "@hookform/resolvers/yup";
import { useQuery } from "@tanstack/react-query";
import { Form, message } from "antd";
import { useRef } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import ButtonCustom from "src/components/Button";
import Field from "src/components/Field";
import FormUserLoading from "src/components/FormUserLoading";
import { RadioGroup, RadioToggle } from "src/components/Radio";
import { UploadFile } from "src/components/UploadFile";
import { AvatarDefault } from "src/constants";
import { MESSAGE } from "src/constants/message";
import { useMutationUploadFile } from "src/mutate/uploadFile/mutationHooksCommon";
import {
  useMutationChangePasswordUser,
  useMutationUpdateProfileUser,
} from "src/mutate/user/mutationHooksCommon";
import { QueryKeysUserCommon } from "src/query/user/queryKeysCommon";
import { TypeProfileUser, TypeUpdateProfileForm } from "src/types/user";
import { handleError } from "src/utils/handleError";
import { isEqual } from "src/utils/isEqual";
import { REGEXP } from "src/utils/RegExp";
import * as yup from "yup";

const schema = yup
  .object({
    name: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.SPECIAL_CHARACTER, MESSAGE.validate.specialCharacter)
      .test("emoji", MESSAGE.validate.emoji, (name: string): boolean => {
        if (!name) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(name);
      }),
    phone: yup
      .string()
      .matches(REGEXP.PHONE, MESSAGE.validate.phone)
      .required(MESSAGE.validate.required),
    birthday: yup.string(),
    currentPassword: yup
      .string()
      .trim()
      .min(6, MESSAGE.validate.min)
      .max(32, MESSAGE.validate.max),
    newPassword: yup
      .string()
      .trim()
      .min(6, MESSAGE.validate.min)
      .max(32, MESSAGE.validate.max)
      .test(
        "empty",
        MESSAGE.validate.notOverlap,
        (
          newPassword: string | undefined,
          testContext: yup.TestContext<yup.AnyObject>,
        ): boolean => {
          if (!testContext.parent.currentPassword) return true;
          return testContext.parent.currentPassword !== newPassword;
        },
      ),
    confirmPassword: yup
      .string()
      .trim()
      .min(6, MESSAGE.validate.min)
      .max(32, MESSAGE.validate.max)
      .test(
        "empty",
        MESSAGE.validate.notMatch,
        (
          confirmPassword: string | undefined,
          testContext: yup.TestContext<yup.AnyObject>,
        ): boolean => {
          if (!testContext.parent.newPassword) return true;
          return testContext.parent.newPassword === confirmPassword;
        },
      ),
    gender: yup.string(),
  })
  .required();

const ProfilePage = () => {
  const fileRef = useRef<File | null>();

  const {
    data: user,
    isLoading,
    refetch,
  } = useQuery<TypeProfileUser>([QueryKeysUserCommon.PROFILE_USER]);

  const { mutateAsync: mutateAsyncUploadFile, isLoading: loadingUploadFile } =
    useMutationUploadFile();
  const {
    mutateAsync: mutateAsyncChangePassword,
    isLoading: loadingChangePassword,
  } = useMutationChangePasswordUser();
  const {
    mutateAsync: mutateAsyncUpdateProfile,
    isLoading: LoadingUpdateProfile,
  } = useMutationUpdateProfileUser();

  const {
    handleSubmit,
    control,
    getValues,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypeUpdateProfileForm> = async (
    data: TypeUpdateProfileForm,
  ) => {
    let avatar;
    const value = getValues();

    try {
      if (fileRef.current) {
        const res = await mutateAsyncUploadFile(fileRef.current);
        avatar = res.link;
      }
    } catch (err: any) {
      handleError(err);
    }

    if (
      avatar ||
      !isEqual(
        value,
        user as TypeProfileUser,
        "phone",
        "name",
        "birthday",
        "gender",
      )
    ) {
      mutateAsyncUpdateProfile({
        ...data,
        avatar,
      })
        .then(() => {
          refetch();
          message.success(MESSAGE.profile.updateProfileSuccess);
          fileRef.current = null;
        })
        .catch(handleError);
    } else if (!value.newPassword) {
      message.warning(MESSAGE.warning.notChange);
    }
    if (value.newPassword) {
      mutateAsyncChangePassword({
        currentPassword: value.currentPassword as string,
        newPassword: value.newPassword,
      })
        .then(() => {
          reset({
            ...user,
            newPassword: "",
            currentPassword: "",
            confirmPassword: "",
          });
          message.success(MESSAGE.profile.changePasswordSuccess);
        })
        .catch(handleError);
    }
  };

  return (
    <>
      <div className="row">
        <div className="col-12 text-center">
          <h3 className="mb-10 max-sm:!mb-6">Information User</h3>
        </div>
      </div>
      {isLoading ? (
        <FormUserLoading />
      ) : (
        <Form onSubmitCapture={handleSubmit(onSubmit)}>
          <div className="row">
            <div className="col-12">
              <div className="profile-avatar">
                <UploadFile onChange={(file) => (fileRef.current = file)}>
                  {({ previewSrc, triggerEvent }) => (
                    <div className="wrap" onClick={triggerEvent}>
                      <img
                        src={previewSrc || user?.avatar || AvatarDefault.src}
                        alt="avatar-user"
                      />
                      <i className="icon">
                        <img
                          src="./static/img/icons/icon-camera.svg"
                          alt="avatar-user"
                        />
                      </i>
                    </div>
                  )}
                </UploadFile>
              </div>
            </div>
            <div className="col-12">
              <Field
                errors={errors.name?.message}
                name={"name"}
                defaultValue={user?.name}
                control={control}
                label="Full Name *"
                placeholder="Full Name . . . "
              />
            </div>
            <div className="col-12 col-md-6">
              <Field
                errors={errors.phone?.message}
                name={"phone"}
                defaultValue={user?.phone}
                control={control}
                label="Number Phone *"
                placeholder="Number Phone . . . "
              />
            </div>
            <div className="col-12 col-md-6">
              <Field
                defaultValue={user?.username}
                label="Email *"
                disabled
                placeholder="Email Address . . . "
              />
            </div>
            <div className="col-12">
              <Field
                control={control}
                errors={errors.currentPassword?.message}
                name="currentPassword"
                label="Current Password"
                type="password"
                placeholder="Current Password . . . "
              />
            </div>
            <div className="col-12 col-md-6">
              <Field
                control={control}
                errors={errors.newPassword?.message}
                name="newPassword"
                label="New Password"
                type="password"
                placeholder="New Password . . . "
              />
            </div>
            <div className="col-12 col-md-6">
              <Field
                control={control}
                label="Confirm Password"
                name="confirmPassword"
                errors={errors.confirmPassword?.message}
                type="password"
                placeholder="Confirm Password . . . "
              />
            </div>
            <div className="col-12 col-md-6">
              <Field
                name="birthday"
                errors={errors.birthday?.message}
                control={control}
                defaultValue={user?.birthday}
                label="Day of Birthday"
                type="date"
              />
            </div>
            <div className="col-12 col-md-6">
              <Field
                label="Gender"
                name="gender"
                errors={errors.gender?.message}
                control={control}
                defaultValue={user?.gender}
                renderField={(props: any) => (
                  <RadioGroup value={props.values} {...props} toggle>
                    <RadioToggle value="male">Male</RadioToggle>
                    <RadioToggle value="female">Female</RadioToggle>
                  </RadioGroup>
                )}
              />
            </div>
            <div className="col-12">
              <ButtonCustom
                loading={
                  loadingChangePassword ||
                  loadingUploadFile ||
                  LoadingUpdateProfile
                }
              >
                Save Changes
              </ButtonCustom>
            </div>
          </div>
        </Form>
      )}
    </>
  );
};

export default ProfilePage;
