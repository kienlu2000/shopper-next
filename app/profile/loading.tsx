import React from "react";
import LoadingLayout from "src/components/LoadingLayout";

export default function Loading() {
  return <LoadingLayout />;
}
