"use client";

import Link from "next/link";
import queryString from "query-string";
import React from "react";
import { PaginateCustom } from "src/components/PaginateCustom";
import ProductCard from "src/components/ProductCard";
import { ProductCardLoading } from "src/components/ProductCardLoading";
import { PATH } from "src/constants/path";
import { useSearch } from "src/hooks/useSearch";
import { useQueryWishlist } from "src/query/wishlist/queryHooksCommon";
import { array } from "src/utils/array";

const CollectionPage = () => {
  const [search] = useSearch({
    page: 1,
    limit: 9,
  });

  const query = queryString.stringify(search);

  const { data, isFetching, refetch } = useQueryWishlist(query);

  const dataWishlist = data?.data;

  const dataPaginate = data?.paginate;

  return (
    <>
      <div className="row max-xl:grid max-xl:grid-cols-2">
        {isFetching ? (
          array(9).map((_, i) => (
            <div
              key={i}
              className="col-6 col-md-4 max-xl:max-w-full max-sm:!px-2"
            >
              <ProductCardLoading />
            </div>
          ))
        ) : dataWishlist && dataWishlist?.length > 0 ? (
          dataWishlist.map((e) => (
            <div
              key={e.id}
              className="col-6 col-md-4 max-xl:max-w-full max-sm:!px-2"
            >
              <ProductCard
                showRemove
                onRemoveWishlistSuccess={() => {
                  refetch();
                }}
                {...e}
              />
            </div>
          ))
        ) : (
          <p className="text-xl border p-5 w-full text-center">
            You currently don&apos;t have any favorite products, you can submit
            any product Which product you want go to favorite products 😞
            <br />
            <Link className="btn btn-sm btn-dark mt-5" href={PATH.products}>
              Go To Shop
            </Link>
          </p>
        )}
      </div>
      <PaginateCustom totalPage={dataPaginate?.totalPage} />
    </>
  );
};

export default CollectionPage;
