"use client";

import Link from "next/link";
import { useMemo } from "react";
import { AddressCard } from "src/components/AddressCard";
import AddressCardLoading from "src/components/AddressCardLoading";
import { PATH } from "src/constants/path";
import { useAddressUser } from "src/query/address/queryHooksCommon";
import { array } from "src/utils/array";

const AddressPage = () => {
  const { data, isFetching, refetch } = useAddressUser("");

  const dataSort = useMemo(() => {
    if (!data) return null;

    const sortedData = [...data].sort((e) => (e.default ? -1 : 0));

    return sortedData;
  }, [data]);

  return (
    <div className="row">
      {isFetching ? (
        array(3).map((_, i) => (
          <div key={i} className="col-12">
            <AddressCardLoading />
          </div>
        ))
      ) : dataSort && dataSort?.length > 0 ? (
        dataSort?.map((e) => (
          <AddressCard refetch={refetch} key={e._id} {...e} />
        ))
      ) : (
        <div className="col-12">
          <p className="text-xl border p-5 text-center">
            You currently don&apos;t have any book addresses, please add a book
            address to use in The purchasing process is better.
          </p>
        </div>
      )}

      <div className="col-12">
        <Link
          className="btn btn-block btn-lg btn-outline-border"
          href={PATH.profile.newAddress}
        >
          Add Address <i className="fe fe-plus" />
        </Link>
      </div>
    </div>
  );
};

export default AddressPage;
