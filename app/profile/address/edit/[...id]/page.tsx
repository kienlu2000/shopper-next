"use client";

import { yupResolver } from "@hookform/resolvers/yup";
import { Form, message } from "antd";
import { useParams, useRouter } from "next/navigation";
import { SubmitHandler, useForm } from "react-hook-form";
import ButtonCustom from "src/components/Button";
import Field from "src/components/Field";
import FormAddressLoading from "src/components/FormAddressLoading";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { PATH } from "src/constants/path";
import {
  useMutationAddAddress,
  useMutationEditAddress,
} from "src/mutate/address/mutationHooksCommon";
import { useAddressDetailUser } from "src/query/address/queryHooksCommon";
import { TypeAddressUser, TypeFormEditAddressUser } from "src/types/user";
import { handleError } from "src/utils/handleError";
import { isEqual } from "src/utils/isEqual";
import { REGEXP } from "src/utils/RegExp";
import * as yup from "yup";

const schema = yup
  .object({
    fullName: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.SPECIAL_CHARACTER, MESSAGE.validate.specialCharacter)
      .test("emoji", MESSAGE.validate.emoji, (name: string): boolean => {
        if (!name) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(name);
      }),
    phone: yup
      .string()
      .matches(REGEXP.PHONE, MESSAGE.validate.phone)
      .required(MESSAGE.validate.required),
    email: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.EMAIL, MESSAGE.validate.email)
      .test("emoji", MESSAGE.validate.emoji, (username: string): boolean => {
        if (!username) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(username);
      }),
    province: yup.string().trim().required(MESSAGE.validate.required),
    district: yup.string().trim().required(MESSAGE.validate.required),
    address: yup.string().trim().required(MESSAGE.validate.required),
  })
  .required();

const EditAddressPage = () => {
  const router = useRouter();
  const params = useParams();
  const id = params.id;

  const { data: addressUser, isFetching } = useAddressDetailUser(String(id));

  const { mutateAsync: mutateEditAddress, isLoading: loadingEditAddress } =
    useMutationEditAddress();
  const { mutateAsync: mutateAddAddress, isLoading: loadingAddAddress } =
    useMutationAddAddress();

  const {
    handleSubmit,
    control,
    getValues,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypeFormEditAddressUser> = async (
    data: TypeFormEditAddressUser,
  ) => {
    const value = getValues();

    try {
      if (id) {
        if (
          isEqual(
            value,
            addressUser as TypeAddressUser,
            "fullName",
            "phone",
            "address",
            "email",
            "district",
            "province",
          )
        )
          return message.warning({
            key: KEYS_MESSAGE.WARNING,
            content: MESSAGE.warning.notChange,
          });
        mutateEditAddress({
          id: String(id),
          form: { ...data, default: false },
        })
          .then(() => {
            router.push(PATH.profile.address);
            message.success({
              key: KEYS_MESSAGE.EDIT_ADDRESS,
              content: MESSAGE.address.editSuccess,
            });
          })
          .catch((err: any) => {
            handleError(err);
          });
      } else {
        mutateAddAddress(data)
          .then(() => {
            router.push(PATH.profile.address);
            message.success({
              key: KEYS_MESSAGE.ADD_ADDRESS,
              content: MESSAGE.address.addSuccess,
            });
          })
          .catch((err: any) => {
            handleError(err);
          });
      }
    } catch (err: any) {
      handleError(err);
    }
  };

  if (isFetching) return <FormAddressLoading />;

  return (
    <Form onSubmitCapture={handleSubmit(onSubmit)}>
      <div className="row">
        <div className="col-12">
          <Field
            defaultValue={addressUser?.fullName || ""}
            name={"fullName"}
            label="Full Name *"
            placeholder="Full Name"
            control={control}
            errors={errors.fullName?.message}
          />
        </div>
        <div className="col-12 col-md-6">
          <Field
            name={"phone"}
            defaultValue={addressUser?.phone || ""}
            label="Number Phone *"
            placeholder="Number Phone"
            control={control}
            errors={errors.phone?.message}
          />
        </div>
        <div className="col-12 col-md-6">
          <Field
            name={"email"}
            defaultValue={addressUser?.email || ""}
            label="Address Email *"
            placeholder="Address Email"
            control={control}
            errors={errors.email?.message}
          />
        </div>
        <div className="col-12 col-md-6">
          <Field
            name={"district"}
            defaultValue={addressUser?.district || ""}
            label="District *"
            placeholder="District"
            control={control}
            errors={errors.district?.message}
          />
        </div>
        <div className="col-12 col-md-6">
          <Field
            name={"province"}
            defaultValue={addressUser?.province || ""}
            label="City *"
            placeholder="City"
            control={control}
            errors={errors.province?.message}
          />
        </div>
        <div className="col-12">
          <Field
            defaultValue={addressUser?.address || ""}
            label="Current address *"
            placeholder="Current address"
            control={control}
            errors={errors.address?.message}
            name={"address"}
          />
        </div>
      </div>
      <ButtonCustom loading={loadingEditAddress || loadingAddAddress}>
        {id ? "Save" : "Add new address"}
      </ButtonCustom>
    </Form>
  );
};

export default EditAddressPage;
