"use client";

import classNames from "classnames";
import Link from "next/link";
import { usePathname } from "next/navigation";
import React from "react";
import { LIST_MENU_PROFILE } from "src/constants/menu-profile";
import { handleLogout } from "src/helper/handleLogout";

const ProfileMenu = () => {
  const pathname = usePathname();

  return (
    <div className="col-12 col-md-3">
      <nav className="mb-10 mb-md-0">
        <div className="list-group list-group-sm list-group-strong list-group-flush-x">
          {LIST_MENU_PROFILE.map((item) => (
            <Link
              key={item.key}
              className={classNames(
                "list-group-item list-group-item-action dropright-toggle",
                pathname === item.link ? "active" : "",
              )}
              href={item.link}
            >
              {item.name}
            </Link>
          ))}
          <div
            onClick={handleLogout()}
            className="list-group-item list-group-item-action dropright-toggle cursor-pointer"
          >
            Đăng xuất
          </div>
        </div>
      </nav>
    </div>
  );
};

export default ProfileMenu;
