"use client";

import Link from "next/link";
import React, { useMemo } from "react";
import PaymentCard from "src/components/PaymentCard";
import PaymentCardLoading from "src/components/PaymentCardLoading";
import { PATH } from "src/constants/path";
import { usePaymentUser } from "src/query/payment/queryHooksCommon";
import { array } from "src/utils/array";

const PaymentPage = () => {
  const { data, isFetching, refetch } = usePaymentUser("");

  const dataSort = useMemo(() => {
    if (!data) return null;

    const sortedData = [...data].sort((e) => (e.default ? -1 : 0));

    return sortedData;
  }, [data]);

  return (
    <div className="row">
      {isFetching ? (
        array(3).map((_, i) => (
          <div key={i} className="col-12">
            <PaymentCardLoading />
          </div>
        ))
      ) : dataSort && dataSort?.length > 0 ? (
        dataSort?.map((e) => (
          <PaymentCard refetch={refetch} key={e._id} {...e} />
        ))
      ) : (
        <div className="col12">
          <p className="text-xl border p-5 text-center">
            You currently don&apos;t have any book addresses, please add a book
            address to use in The purchasing process is better.
          </p>
        </div>
      )}
      <div className="col-12">
        <Link
          className="btn btn-block btn-lg btn-outline-border"
          href={PATH.profile.actionPayment}
        >
          Add Payment Method <i className="fe fe-plus" />
        </Link>
      </div>
    </div>
  );
};

export default PaymentPage;
