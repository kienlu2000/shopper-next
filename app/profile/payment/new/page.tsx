"use client";

import { yupResolver } from "@hookform/resolvers/yup";
import { Form, message, Popover } from "antd";
import moment from "moment";
import { useParams, useRouter } from "next/navigation";
import { useMemo, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import ButtonCustom from "src/components/Button";
import Field from "src/components/Field";
import FormPaymentLoading from "src/components/FormPaymentLoading";
import { Radio, RadioGroup } from "src/components/Radio";
import SelectCustom from "src/components/SelectCustom";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { PATH } from "src/constants/path";
import {
  useMutationAddPayment,
  useMutationEditPayment,
} from "src/mutate/payment/mutationHooksCommon";
import { usePaymentDetailUser } from "src/query/payment/queryHooksCommon";
import { TypeFormEditPaymentUser, TypePaymentItem } from "src/types/payment";
import { handleError } from "src/utils/handleError";
import { isEqual } from "src/utils/isEqual";
import * as yup from "yup";

const schema = yup
  .object({
    cardName: yup.string().trim().required(MESSAGE.validate.required),
    cardNumber: yup.string().trim().required(MESSAGE.validate.required),
    cvv: yup.string().trim().required(MESSAGE.validate.required),
    month: yup.string().trim().required(MESSAGE.validate.required),
    year: yup.string().trim().required(MESSAGE.validate.required),
  })
  .required();

const ActionPaymentPage = () => {
  const router = useRouter();
  const params = useParams();
  const id = params.id;

  const { data: paymentDetail, isFetching } = usePaymentDetailUser(String(id));

  const { mutateAsync: mutateAddPayment, isLoading: loadingAddPayment } =
    useMutationAddPayment();
  const { mutateAsync: mutateEditPayment, isLoading: loadingEditPayment } =
    useMutationEditPayment();

  const [type, setType] = useState<string>("card");
  const [step, setStep] = useState<1 | 0>(id ? 1 : 0);

  const optionsYear = useMemo(() => {
    const currentYear = new Date().getFullYear();
    return Array.from({ length: 30 }, (_, i) => ({
      label: currentYear - 15 + i,
      value: currentYear - 15 + i,
    }));
  }, []);

  const optionsMonth = useMemo(() => {
    return Array.from({ length: 12 }, (_, i) => ({
      value: String(i + 1),
      label: moment(`${i + 1}/01/2000`).format("MMMM"),
    }));
  }, []);

  const {
    handleSubmit,
    control,
    getValues,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypeFormEditPaymentUser> = async (
    data: TypeFormEditPaymentUser,
  ) => {
    const value = getValues();

    const expired = `${data.month}/${data.year}`;

    try {
      if (id) {
        if (
          isEqual(
            value,
            paymentDetail as TypePaymentItem,
            "cardNumber",
            "cardName",
            "month",
            "year",
            "cvv",
          )
        )
          return message.warning({
            key: KEYS_MESSAGE.WARNING,
            content: MESSAGE.warning.notChange,
          });
        mutateEditPayment({
          id: String(id),
          form: { ...data, expired, default: false },
        })
          .then(() => {
            router.push(PATH.profile.payment);
            message.success({
              key: KEYS_MESSAGE.EDIT_PAYMENT,
              content: MESSAGE.payment.editSuccess,
            });
          })
          .catch((err: any) => {
            handleError(err);
          });
      } else {
        mutateAddPayment({
          type,
          expired,
          ...data,
        })
          .then(() => {
            router.push(PATH.profile.payment);
            message.success({
              key: KEYS_MESSAGE.ADD_PAYMENT,
              content: MESSAGE.payment.addSuccess,
            });
          })
          .catch((err: any) => {
            handleError(err);
          });
      }
    } catch (err: any) {
      handleError(err);
    }
  };

  if (isFetching) return <FormPaymentLoading />;

  return (
    <div>
      {step === 0 && (
        <div>
          <RadioGroup
            value={type}
            onChange={(value) => {
              setType(value.target.value);
            }}
          >
            <div className="form-group card card-sm border">
              <div className="card-body">
                <Radio value="card">
                  I want to add Debit / Credit Card{" "}
                  <img
                    className="ml-2"
                    src="/static/img/brands/color/cards.svg"
                    alt="..."
                  />
                </Radio>
              </div>
            </div>
            <div className="form-group card card-sm border">
              <div className="card-body">
                <Radio value="paypall">
                  I want to add PayPal{" "}
                  <img
                    className="ml-2"
                    src="/static/img/brands/color/paypal.svg"
                    alt="..."
                  />
                </Radio>
              </div>
            </div>
          </RadioGroup>
          <button onClick={() => setStep(1)} className="btn btn-dark">
            Continue <i className="fe fe-arrow-right ml-2" />
          </button>
        </div>
      )}
      {step === 1 && (
        <Form onSubmitCapture={handleSubmit(onSubmit)}>
          <div className="row">
            <div className="col-12 col-md-6">
              <Field
                control={control}
                errors={errors.cardNumber?.message}
                name={"cardNumber"}
                label="Card Number *"
                placeholder="Card Number *"
                defaultValue={paymentDetail?.cardNumber || ""}
              />
            </div>

            <div className="col-12 col-md-6">
              <Field
                control={control}
                errors={errors.cardName?.message}
                name={"cardName"}
                label="Name on Card *"
                placeholder="Name on Card *"
                defaultValue={paymentDetail?.cardName || ""}
              />
            </div>
            <div className="col-12">
              <label>Expiry Date *</label>
            </div>
            <div className="col-12 col-md-4">
              <SelectCustom
                control={control}
                errors={errors.month?.message}
                name={"month"}
                placeholder="Month *"
                options={optionsMonth}
                defaultValue={paymentDetail?.expired.split("/")?.[0]}
              />
            </div>
            <div className="col-12 col-md-4">
              <SelectCustom
                control={control}
                errors={errors.year?.message}
                name={"year"}
                placeholder="Year *"
                options={optionsYear}
                defaultValue={paymentDetail?.expired.split("/")?.[1]}
              />
            </div>
            <div className="col-12 col-md-4">
              <Field
                control={control}
                errors={errors.cvv?.message}
                name={"cvv"}
                placeholder="CVV *"
                type="number"
                defaultValue={paymentDetail?.cvv || ""}
                suffix={
                  <Popover
                    placement="topRight"
                    content={
                      "The CVV Number on your credit card or debit card is a 3 digit number on VISA, MasterCard and Discover branded credit and debit cards."
                    }
                    overlayStyle={{ maxWidth: 250 }}
                  >
                    <i className="fe fe-help-circle flex items-center" />
                  </Popover>
                }
              />
            </div>
          </div>
          <ButtonCustom
            loading={loadingAddPayment || loadingEditPayment}
            className="btn btn-dark"
          >
            {id ? "Save" : "Add Card"}
          </ButtonCustom>
        </Form>
      )}
    </div>
  );
};

export default ActionPaymentPage;
