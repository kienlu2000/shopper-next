import { Metadata } from "next";

export const metadata: Metadata = {
  title: {
    default: "Products",
    template: "%s | Shopper ",
  },
  description: "Products's Shopper eCommerce Website",
};

export default async function ProductLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <>{children}</>;
}
