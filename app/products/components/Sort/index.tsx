import React from "react";
import { useSearch } from "src/hooks/useSearch";

const Sort = () => {
  const [search, setSearch] = useSearch({
    page: 1,
    sort: "newest",
  });

  return (
    <div className="col-12 col-md-auto flex gap-1 items-center whitespace-nowrap">
      Sắp xếp theo :
      <select
        value={search.sort}
        onChange={(ev) => {
          setSearch({
            sort: ev.target.value,
            page: 1,
          });
        }}
        className="custom-select custom-select-xs"
      >
        <option value="newest">Newest</option>
        <option value="real_price.desc">Price Decrease</option>
        <option value="real_price.asc">Price Increase</option>
        <option value="discount_rate.desc">Discount Higher</option>
        <option value="rating_average.desc">Rated Higher</option>
        <option value="top_seller">Buy Most</option>
      </select>
    </div>
  );
};

export default Sort;
