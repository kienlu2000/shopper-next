import IconStar from "public/static/img/icons/icon-star";
import IconStarFill from "public/static/img/icons/icon-star-fill";
import React from "react";
import { Radio, RadioGroup } from "src/components/Radio";
import { useSearch } from "src/hooks/useSearch";
import { array } from "src/utils/array";

const ShortRating = () => {
  const [search, setSearch] = useSearch({
    page: 1,
    sort: "newest",
  });

  const onChangeFilterRating = (
    ev?: React.ChangeEvent<HTMLInputElement>,
    reason?: string,
  ) => {
    if (reason === "uncheckedWhen2nd") {
      setSearch({
        ...search,
        rating_average: undefined,
      });
    } else {
      setSearch({
        ...search,
        rating_average: ev?.target.value,
      });
    }
  };

  return (
    <li className="nav-item">
      <div className="nav-link font-size-lg text-reset border-bottom mb-6">
        Rating
      </div>
      <div>
        <RadioGroup
          value={search.rating_average || ""}
          onChange={onChangeFilterRating}
        >
          <div className="form-group form-group-overflow mb-6" id="seasonGroup">
            <div className="custom-control custom-radio mb-3">
              <Radio value="5">
                {array(5).map((_, index) => (
                  <IconStarFill key={index} />
                ))}
                <span className="text-small inline-block ml-2">
                  from 5 star
                </span>
              </Radio>
            </div>
            <div className="custom-control custom-radio mb-3">
              <Radio value="4">
                {array(4).map((_, index) => (
                  <IconStarFill key={index} />
                ))}
                <IconStar />
                <span className="text-small inline-block ml-2">
                  from 4 star
                </span>
              </Radio>
            </div>
            <div className="custom-control custom-radio">
              <Radio value="3">
                {array(3).map((_, index) => (
                  <IconStarFill key={index} />
                ))}

                {array(2).map((_, index) => (
                  <IconStar key={index} />
                ))}
                <span className="text-small inline-block ml-2">
                  from 3 star
                </span>
              </Radio>
            </div>
          </div>
        </RadioGroup>
      </div>
    </li>
  );
};

export default ShortRating;
