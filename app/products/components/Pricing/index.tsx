import React, { useState } from "react";
import ButtonCustom from "src/components/Button";
import { useSearch } from "src/hooks/useSearch";

interface State {
  minPrice: string;
  maxPrice: string;
}

const Pricing = () => {
  const [search, setSearch] = useSearch({
    page: 1,
    sort: "newest",
  });

  const [state, setState] = useState<State>({
    maxPrice: "",
    minPrice: "",
  });

  const handlePricing = () => {
    setSearch({
      page: 1,
      sort: search.sort,
      minPrice: state.minPrice || undefined,
      maxPrice: state.maxPrice || undefined,
    });
  };

  return (
    <li className="nav-item">
      <div className="nav-link font-size-lg text-reset border-bottom mb-6">
        Price
      </div>
      <div>
        <div className="d-flex align-items-center">
          <input
            type="number"
            className="form-control form-control-xs"
            placeholder="Giá thấp nhất"
            value={state.minPrice}
            onChange={(ev) =>
              setState((prev) => ({
                ...prev,
                minPrice: ev.target.value,
              }))
            }
          />
          <div className="text-gray-350 mx-2">‒</div>
          <input
            type="number"
            className="form-control form-control-xs"
            placeholder="Giá cao nhất"
            value={state.maxPrice}
            onChange={(ev) =>
              setState((prev) => ({
                ...prev,
                maxPrice: ev.target.value,
              }))
            }
          />
        </div>
        <ButtonCustom
          onClick={() => handlePricing()}
          className="btn btn-outline-dark btn-block mt-5"
          type="button"
        >
          Apply
        </ButtonCustom>
      </div>
    </li>
  );
};

export default Pricing;
