import React from "react";

const NotFoundProduct = () => {
  return (
    <div className="col-12">
      <div className="modal-body border ">
        {/* Text */}
        <p className="mb-3 font-size-sm text-center">
          Rất tiếc không tìm thấy sản phẩm phù hợp với tiêu chí của bạn
        </p>
        <p className="mb-0 font-size-sm text-center">😞</p>
      </div>
    </div>
  );
};

export default NotFoundProduct;
