import { useSearchParams } from "next/navigation";
import React from "react";

const ResultSearch = () => {
  const searchParams = useSearchParams();

  const keyword = searchParams.get("search");

  return (
    <>
      {keyword && <h4 className="mb-5">Kết quả tìm kiếm cho : {keyword} </h4>}
    </>
  );
};

export default ResultSearch;
