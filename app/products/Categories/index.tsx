import classNames from "classnames";
import Link from "next/link";
import { useParams, useRouter } from "next/navigation";
import CategoriesLoading from "src/components/CategoriesLoading";
import { PATH } from "src/constants/path";
import { useAppSelector } from "src/hooks/app-hooks";
import {
  selectCategories,
  selectLoadingCategories,
} from "src/store/categories";
import { TypeCategoriesDetail } from "src/types/categories";
import { slugify } from "src/utils/stringToPath";

interface Props {
  e?: MouseEvent;
  id: number;
  slug: string;
}

const Categories = () => {
  const router = useRouter();
  const params = useParams();

  const uid = params.id;

  const listCategories = useAppSelector(selectCategories);
  const isLoading = useAppSelector(selectLoadingCategories);

  const handleClick = ({ e, id, slug }: Props) => {
    e?.preventDefault();
    router.push(`${PATH.products}/${slugify(slug)}/${id}`);
  };

  return (
    <li className="nav-item">
      <div className="nav-link font-size-lg text-reset border-bottom mb-6">
        Category
      </div>
      <div>
        <div className="form-group">
          <ul className="list-styled mb-0" id="productsNav">
            <li className="list-styled-item">
              <Link
                className={classNames(
                  "list-styled-link",
                  uid ? "" : "font-bold",
                )}
                href={PATH.products}
              >
                Tất cả sản phẩm
              </Link>
            </li>
            {isLoading ? (
              <CategoriesLoading />
            ) : (
              <>
                {listCategories?.map((e: TypeCategoriesDetail) => (
                  <li key={e.id} className="list-styled-item">
                    <div
                      className={classNames(
                        "list-styled-link cursor-pointer",
                        uid?.includes(String(e.id)) && "font-bold",
                      )}
                      onClick={() => handleClick({ id: e.id, slug: e.slug })}
                    >
                      {e.title}
                    </div>
                  </li>
                ))}
              </>
            )}
          </ul>
        </div>
      </div>
    </li>
  );
};

export default Categories;
