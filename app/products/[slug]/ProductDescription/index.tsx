import ShortedContent from "src/components/ShortedContent";

interface Props {
  description: string;
}

const ProductDescription = ({ description }: Props) => {
  return (
    <section className="pt-11 max-sm:!pt-2">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="nav nav-tabs nav-overflow justify-content-start justify-content-md-center border-bottom">
              <div className="nav-link active" data-toggle="tab">
                Description
              </div>
            </div>
            <div className="tab-content">
              <div>
                <div className="row justify-content-center py-9">
                  <div className="col-12 col-lg-10 col-xl-8">
                    <div className="row">
                      <div className="col-12">
                        <ShortedContent description={description || ""} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProductDescription;
