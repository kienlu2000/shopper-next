import { Image } from "antd";
import { useState } from "react";
import { isDesktop } from "react-device-detect";
import { queryClient } from "src/components/QueryProvider";
import { initialProductDetailValue } from "src/constants/initialValue";
import { QueryKeysCommon } from "src/query/products/queryKeysCommon";
import { TypeImageProductItem, TypeProductDetail } from "src/types/products";

interface Props {
  id: number;
}

const ProductPreview = ({ id }: Props) => {
  const [openModal, setOpenModal] = useState(false);

  const dataProductDetails =
    queryClient.getQueryData<TypeProductDetail>([
      QueryKeysCommon.PRODUCTS_DETAILS,
      id,
    ]) || initialProductDetailValue;

  const listImagePreview = isDesktop
    ? dataProductDetails?.images?.slice(0, 3)
    : dataProductDetails?.images?.slice(0, 2);

  const imagePreview = isDesktop ? 3 : 2;

  return (
    <div className="col-12 col-md-6">
      <div className="card">
        {dataProductDetails?.discount > 0 && (
          <div className="badge badge-primary card-badge text-uppercase">
            Sale
          </div>
        )}
        <div className="mb-4">
          <img
            onClick={() => setOpenModal(true)}
            src={dataProductDetails?.images?.[0].large_url}
            alt="..."
            className="card-img-top cursor-pointer"
          ></img>
        </div>
        <div style={{ display: "none" }}>
          <Image.PreviewGroup
            preview={{
              visible: openModal,
              onVisibleChange: (vis) => setOpenModal(vis),
              wrapStyle: { margin: 100 },
            }}
          >
            {dataProductDetails?.images?.map((e: TypeImageProductItem, i) => (
              <Image key={i} src={e.large_url} alt={e.large_url} />
            ))}
          </Image.PreviewGroup>
        </div>
      </div>
      <div
        className="flex mx-n2 mb-10 mb-md-0"
        data-flickity='{"asNavFor": "#productSlider", "contain": true, "wrapAround": false}'
      >
        {listImagePreview.map((e, i) => (
          <div key={i} className="col-12 px-2" style={{ maxWidth: "113px" }}>
            <div
              onClick={() => {
                setOpenModal(true);
              }}
              className="embed-responsive embed-responsive-1by1 bg-cover cursor-pointer"
              style={{
                backgroundImage: `url(${e.thumbnail_url})`,
              }}
            ></div>
          </div>
        ))}

        {dataProductDetails?.images?.length > imagePreview && (
          <div className=" col-12 px-2" style={{ maxWidth: "113px" }}>
            <div
              onClick={() => {
                setOpenModal(true);
              }}
              className="rounded-sm h-full bg-light flex items-center justify-center cursor-pointer"
            >
              + {dataProductDetails?.images?.length - imagePreview} <br />
              hình
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default ProductPreview;
