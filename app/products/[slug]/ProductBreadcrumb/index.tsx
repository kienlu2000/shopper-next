import Link from "next/link";
import { PATH } from "src/constants/path";
import { slugify } from "src/utils/stringToPath";
interface Props {
  name: string;
  idCategories?: number;
  title?: string;
  slug?: string;
}
const ProductBreadcrumb = ({ name, title, idCategories, slug }: Props) => {
  const linkCategories = `${PATH.products}/${slugify(slug)}/${idCategories}`;

  return (
    <nav className="py-5">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <ol className="breadcrumb flex items-center mb-0 font-size-xs text-gray-400">
              <li className="breadcrumb-item">
                <Link className="text-gray-400" href={PATH.home}>
                  Home
                </Link>
              </li>
              <li className="breadcrumb-item">
                <Link className="text-gray-400" href={linkCategories}>
                  {title}
                </Link>
              </li>
              <li className="breadcrumb-item font-bold  active">{name}</li>
            </ol>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default ProductBreadcrumb;
