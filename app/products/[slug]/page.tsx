"use client";

import NotFoundPage from "app/not-found";
import { usePathname } from "next/navigation";
import ProductDetailLoading from "src/components/ProductDetailLoading";
import { useScrollToTop } from "src/hooks/useScrollToTop";
import { useQueryCategoriesDetail } from "src/query/categories/queryHooksCommon";
import { useQueryProductDetails } from "src/query/products/queryHooksCommon";
import { TypeProductDetail } from "src/types/products";

import ProductBreadcrumb from "./ProductBreadcrumb";
import ProductDescription from "./ProductDescription";
import ProductInformation from "./ProductInformation";
import ProductPreview from "./ProductPreview";
import ProductReview from "./ProductReview";

const ProductDetails = () => {
  useScrollToTop();
  const slug = usePathname();

  const id = slug.split("-p").pop();

  const _id = Number(id);

  const {
    data: dataProductDetails,
    isError,
    isLoading,
  } = useQueryProductDetails(_id);

  const { data: listCategories } = useQueryCategoriesDetail({
    categories: Number(dataProductDetails?.categories),
    enabled: dataProductDetails as TypeProductDetail,
  });

  if (!id || isError) return <NotFoundPage />;

  return (
    <div>
      <>
        {isLoading ? (
          <ProductDetailLoading />
        ) : (
          <>
            <ProductBreadcrumb
              idCategories={listCategories?.id}
              slug={listCategories?.slug}
              title={listCategories?.title}
              name={dataProductDetails.name}
            />
            <section>
              <div className="container">
                <div className="row">
                  <div className="col-12">
                    <div className="row">
                      <ProductPreview id={_id} />

                      <ProductInformation
                        name={dataProductDetails.name}
                        id={_id}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </section>

            <ProductDescription description={dataProductDetails.description} />

            <ProductReview id={_id} />
          </>
        )}
      </>
    </div>
  );
};

export default ProductDetails;
