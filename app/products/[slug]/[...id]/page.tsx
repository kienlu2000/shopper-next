import ProductsPage from "app/products/page";
import React from "react";

const page = () => {
  return <ProductsPage />;
};

export default page;
