import styled from "styled-components";

export const BtnGroupStyle = styled.div`
  button {
    width: 100%;
  }
`;
