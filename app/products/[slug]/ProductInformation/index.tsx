import { Rate } from "antd";
import Link from "next/link";
import { useEffect, useState } from "react";
import PopConfirmCustom from "src/components/PopConfirmCustom";
import { queryClient } from "src/components/QueryProvider";
import SocialShare from "src/components/SocialShare";
import { initialProductDetailValue } from "src/constants/initialValue";
import { MESSAGE } from "src/constants/message";
import { useAppDispatch, useAppSelector } from "src/hooks/app-hooks";
import { useAction } from "src/hooks/useActions";
import { useMutationAddProductToWishlist } from "src/mutate/products/mutationHooksCommon";
import { QueryCategoriesKeysCommon } from "src/query/categories/queryKeysCommon";
import { QueryKeysCommon } from "src/query/products/queryKeysCommon";
import { QueryKeysUserCommon } from "src/query/user/queryKeysCommon";
import { selectAddProductLoading, selectCart } from "src/store/cart";
import { updateCartItemAction } from "src/store/cart/cart.action";
import { TypeCategoriesDetail } from "src/types/categories";
import { TypeProductDetail } from "src/types/products";
import { TypeProfileUser } from "src/types/user";
import { currency } from "src/utils/currency";

import { BtnGroupStyle } from "./style.index";

interface Props {
  id: number;
  name: string;
}

const ProductInformation = ({ id, name }: Props) => {
  const dispatch = useAppDispatch();

  const addProductLoading = useAppSelector(selectAddProductLoading);

  const cart = useAppSelector(selectCart);
  const user = queryClient.getQueryData<TypeProfileUser>([
    QueryKeysUserCommon.PROFILE_USER,
  ]);
  const dataProductDetails =
    queryClient.getQueryData<TypeProductDetail>([
      QueryKeysCommon.PRODUCTS_DETAILS,
      id,
    ]) || initialProductDetailValue;
  const listCategories = queryClient.getQueryData<TypeCategoriesDetail>([
    QueryCategoriesKeysCommon.CATEGORIES_DETAIL,
  ]);

  const { mutateAsync, isLoading } = useMutationAddProductToWishlist();

  const [rating, setRating] = useState<number>(0);

  const onAddProductCart = useAction({
    action: () => {
      const quantity =
        cart?.listItems?.find((e) => e.productId === id)?.quantity || 0;
      return dispatch(
        updateCartItemAction({
          productId: id,
          quantity: quantity + 1,
          showPopover: true,
        }),
      );
    },
    messageLoading: MESSAGE.cart.addingCart(name),
  });

  const onAddWishlist = useAction({
    action: () => mutateAsync(id),
    messageLoading: MESSAGE.wishlist.adding,
    messageSuccess: MESSAGE.wishlist.added(name),
  });

  useEffect(() => {
    if (!dataProductDetails) return;
    setRating(dataProductDetails?.rating_average);
  }, [dataProductDetails]);

  return (
    <div className="col-12 col-md-6 pl-lg-10">
      <div className="row mb-1">
        <div className="col">
          <div className="text-muted">{listCategories?.title}</div>
        </div>
        <div className="col-auto flex items-center max-sm:flex-col max-sm:items-start">
          <Rate
            className="text-sm rate"
            allowHalf
            disabled
            defaultValue={dataProductDetails?.rating_average}
          />
          <div className="font-size-sm text-reset ml-2 max-sm:!ml-0">
            {rating} star ({dataProductDetails?.review_count} Reviews)
          </div>
        </div>
      </div>
      <h3 className="mb-2">{dataProductDetails?.name}</h3>
      <div className="mb-7">
        {dataProductDetails?.real_price === dataProductDetails?.price ? (
          <span className="ml-1 font-size-h5 font-weight-bolder text-primary">
            {dataProductDetails?.real_price} VNĐ
          </span>
        ) : (
          <>
            <span className="font-size-lg font-weight-bold text-gray-350 text-decoration-line-through">
              {currency(dataProductDetails?.price || 0)} VNĐ
            </span>
            <span className="ml-1 font-size-h5 font-weight-bolder text-primary">
              {currency(dataProductDetails?.real_price || 0)} VNĐ
            </span>
          </>
        )}

        {dataProductDetails?.stock_item?.qty >= 1 ? (
          <span className="font-size-sm ml-1">(Stocking)</span>
        ) : (
          <span className="font-size-sm ml-1">(Out of stock)</span>
        )}
      </div>
      <p className="mb-10 max-sm:!mb-6">
        {dataProductDetails?.short_description}
      </p>
      <div className="form-group">
        <BtnGroupStyle className="form-row mb-7">
          <div className="col-12 col-lg">
            <PopConfirmCustom
              enable={!!user}
              type="button"
              loadingAction={addProductLoading}
              action={() => onAddProductCart()}
            />
          </div>
          <div className="col-12 col-lg-auto">
            <PopConfirmCustom
              loadingAction={isLoading}
              buttonType="outline"
              action={() => onAddWishlist()}
              enable={!!user}
              type="button"
              icon="fe fe-heart"
              name="Add To Wishlist"
            />
          </div>
        </BtnGroupStyle>
        <p>
          <span className="text-gray-500">Is your size/color sold out?</span>
          <Link
            className="text-reset text-decoration-underline"
            data-toggle="modal"
            href="#modalWaitList"
          >
            Join the Wait List!
          </Link>
        </p>
        <SocialShare />
      </div>
    </div>
  );
};

export default ProductInformation;
