import { yupResolver } from "@hookform/resolvers/yup";
import { Form, message, Rate } from "antd";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import ButtonCustom from "src/components/Button";
import TextAreaCustom from "src/components/TextAreaCustom";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { useMutationAddReview } from "src/mutate/review/mutationHooksCommon";
import { handleError } from "src/utils/handleError";
import * as yup from "yup";

interface Props {
  id: number;
  refetch: () => void;
}

interface TypeFormAddReview {
  content: string;
}

const schema = yup
  .object({
    content: yup.string().trim().required(MESSAGE.validate.required),
  })
  .required();

const ReviewNew = ({ id, refetch }: Props) => {
  const searchParam = useSearchParams();
  const slug = usePathname();
  const router = useRouter();

  const orderId = searchParam.get("orderId");

  const { mutateAsync, isLoading } = useMutationAddReview();

  const [valueRate, setValueRate] = useState<number>(0);

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypeFormAddReview> = async (
    data: TypeFormAddReview,
  ) => {
    try {
      mutateAsync({
        productId: id,
        form: {
          ...data,
          orderId: orderId as string,
          star: valueRate,
        },
      })
        .then(() => {
          router.replace(slug);
          message.success({
            key: KEYS_MESSAGE.ADD_REVIEW,
            content: MESSAGE.review.addSuccess,
          });
          refetch();
        })
        .catch((error: any) => {
          handleError(error);
        });
    } catch (error: any) {
      handleError(error);
    }
  };

  return (
    <div className="" id="reviewForm">
      <hr className="my-8" />
      <Form onSubmitCapture={handleSubmit(onSubmit)}>
        <div className="row">
          <div className="col-12 mb-6 text-center">
            <p className="mb-1 font-size-xs">Score:</p>
            <div className="rating-form">
              <Rate
                onChange={(value) => setValueRate(value)}
                allowHalf
                defaultValue={5}
              />
            </div>
          </div>
          <div className="col-12">
            <TextAreaCustom
              errors={errors.content?.message}
              name={"content"}
              control={control}
              placeholder="Content *"
            />
          </div>
          <div className="col-12 text-center flex justify-center">
            <ButtonCustom loading={isLoading} className="btn btn-dark">
              Post Review
            </ButtonCustom>
          </div>
        </div>
      </Form>
    </div>
  );
};

export default ReviewNew;
