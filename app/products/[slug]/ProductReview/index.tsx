import { useSearchParams } from "next/navigation";
import queryString from "query-string";
import { PaginateCustom } from "src/components/PaginateCustom";
import { queryClient } from "src/components/QueryProvider";
import { useQueryReviews } from "src/query/review/queryHooksCommon";
import { QueryKeysUserCommon } from "src/query/user/queryKeysCommon";
import { TypeProfileUser } from "src/types/user";
import WithListLoading from "src/utils/withListLoading";

import { ReviewItem } from "./ReviewItem";
import ReviewItemLoading from "./ReviewItemLoading";
import ReviewNew from "./ReviewNew";

interface Props {
  id: number;
}

const ProductReview = ({ id }: Props) => {
  const searchParam = useSearchParams();

  const page = searchParam.get("page");
  const orderId = searchParam.get("orderId");

  const query = queryString.stringify({
    page: page,
  });

  const { data, isLoading, refetch } = useQueryReviews(id, query);

  const user = queryClient.getQueryData<TypeProfileUser>([
    QueryKeysUserCommon.PROFILE_USER,
  ]);

  const dataReview = data?.data;

  const renderItem = () => {
    const ListReview = WithListLoading({
      Component: ReviewItem,
      ComponentLoading: ReviewItemLoading,
      data: dataReview,
      wrapperClass: "",
      loading: isLoading,
      loadingCount: 5,
      empty: (
        <div className="col-12 mt-5">
          <p className="text-xl border p-5 text-center mb-5 max-sm:!text-lg">
            There are currently no reviews for this product, please help us buy
            and Rate it for others to know
          </p>
        </div>
      ),
    });

    return ListReview;
  };

  return (
    <section className="pt-9 pb-11 max-sm:!pt-2" id="reviews">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h4 className="mb-10 text-center max-sm:!mb-6">Customer Reviews</h4>
            <div className="row align-items-center">
              <div className="col-12 col-md">
                <span className="ml-2">Reviews ({dataReview?.length})</span>
              </div>
            </div>

            {renderItem()}

            <PaginateCustom totalPage={data?.paginate.totalPage} />

            {user && orderId && <ReviewNew id={id} refetch={refetch} />}
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProductReview;
