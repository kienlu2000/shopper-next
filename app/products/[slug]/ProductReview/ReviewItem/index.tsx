import { Rate } from "antd";
import moment from "moment";
import React from "react";
import { TypeReviewItem } from "src/types/review";

export const ReviewItem = ({
  star,
  content,
  createdAt,
  user,
}: TypeReviewItem) => {
  return (
    <div className="mt-8">
      <div className="review">
        <div className="review-body">
          <div className="row">
            <div className="col-12 col-md-auto">
              <div className="avatar avatar-xxl mb-6 mb-md-0">
                <span className="avatar-title rounded-circle">
                  {user.avatar ? (
                    <div className="w-[100px] h-[100px] rounded-full overflow-hidden">
                      <img
                        className="w-full h-full object-cover"
                        src={user.avatar}
                        alt=""
                      />
                    </div>
                  ) : (
                    <i className="fa fa-user" />
                  )}
                </span>
              </div>
            </div>
            <div className="col-12 col-md">
              <div className="row mb-6">
                <div className="col-12">
                  <Rate
                    className="text-sm rate"
                    allowHalf
                    disabled
                    defaultValue={star}
                  />
                </div>
                <div className="col-12">
                  <span className="font-size-xs text-muted">
                    {user.name},{" "}
                    <time dateTime="2019-07-25">
                      {moment(createdAt).format("MMM DD YYYY")}
                    </time>
                  </span>
                </div>
              </div>
              <p className="text-gray-500 mb-0">{content}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
