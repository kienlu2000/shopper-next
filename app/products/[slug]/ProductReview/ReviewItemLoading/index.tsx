import React from "react";
import SkeletonCustom from "src/components/SkeletonCustom";

const ReviewItemLoading = () => {
  return (
    <div className="mt-8">
      <div className="review">
        <div className="review-body">
          <div className="row">
            <div className="col-12 col-md-auto">
              <div className="avatar avatar-xxl mb-6 mb-md-0">
                <span className="avatar-title rounded-circle">
                  <SkeletonCustom shape="circle" />
                </span>
              </div>
            </div>
            <div className="col-12 col-md">
              <div className="row mb-6">
                <div className="col-12">
                  <div className="rating font-size-sm text-dark">
                    <SkeletonCustom height={24} width={84.38} />
                  </div>
                </div>
                <div className="col-12">
                  <span className="font-size-xs text-muted">
                    <SkeletonCustom height={24} />
                  </span>
                </div>
              </div>
              <SkeletonCustom height={60} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ReviewItemLoading;
