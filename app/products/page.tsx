"use client";

import Link from "next/link";
import { useParams } from "next/navigation";
import queryString from "query-string";
import { PaginateCustom } from "src/components/PaginateCustom";
import ProductCard from "src/components/ProductCard";
import { ProductCardLoading } from "src/components/ProductCardLoading";
import { PATH } from "src/constants/path";
import { useCategory } from "src/hooks/useCategory";
import { useSearch } from "src/hooks/useSearch";
import { useQueryProducts } from "src/query/products/queryHooksCommon";
import { TypeProductDetail } from "src/types/products";
import { array } from "src/utils/array";

import PromotionSale from "../homepage/PromotionSale";
import Categories from "./Categories";
import NotFoundProduct from "./components/NotFoundProduct";
import Pricing from "./components/Pricing";
import ResultSearch from "./components/ResultSearch";
import ShortRating from "./components/ShortRating";
import Sort from "./components/Sort";

const ProductsPage = () => {
  const params = useParams();

  const [search] = useSearch({
    page: 1,
    sort: "newest",
  });

  const uid = params.id;

  const category = useCategory(parseInt(String(uid) || "0"));
  const query = queryString.stringify({
    categories: uid,
    page: search.page,
    sort: search.sort,
    name: search.search,
    maxPrice: search.maxPrice,
    minPrice: search.minPrice,
    rating_average: search.rating_average,
  });

  const { data, isFetched } = useQueryProducts(query);

  const dataProducts = data?.data;
  const paginateProducts = data?.paginate;

  return (
    <div>
      <PromotionSale />

      <section className="py-11 max-sm:!pt-2">
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-4 col-lg-3">
              <form className="mb-10 mb-md-0">
                <ul className="nav nav-vertical" id="filterNav">
                  <Categories />

                  <ShortRating />

                  <Pricing />
                </ul>
              </form>
            </div>
            <div className="col-12 col-md-8 col-lg-9">
              <div className="row align-items-center mb-7">
                <div className="col-12 col-md">
                  <h3 className="mb-1">
                    {category?.title || "Tất cả sản phẩm"}
                  </h3>
                  <ol className="breadcrumb flex items-center  mb-md-0 font-size-xs text-gray-400">
                    <li className="breadcrumb-item">
                      <Link className="text-gray-400" href={PATH.home}>
                        Home
                      </Link>
                    </li>
                    <li className="breadcrumb-item font-bold active">
                      {category?.title || "Tất cả sản phẩm"}
                    </li>
                  </ol>
                </div>
                <Sort />
              </div>
              <ResultSearch />
              <div className="row max-lg:grid max-lg:grid-cols-2">
                {!isFetched ? (
                  array(15).map((_, i) => (
                    <div
                      className="col-6 col-md-4 max-lg:max-w-full max-sm:!px-2"
                      key={i}
                    >
                      <ProductCardLoading />
                    </div>
                  ))
                ) : dataProducts && dataProducts?.length > 0 ? (
                  dataProducts?.map((item: TypeProductDetail) => (
                    <div
                      className="col-6 col-md-4 max-lg:max-w-full max-sm:!px-2"
                      key={item.id}
                    >
                      <ProductCard showWishlist {...item} />
                    </div>
                  ))
                ) : (
                  <NotFoundProduct />
                )}
              </div>
              <PaginateCustom totalPage={paginateProducts?.totalPage} />
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default ProductsPage;
