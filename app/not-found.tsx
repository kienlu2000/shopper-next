import Link from "next/link";
import NotFoundAnimate from "src/assets/json/404.json";
import LoadingAnimate from "src/components/LoadingAnimate";
import { PATH } from "src/constants/path";

const NotFoundPage = () => {
  return (
    <section className="py-12 max-sm:!py-8">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12 col-md-10 col-lg-8 col-xl-6 text-center">
            <LoadingAnimate path={NotFoundAnimate} height="150px" />
            <h2 className="mb-5">404. Page not found.</h2>
            <p className="mb-7 text-gray-500">
              Sorry, we couldn&apos;t find the page you where looking for. We
              suggest that you return to home page.
            </p>
            <Link className="btn btn-dark" href={PATH.home}>
              Go to Homepage
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NotFoundPage;
