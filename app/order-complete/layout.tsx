import { Metadata } from "next";

export const metadata: Metadata = {
  title: {
    default: "Order Complete",
    template: "%s | Shopper ",
  },
  description: "Order Complete's Shopper eCommerce Website",
};

export default async function OrderCompleteLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <>{children}</>;
}
