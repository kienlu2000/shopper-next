"use client";

import { useParams, useRouter } from "next/navigation";
import { useEffect } from "react";
import OrderCompleteAnimate from "src/assets/json/order-complete.json";
import ButtonCustom from "src/components/Button";
import LoadingAnimate from "src/components/LoadingAnimate";
import { PATH } from "src/constants/path";
import { useAppDispatch, useAppSelector } from "src/hooks/app-hooks";
import {
  actionIdOrderComplete,
  clearCart,
  selectIdOrderComplete,
} from "src/store/cart";

const OrderCompletePage = () => {
  const params = useParams();
  const router = useRouter();
  const dispatch = useAppDispatch();

  const uid = params.id;

  const id = useAppSelector(selectIdOrderComplete);

  useEffect(() => {
    if (id === String(uid)) {
      dispatch(clearCart());
    } else {
      router.push(PATH.myCart);
    }
  }, [uid]);

  const onNavigateDetailOrder = async () => {
    await router.push(PATH.profile.order + "/" + id);
    dispatch(actionIdOrderComplete(""));
  };

  return (
    <section className="py-12 max-sm:!py-8">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12 col-md-10 col-lg-8 col-xl-6 text-center">
            <LoadingAnimate path={OrderCompleteAnimate} height="150px" />
            <h2 className="mb-5">Your Order is Completed!</h2>
            <p className="mb-7 text-gray-500">
              Your order{" "}
              <span className="text-body text-decoration-underline">{id}</span>{" "}
              has been completed. Your order details are shown for your personal
              account.
            </p>
            <ButtonCustom
              onClick={() => onNavigateDetailOrder()}
              className="btn btn-dark cursor-pointer w-full"
            >
              View My Orders
            </ButtonCustom>
          </div>
        </div>
      </div>
    </section>
  );
};

export default OrderCompletePage;
