"use clients";

import { yupResolver } from "@hookform/resolvers/yup";
import { Form, message } from "antd";
import { SubmitHandler, useForm } from "react-hook-form";
import ButtonCustom from "src/components/Button";
import Field from "src/components/Field";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { useMutationRegisterUser } from "src/mutate/user/mutationHooksCommon";
import { TypeRegisterForm } from "src/types/auth";
import { handleError } from "src/utils/handleError";
import { REGEXP } from "src/utils/RegExp";
import * as yup from "yup";

const schema = yup
  .object({
    name: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.SPECIAL_CHARACTER, MESSAGE.validate.specialCharacter)
      .test("emoji", MESSAGE.validate.emoji, (name: string): boolean => {
        if (!name) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(name);
      }),
    username: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.EMAIL, MESSAGE.validate.email)
      .test("emoji", MESSAGE.validate.emoji, (username: string): boolean => {
        if (!username) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(username);
      }),
    password: yup
      .string()
      .trim()
      .min(6, MESSAGE.validate.min)
      .max(32, MESSAGE.validate.max)
      .required(MESSAGE.validate.required),
    confirmPassword: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .test(
        "empty",
        MESSAGE.validate.notMatch,
        (
          confirm_password: string | undefined,
          testContext: yup.TestContext<yup.AnyObject>,
        ): boolean => {
          if (!testContext.parent.password) return true;
          return testContext.parent.password === confirm_password;
        },
      ),
  })
  .required();

const Register = () => {
  const { mutateAsync, isLoading } = useMutationRegisterUser();
  const {
    handleSubmit,
    control,
    reset,
    formState: { errors, isValid },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypeRegisterForm> = async (
    data: TypeRegisterForm,
  ) => {
    try {
      mutateAsync(data)
        .then(() => {
          message.success({
            key: KEYS_MESSAGE.REGISTER,
            content: MESSAGE.announcement.register,
          });
          reset();
        })
        .catch((err) => {
          handleError(err);
        });
    } catch (error: any) {
      handleError(error);
    }
  };

  return (
    <div className="col-12 col-md-6">
      <div className="card card-lg">
        <div className="card-body max-sm:!py-4 max-sm:!px-4">
          <h6 className="mb-7">New Customer</h6>
          <Form onSubmitCapture={handleSubmit(onSubmit)}>
            <div className="row">
              <div className="col-12">
                <Field
                  control={control}
                  placeholder="Full Name *"
                  errors={errors.name?.message}
                  name={"name"}
                />
              </div>

              <div className="col-12">
                <Field
                  control={control}
                  placeholder="Email Address *"
                  errors={errors.username?.message}
                  name="username"
                />
              </div>

              <div className="col-12 col-md-6">
                <Field
                  control={control}
                  placeholder="Password *"
                  errors={errors.password?.message}
                  name="password"
                  type="password"
                />
              </div>

              <div className="col-12 col-md-6">
                <Field
                  control={control}
                  placeholder="Confirm Password *"
                  errors={errors.confirmPassword?.message}
                  name="confirmPassword"
                  type="password"
                />
              </div>

              <div className="col-12 col-md-auto">
                <div className="form-group font-size-sm text-muted">
                  By registering your details, you agree with our Terms &amp;
                  Conditions, and Privacy and Cookie Policy.
                </div>
              </div>
              <div className="col-12">
                <ButtonCustom disable={!isValid} loading={isLoading}>
                  Register
                </ButtonCustom>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Register;
