import { yupResolver } from "@hookform/resolvers/yup";
import { Form, message } from "antd";
import { signIn } from "next-auth/react";
import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import ButtonCustom from "src/components/Button";
import Field from "src/components/Field";
import ForgotPasswordModal from "src/components/ForgotPasswordModal";
import { ACCOUNT_DEMO } from "src/constants";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { PATH } from "src/constants/path";
import { useAppDispatch } from "src/hooks/app-hooks";
import { useCopyToClipboard } from "src/hooks/useCopyToClipboard";
import { getCartAction } from "src/store/cart/cart.action";
import { TypeLoginForm } from "src/types/auth";
import { REGEXP } from "src/utils/RegExp";
import * as yup from "yup";

interface State {
  loading: boolean;
  openModal: boolean;
}

const schema = yup
  .object({
    username: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.EMAIL, MESSAGE.validate.email)
      .test("emoji", MESSAGE.validate.emoji, (username: string): boolean => {
        if (!username) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(username);
      }),
    password: yup
      .string()
      .trim()
      .min(6, MESSAGE.validate.min)
      .max(32, MESSAGE.validate.max)
      .required(MESSAGE.validate.required),
  })
  .required();

const Login = () => {
  const dispatch = useAppDispatch();
  const [, copy] = useCopyToClipboard();

  const [state, setState] = useState<State>({
    loading: false,
    openModal: false,
  });

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypeLoginForm> = async (
    data: TypeLoginForm,
  ) => {
    try {
      setState((prev) => ({
        ...prev,
        loading: true,
      }));

      const res = await signIn("credentials", {
        ...data,
        redirect: false,
      });
      if (res?.ok) {
        dispatch(getCartAction());
        setState((prev) => ({
          ...prev,
          loading: false,
        }));
        window.location.replace(PATH.products);
        return;
      }

      message.error({
        key: KEYS_MESSAGE.LOGIN,
        content: res?.error,
      });

      setState((prev) => ({
        ...prev,
        loading: false,
      }));
    } catch (err) {
      message.error({
        key: KEYS_MESSAGE.LOGIN,
        content: err + "",
      });
    }
  };

  return (
    <div className="col-12 col-md-6">
      <div className="card card-lg mb-10 mb-md-0 max-sm:!mb-6">
        <div className="card-body max-sm:!py-4 max-sm:!px-4">
          <h6 className="mb-7">Returning Customer</h6>
          <Form onSubmitCapture={handleSubmit(onSubmit)}>
            <div className="row">
              <div className="col-12">
                <Field
                  control={control}
                  placeholder="Email Address *"
                  errors={errors.username?.message}
                  name="username"
                />
              </div>
              <div className="col-12">
                <Field
                  control={control}
                  placeholder="Password *"
                  errors={errors.password?.message}
                  name="password"
                  type="password"
                />
              </div>
              <div className="col-12 col-md">{/* Remember */}</div>
              <div className="col-12 col-md-auto">
                <div className="form-group">
                  <div
                    onClick={() =>
                      setState((prev) => ({
                        ...prev,
                        openModal: true,
                      }))
                    }
                    className="font-size-sm text-reset cursor-pointer"
                  >
                    Forgot Password?
                  </div>
                </div>
              </div>
              <div className="col-12">
                <ButtonCustom
                  loading={state.loading}
                  className="btn btn-sm btn-dark"
                >
                  Sign In
                </ButtonCustom>
              </div>
              <div className="col-12">
                <p className="font-size-sm text-muted mt-5 mb-2 font-light">
                  Account demo:{" "}
                  <b className="text-black">
                    <span
                      className="cursor-pointer underline"
                      onClick={() => copy(ACCOUNT_DEMO.username)}
                    >
                      {ACCOUNT_DEMO.username}
                    </span>
                    /
                    <span
                      className="cursor-pointer underline"
                      onClick={() => copy(ACCOUNT_DEMO.password)}
                    >
                      {ACCOUNT_DEMO.password}
                    </span>
                  </b>
                </p>
                <p className="font-size-sm text-muted mt-5 mb-2 font-light text-justify">
                  We provide you with a demo account for learning purposes, to
                  ensure others can use the joint account We will restrict a lot
                  of permissions on this account for example: <br />
                  - Do not change personal information or passwords <br />
                  - Do not reset password,... <br />
                  <br />
                  To be able to use all functions on the website, please proceed{" "}
                  <b className="text-black">register</b> with existing email
                  account Real
                </p>
              </div>
            </div>
          </Form>
          <ForgotPasswordModal
            isOpen={state.openModal}
            isCancel={() =>
              setState((prev) => ({
                ...prev,
                openModal: false,
              }))
            }
          />
        </div>
      </div>
    </div>
  );
};

export default Login;
