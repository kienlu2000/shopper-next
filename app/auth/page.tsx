"use client";

import { useBodyClass } from "src/hooks/useBodyClass";

import Login from "./Login";
import Register from "./Register";

const AuthPage = () => {
  useBodyClass("bg-light");

  return (
    <section className="py-12 max-sm:!py-8">
      <div className="container">
        <div className="row">
          <Login />
          <Register />
        </div>
      </div>
    </section>
  );
};

export default AuthPage;
