import { Metadata } from "next";
import { redirect } from "next/navigation";
import { getServerSession } from "next-auth";
import React from "react";
import { PATH } from "src/constants/path";
import authOptions from "src/helper/authOptions";

export const metadata: Metadata = {
  title: {
    default: "Authentication",
    template: "%s | Shopper ",
  },
  description: "Authentication's Shopper eCommerce Website",
};

const AuthLayout = async ({ children }: { children: React.ReactNode }) => {
  const session = await getServerSession(authOptions);

  if (session) {
    redirect(PATH.profile.index);
  }

  return <>{children}</>;
};

export default AuthLayout;
