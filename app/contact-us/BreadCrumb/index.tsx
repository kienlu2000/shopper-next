import Link from "next/link";
import React from "react";
import { PATH } from "src/constants/path";

const BreadCrumb = () => {
  return (
    <ol className="breadcrumb mb-0 font-size-xs text-gray-400">
      <li className="breadcrumb-item">
        <Link className="text-gray-400" href={PATH.home}>
          Home
        </Link>
      </li>
      <li className="breadcrumb-item active">Contact Us</li>
    </ol>
  );
};

export default BreadCrumb;
