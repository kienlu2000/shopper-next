"use client";

import { yupResolver } from "@hookform/resolvers/yup";
import { Form } from "antd";
import { SubmitHandler, useForm } from "react-hook-form";
import ButtonCustom from "src/components/Button";
import Field from "src/components/Field";
import { MESSAGE } from "src/constants/message";
import { useMutationContactUs } from "src/mutate/organization/mutationHooksCommon";
import { TypeContactForm } from "src/types/organization";
import { REGEXP } from "src/utils/RegExp";
import * as yup from "yup";

import BreadCrumb from "./BreadCrumb";
import InformationSide from "./InformationSide";

const schema = yup
  .object({
    name: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.SPECIAL_CHARACTER, MESSAGE.validate.specialCharacter)
      .test("emoji", MESSAGE.validate.emoji, (name: string): boolean => {
        if (!name) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(name);
      }),
    email: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.EMAIL, MESSAGE.validate.email)
      .test("emoji", MESSAGE.validate.emoji, (username: string): boolean => {
        if (!username) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(username);
      }),
    phone: yup
      .string()
      .trim()
      .matches(REGEXP.PHONE, MESSAGE.validate.phone)
      .required(MESSAGE.validate.required),
    website: yup
      .string()
      .trim()
      .matches(REGEXP.FACEBOOK, MESSAGE.validate.facebook)
      .required(MESSAGE.validate.required),
    title: yup.string().trim().required(MESSAGE.validate.required),
    content: yup.string().trim().required(MESSAGE.validate.required),
  })
  .required();

const ContactUsPage = () => {
  const { mutate, isLoading } = useMutationContactUs();

  const {
    handleSubmit,
    control,
    formState: { errors, isValid },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypeContactForm> = async (
    data: TypeContactForm,
  ) => {
    mutate(data);
  };

  return (
    <div>
      <nav className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <BreadCrumb />
            </div>
          </div>
        </div>
      </nav>
      <section className="pt-7 pb-12">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <h3 className="mb-10 text-center max-sm:!mb-6">Contact Us</h3>
            </div>
          </div>
          <div className="row justify-content-between">
            <InformationSide />
            <div className="col-12 col-md-8">
              <Form onSubmitCapture={handleSubmit(onSubmit)}>
                <Field
                  errors={errors.name?.message}
                  name={"name"}
                  control={control}
                  label="Full Name *"
                  placeholder="Full Name"
                />
                <Field
                  errors={errors.email?.message}
                  name={"email"}
                  control={control}
                  label="Email *"
                  placeholder="Email"
                />
                <Field
                  errors={errors.phone?.message}
                  name={"phone"}
                  control={control}
                  label="Number Phone *"
                  placeholder="Number Phone"
                />
                <Field
                  errors={errors.website?.message}
                  name={"website"}
                  control={control}
                  label="URL Facebook *"
                  placeholder="URL Facebook"
                />
                <Field
                  errors={errors.title?.message}
                  name={"title"}
                  control={control}
                  label="Title *"
                  placeholder="Title"
                />
                <Field
                  errors={errors.content?.message}
                  name={"content"}
                  control={control}
                  label="Content *"
                  placeholder="Content"
                  renderField={(props: any) => (
                    <textarea
                      {...props}
                      className="form-control form-control-sm"
                      id="contactMessage"
                      style={{ width: "100%" }}
                      rows={5}
                    />
                  )}
                />
                <ButtonCustom loading={isLoading} disable={!isValid}>
                  Submit
                </ButtonCustom>
              </Form>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default ContactUsPage;
