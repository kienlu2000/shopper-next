import { Metadata } from "next";

export const metadata: Metadata = {
  title: {
    default: "Contact Us",
    template: "%s | Shopper ",
  },
  description: "Contact Us's Shopper eCommerce Website",
};

export default async function ContactUsLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return <>{children}</>;
}
