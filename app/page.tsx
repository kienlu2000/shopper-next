"use client";

import BannerCategory from "./homepage/BannerCategory";
import BestPicks from "./homepage/BestPicks";
import Brand from "./homepage/Brand";
import CountDown from "./homepage/CountDown";
import FeedBacks from "./homepage/FeedBacks";
import Policy from "./homepage/Policy";
import PromotionSale from "./homepage/PromotionSale";
import SellingProduct from "./homepage/SellingProduct";

const Homepage = () => {
  return (
    <main>
      <PromotionSale />

      <BannerCategory />

      <Policy />

      <BestPicks />

      <SellingProduct />

      <CountDown />

      <FeedBacks />

      <Brand />
    </main>
  );
};

export default Homepage;
