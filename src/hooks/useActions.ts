import { message } from "antd";
import { useId, useRef } from "react";

import { handleError } from "../utils/handleError";

interface Props {
  action: () => void;
  messageSuccess?: string;
  messageLoading: string;
  onSuccess?: () => void;
}

export const useAction = ({
  action,
  messageLoading,
  messageSuccess,
  onSuccess,
}: Props) => {
  const loadingRef = useRef(false);
  const key = useId();
  const onAction = async (...data: []) => {
    if (loadingRef.current) return;

    loadingRef.current = true;
    try {
      message.loading({
        content: messageLoading,
        key,
        duration: 1,
      });
      await action(...data);
      if (messageSuccess) {
        message.success({
          key,
          content: messageSuccess,
          duration: 1,
        });
      } else {
        message.destroy(key);
      }
      onSuccess?.();
    } catch (err: any) {
      handleError(err);
    }
    loadingRef.current = false;
  };

  return onAction;
};
