"use client";

import { useEffect } from "react";

export const useScrollToTop = () => {
  useEffect(() => {
    window.scroll({
      top: 0,
      behavior: "smooth",
    });
  }, []);
};
