import { useRouter, useSearchParams } from "next/navigation";
import queryString from "query-string";

type TypeDefaultValue = {
  sort?: string;
  page: number;
  minPrice?: string;
  maxPrice?: string;
  rating_average?: string;
  [key: string]: string | number | undefined;
};

export const useSearch = (
  defaultValue: TypeDefaultValue = {
    page: 1,
    sort: "",
    maxPrice: "",
    minPrice: "",
    rating_average: "",
  },
) => {
  const router = useRouter();

  const search = useSearchParams();

  let value = { ...defaultValue };

  for (const [key, val] of search.entries()) {
    if (val !== undefined) {
      try {
        value[key] = JSON.parse(val);
      } catch (err) {
        value[key] = val;
      }
    }
  }

  const setValue = (valueObj: TypeDefaultValue) => {
    const qs = queryString.stringify({ ...value, ...valueObj });

    value = { ...value, ...valueObj };
    router.push(`?${qs}`);
  };

  return [value, setValue] as const;
};
