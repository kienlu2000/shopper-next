import { selectCategories } from "src/store/categories";

import { useAppSelector } from "./app-hooks";

export const useCategory = (id: number) => {
  const data = useAppSelector(selectCategories);

  return data.find((e) => e.id === id);
};
