import { usePathname } from "next/navigation";

const useBreadcrumbPath = () => {
  const pathname = usePathname();

  const removeQuestionMark = pathname.replace(/\?/g, "/");
  const removeEquals = removeQuestionMark.replace(/\=/g, "/");

  return removeEquals.split("/");
};

export default useBreadcrumbPath;
