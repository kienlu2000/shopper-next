/* eslint-disable no-unused-vars */

import { message } from "antd";
import { useState } from "react";

type CopiedValue = string | null;
type CopyFn = (text: string) => Promise<boolean>;

export function useCopyToClipboard(): [CopiedValue, CopyFn] {
  const [copiedText, setCopiedText] = useState<CopiedValue>(null);

  const copy: CopyFn = async (text) => {
    if (!navigator?.clipboard) {
      message.warning("Clipboard Not Supported");
      return false;
    }

    try {
      await navigator.clipboard.writeText(text);
      setCopiedText(text);
      message.info("Copy To Clipboard Success");
      return true;
    } catch (error) {
      message.error("Copy Failed");
      setCopiedText(null);
      return false;
    }
  };

  return [copiedText, copy];
}
