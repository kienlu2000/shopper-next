import { useQuery } from "@tanstack/react-query";

import { getShippingMethod } from "./queryFnsCommon";
import { QueryKeysCommon } from "./queryKeysCommon";

export const useShippingMethod = () =>
  useQuery({
    queryFn: () => getShippingMethod(),
    queryKey: [QueryKeysCommon.SHIPPING_METHOD],
    cacheTime: 3600000,
  });
