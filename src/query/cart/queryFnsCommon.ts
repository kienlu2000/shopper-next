import { cartService } from "src/service/cart";

export const getShippingMethod = async () => {
  const { data: dataShipping } = await cartService.getMethodShipping();

  return dataShipping;
};
