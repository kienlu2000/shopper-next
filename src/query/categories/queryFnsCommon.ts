import { productService } from "src/service/products";

// export const getCategoriesQuery = async () => {
//   const { data: dataCategories } = await productService.getCategories();

//   return dataCategories;
// };

export const getCategoriesDetailQuery = async (categories: number) => {
  const { data: dataCategoriesDetail } =
    await productService.getCategoriesDetail(categories);

  return dataCategoriesDetail;
};
