import { useQuery } from "@tanstack/react-query";
import { TypeProductDetail } from "src/types/products";

import { getCategoriesDetailQuery } from "./queryFnsCommon";
import { QueryCategoriesKeysCommon } from "./queryKeysCommon";

interface PropsCategoriesDetail {
  categories: number;
  enabled: TypeProductDetail;
}

// export const useQueryCategories = (enabled?: boolean) =>
//   useQuery({
//     queryFn: () => getCategoriesQuery(),
//     queryKey: [QueryCategoriesKeysCommon.CATEGORIES],
//     cacheTime: 3600_000,
//     enabled,
//   });

// export const useCategory = (id: number) => {
//   const { data } = useQuery<TypeCategoriesDetail[]>([
//     QueryCategoriesKeysCommon.CATEGORIES,
//   ]);

//   return data?.find((e: TypeCategoriesDetail) => e.id === id);
// };

export const useQueryCategoriesDetail = ({
  categories,
  enabled,
}: PropsCategoriesDetail) =>
  useQuery({
    queryFn: () => getCategoriesDetailQuery(categories),
    queryKey: [QueryCategoriesKeysCommon.CATEGORIES_DETAIL],
    enabled: !!enabled,
  });
