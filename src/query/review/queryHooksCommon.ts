import { useQuery } from "@tanstack/react-query";

import { getReviewsQueryFn, getTopReviewFn } from "./queryFnsCommon";
import { QueryKeysCommon } from "./queryKeysCommon";

export const useQueryReviews = (productId: number, query: string) =>
  useQuery({
    queryFn: () => getReviewsQueryFn(productId, `${query}`),
    queryKey: [QueryKeysCommon.REVIEW, query, productId],
  });

export const useQueryTopReviews = () =>
  useQuery({
    queryFn: () => getTopReviewFn(),
    queryKey: [QueryKeysCommon.TOP_REVIEW],
  });
