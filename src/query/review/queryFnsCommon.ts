import { reviewService } from "src/service/review";

export const getReviewsQueryFn = async (productId: number, query: string) => {
  const data = await reviewService.getReview(productId, `?${query}`);

  return data;
};

export const getTopReviewFn = async () => {
  const { data: dataTopReview } = await reviewService.getTopReview();

  return dataTopReview;
};
