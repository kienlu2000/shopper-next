import { useQuery } from "@tanstack/react-query";

import { getProductsDetailQueryFn, getProductsQueryFn } from "./queryFnsCommon";
import { QueryKeysCommon } from "./queryKeysCommon";

export const useQueryProducts = (query: string) =>
  useQuery({
    queryFn: () => getProductsQueryFn(query),
    queryKey: [QueryKeysCommon.PRODUCTS, query],
    keepPreviousData: true,
  });

export const useQueryProductDetails = (id: number) =>
  useQuery({
    queryFn: () => getProductsDetailQueryFn(id),
    queryKey: [QueryKeysCommon.PRODUCTS_DETAILS, id],
    enabled: !!id,
  });
