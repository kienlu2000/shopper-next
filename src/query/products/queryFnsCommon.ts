import { productService } from "src/service/products";

export const getProductsQueryFn = async (query: string) => {
  const data = await productService.getProduct(`${query}`);

  return data;
};

export const getProductsDetailQueryFn = async (id: number) => {
  const { data: dataProductDetails } =
    await productService.getProductDetail(id);

  return dataProductDetails;
};
