import { useQuery } from "@tanstack/react-query";

import {
  getAddressDetailUserQuery,
  getAddressUserQuery,
} from "./queryFnsCommon";
import { QueryKeysCommon } from "./queryKeysCommon";

export const useAddressUser = (query: string) => {
  return useQuery({
    queryFn: () => getAddressUserQuery(query),
    queryKey: [QueryKeysCommon.ADDRESS],
  });
};

export const useAddressDetailUser = (id: string) => {
  return useQuery({
    queryFn: () => getAddressDetailUserQuery(id),
    queryKey: [QueryKeysCommon.ADDRESS_DETAIL, id],
    keepPreviousData: false,
    enabled: !!id,
    retry: 1,
  });
};
