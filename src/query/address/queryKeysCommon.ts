/* eslint-disable no-unused-vars */
export enum QueryKeysCommon {
  ADDRESS = "ADDRESS",
  ADDRESS_DEFAULT = "ADDRESS_DEFAULT",
  ADDRESS_DETAIL = "ADDRESS_DETAIL",
}
