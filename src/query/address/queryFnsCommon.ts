import { userService } from "src/service/user";

export const getAddressUserQuery = async (query: string) => {
  const { data: dataAddressUser } = await userService.getAddressUser(query);

  return dataAddressUser;
};

export const getAddressDetailUserQuery = async (id: string) => {
  const { data: dataAddressUser } = await userService.getAddressDetailUser(id);

  return dataAddressUser;
};
