import { useQuery } from "@tanstack/react-query";

import {
  getPaymentDetailUserQuery,
  getPaymentUserQuery,
} from "./queryFnsCommon";
import { QueryKeysCommon } from "./queryKeysCommon";

export const usePaymentUser = (query: string) => {
  return useQuery({
    queryFn: () => getPaymentUserQuery(query),
    queryKey: [QueryKeysCommon.PAYMENT],
  });
};

export const usePaymentDetailUser = (id: string) => {
  return useQuery({
    queryFn: () => getPaymentDetailUserQuery(id),
    queryKey: [QueryKeysCommon.PAYMENT_DETAIL, id],
    keepPreviousData: false,
    enabled: !!id,
    retry: 1,
  });
};
