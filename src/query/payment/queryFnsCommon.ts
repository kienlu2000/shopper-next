import { userService } from "src/service/user";

export const getPaymentUserQuery = async (query: string) => {
  const { data: dataPayment } = await userService.getPaymentUser(query);

  return dataPayment;
};

export const getPaymentDetailUserQuery = async (id: string) => {
  const { data: dataPaymentDetail } = await userService.getPaymentDetail(id);

  return dataPaymentDetail;
};
