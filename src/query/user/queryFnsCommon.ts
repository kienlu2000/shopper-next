import { userService } from "src/service/user";

export const getProfileUserQueryFn = async () => {
  const { data: dataUser } = await userService.getProfile();

  return dataUser;
};
