import { useQuery } from "@tanstack/react-query";

import { getProfileUserQueryFn } from "./queryFnsCommon";
import { QueryKeysUserCommon } from "./queryKeysCommon";

export const useQueryProfileUser = (isAuthentic: boolean) =>
  useQuery({
    queryFn: () => getProfileUserQueryFn(),
    queryKey: [QueryKeysUserCommon.PROFILE_USER],
    enabled: isAuthentic,
  });
