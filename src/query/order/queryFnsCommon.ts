import { orderService } from "src/service/order";

export const getAllOrder = async (query: string) => {
  const data = await orderService.getOrder(`${query}`);

  return data;
};

export const getOrderDetail = async (id: string) => {
  const { data: dataOrderDetail } = await orderService.getOrderDetail(id);

  return dataOrderDetail;
};

export const getCountOrder = async (query: string) => {
  const data = await orderService.count(`${query}`);

  return data.count;
};
