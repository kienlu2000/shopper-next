/* eslint-disable no-unused-vars */
export enum QueryKeysCommon {
  ALL_ORDER = "ALL_ORDER",
  ORDER_DETAIL = "ORDER_DETAIL",
  COUNT_ORDER = "COUNT_ORDER",
}
