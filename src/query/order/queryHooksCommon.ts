import { useQuery } from "@tanstack/react-query";
import { initialOrderDetailValue } from "src/constants/initialValue";

import { getAllOrder, getCountOrder, getOrderDetail } from "./queryFnsCommon";
import { QueryKeysCommon } from "./queryKeysCommon";

export const useQueryOrder = (query: string) =>
  useQuery({
    queryFn: () => getAllOrder(query),
    queryKey: [QueryKeysCommon.ALL_ORDER, query],
  });

export const useQueryOrderDetail = (id: string) =>
  useQuery({
    queryFn: () => getOrderDetail(id),
    queryKey: [QueryKeysCommon.ORDER_DETAIL, id],
    initialData: initialOrderDetailValue,
  });

export const useQueryCountOrder = (query: string) =>
  useQuery({
    queryFn: () => getCountOrder(query),
    queryKey: [QueryKeysCommon.COUNT_ORDER, query],
  });
