import { productService } from "src/service/products";

export const getWishlistQuery = async (query: string) => {
  const data = await productService.getWishList(`${query}`);

  return data;
};
