import { useQuery } from "@tanstack/react-query";

import { getWishlistQuery } from "./queryFnsCommon";
import { QueryKeysCommon } from "./queryKeysCommon";

export const useQueryWishlist = (query: string) =>
  useQuery({
    queryFn: () => getWishlistQuery(query),
    queryKey: [QueryKeysCommon.WISHLIST, query],
    keepPreviousData: true,
  });
