import { useMutation } from "@tanstack/react-query";

import { checkoutMutationFn } from "./mutationFnsCommon";
import { MutationKeysCommon } from "./mutationKeysCommon";

export const useMutationCheckout = () => {
  return useMutation({
    mutationFn: checkoutMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_CHECKOUT],
  });
};
