import { cartService } from "src/service/cart";
import { TypeFormCheckout } from "src/types/checkout";

export const checkoutMutationFn = async (form: TypeFormCheckout) => {
  const { data: dataCheckout } = await cartService.checkout(form);

  return dataCheckout;
};
