import { userService } from "src/service/user";
import {
  TypeChangePasswordByCodeForm,
  TypeChangePasswordForm,
  TypeRegisterForm,
  TypeSendEmailResetPasswordForm,
  TypeUpdateProfileForm,
} from "src/types/user";

export const registerUserMutationFn = async (form: TypeRegisterForm) => {
  // try {
  //   const data = await userService.signup(form);
  //   return data;
  // } catch (error) {
  //   const _error = error as TypeError;

  //   throw new Error(_error.response?.data?.message);
  // }
  const data = await userService.signup(form);

  return data;
};

export const updateProfileUserMutationFn = async (
  form: TypeUpdateProfileForm,
) => {
  const data = await userService.updateInfo(form);

  return data;
};

export const changePasswordUserMutationFn = async (
  form: TypeChangePasswordForm,
) => {
  const data = await userService.changePassword(form);

  return data;
};

export const sendEmailResetPasswordMutationFn = async (
  form: TypeSendEmailResetPasswordForm,
) => {
  const data = await userService.sendEmailResetPassword(form);

  return data;
};

export const changePasswordByCodeMutationFn = async (
  form: TypeChangePasswordByCodeForm,
) => {
  const data = await userService.changePasswordByCode(form);

  return data.data;
};
