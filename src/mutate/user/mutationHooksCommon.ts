import { useMutation } from "@tanstack/react-query";
import { message } from "antd";
import { useRouter } from "next/navigation";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { PATH } from "src/constants/path";
import { handleError } from "src/utils/handleError";

import {
  changePasswordByCodeMutationFn,
  changePasswordUserMutationFn,
  registerUserMutationFn,
  sendEmailResetPasswordMutationFn,
  updateProfileUserMutationFn,
} from "./mutationFnsCommon";
import { MutationKeysCommon } from "./mutationKeysCommon";

export const useMutationRegisterUser = () => {
  return useMutation({
    mutationFn: registerUserMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_REGISTER_USER],
  });
};

export const useMutationUpdateProfileUser = () => {
  return useMutation({
    mutationFn: updateProfileUserMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_UPDATE_PROFILE_USER],
  });
};

export const useMutationChangePasswordUser = () => {
  return useMutation({
    mutationFn: changePasswordUserMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_CHANGE_PASSWORD_USER],
  });
};

export const useMutationSendEmailResetPassword = () => {
  return useMutation({
    mutationFn: sendEmailResetPasswordMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_SEND_EMAIL_RESET_PASSWORD],
  });
};

export const useMutationChangePasswordByCode = () => {
  const router = useRouter();

  return useMutation({
    mutationFn: changePasswordByCodeMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_CHANGE_PASSWORD_BY_CODE],
    onSuccess: () => {
      router.push(PATH.authentication);
      message.success({
        key: KEYS_MESSAGE.CHANGE_PASSWORD_BY_CODE,
        content: MESSAGE.changePassword.success,
      });
    },
    onError: (error: any) => {
      handleError(error);
    },
  });
};
