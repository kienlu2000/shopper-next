import { userService } from "src/service/user";
import { TypeFormEditAddressUser } from "src/types/user";

interface Props {
  id: string;
  form: TypeFormEditAddressUser;
}

export const editAddressMutationFn = async ({ form, id }: Props) => {
  const data = await userService.editAddressUser(id, form);

  return data;
};

export const addAddressMutationFn = async (form: TypeFormEditAddressUser) => {
  const data = await userService.addAddressUser(form);

  return data;
};
