import { useMutation } from "@tanstack/react-query";

import {
  addAddressMutationFn,
  editAddressMutationFn,
} from "./mutationFnsCommon";
import { MutationKeysCommon } from "./mutationKeysCommon";

export const useMutationEditAddress = () => {
  return useMutation({
    mutationFn: editAddressMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_EDIT_ADDRESS],
  });
};

export const useMutationAddAddress = () => {
  return useMutation({
    mutationFn: addAddressMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_ADD_ADDRESS],
  });
};
