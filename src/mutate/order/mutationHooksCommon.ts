import { useMutation } from "@tanstack/react-query";

import { changeStatusOrderMutationFn } from "./mutationFnsCommon";
import { MutationKeysCommon } from "./mutationKeysCommon";

export const useMutationChangeStatusOrder = () => {
  return useMutation({
    mutationFn: changeStatusOrderMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_CHANGE_STATUS_ORDER],
  });
};
