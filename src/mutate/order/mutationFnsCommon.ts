import { orderService } from "src/service/order";
import { TypeActionChangeStatusOrder } from "src/types/order";

interface Props {
  id: string;
  action: TypeActionChangeStatusOrder;
}

export const changeStatusOrderMutationFn = async ({ action, id }: Props) => {
  const data = await orderService.changeOrderStatus(id, action);

  return data;
};
