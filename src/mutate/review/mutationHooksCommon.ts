import { useMutation } from "@tanstack/react-query";

import { addReviewMutationFn } from "./mutationFnsCommon";
import { MutationKeysCommon } from "./mutationKeysCommon";

export const useMutationAddReview = () => {
  return useMutation({
    mutationFn: addReviewMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_ADD_REVIEW],
  });
};
