import { reviewService } from "src/service/review";
import { TypeAddReviewForm } from "src/types/review";

interface Props {
  productId: number;
  form: TypeAddReviewForm;
}

export const addReviewMutationFn = async ({ form, productId }: Props) => {
  const data = await reviewService.newReview(productId, form);

  return data.data;
};
