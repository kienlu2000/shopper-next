import { useMutation } from "@tanstack/react-query";

import {
  addProductToWishlistMutationFn,
  removeProductWishlistMutationFn,
} from "./mutationFnsCommon";
import { MutationKeysCommon } from "./mutationKeysCommon";

export const useMutationAddProductToWishlist = () => {
  return useMutation({
    mutationFn: addProductToWishlistMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_ADD_PRODUCT_WISHLIST],
  });
};

export const useMutationRemoveProductToWishlist = () => {
  return useMutation({
    mutationFn: removeProductWishlistMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_REMOVE_PRODUCT_WISHLIST],
  });
};
