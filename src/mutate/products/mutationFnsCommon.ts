import { productService } from "src/service/products";

export const addProductToWishlistMutationFn = async (productId: number) => {
  const data = await productService.addWishList(productId);

  return data;
};

export const removeProductWishlistMutationFn = async (productId: number) => {
  const data = await productService.deleteWishList(productId);

  return data;
};
