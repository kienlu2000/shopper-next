import { useMutation } from "@tanstack/react-query";

import { uploadFileMutationFn } from "./mutationFnsCommon";
import { MutationKeysCommon } from "./mutationKeysCommon";

export const useMutationUploadFile = () => {
  return useMutation({
    mutationFn: uploadFileMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_UPLOAD_FILE],
  });
};
