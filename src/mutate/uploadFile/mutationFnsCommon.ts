import { fileService } from "src/service/service";

export const uploadFileMutationFn = async (file: File) => {
  const data = await fileService.uploadFile(file);

  return data;
};
