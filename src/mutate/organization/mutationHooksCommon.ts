import { useMutation } from "@tanstack/react-query";
import { message } from "antd";
import { useRouter } from "next/navigation";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { PATH } from "src/constants/path";
import { TypeErrorResponsive } from "src/types";

import { contactUsMutationFn } from "./mutationFnsCommon";
import { MutationKeysCommon } from "./mutationKeysCommon";

export const useMutationContactUs = () => {
  const router = useRouter();
  return useMutation({
    mutationFn: contactUsMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_CONTACT_US],
    onSuccess: () => {
      router.push(PATH.home);
      message.success({
        key: KEYS_MESSAGE.CONTACT_US,
        content: MESSAGE.announcement.contactUs,
      });
    },
    onError: (error: TypeErrorResponsive) => {
      message.error({
        key: KEYS_MESSAGE.CONTACT_US,
        content: error.response?.data.message,
      });
    },
  });
};
