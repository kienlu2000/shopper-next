import { organizationService } from "src/service/organization";
import { TypeContactForm } from "src/types/organization";

export const contactUsMutationFn = async (form: TypeContactForm) => {
  const data = await organizationService.contact(form);

  return data;
};
