import { useMutation } from "@tanstack/react-query";

import {
  addPaymentMutationFn,
  editPaymentMutationFn,
} from "./mutationFnsCommon";
import { MutationKeysCommon } from "./mutationKeysCommon";

export const useMutationEditPayment = () => {
  return useMutation({
    mutationFn: editPaymentMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_EDIT_PAYMENT],
  });
};

export const useMutationAddPayment = () => {
  return useMutation({
    mutationFn: addPaymentMutationFn,
    mutationKey: [MutationKeysCommon.MUTATION_ADD_PAYMENT],
  });
};
