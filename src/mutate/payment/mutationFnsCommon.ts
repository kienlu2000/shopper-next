import { userService } from "src/service/user";
import { TypeFormEditPaymentUser } from "src/types/payment";

interface Props {
  id: string;
  form: TypeFormEditPaymentUser;
}

export const editPaymentMutationFn = async ({ form, id }: Props) => {
  const data = await userService.editPaymentUser(id, form);

  return data;
};

export const addPaymentMutationFn = async (form: TypeFormEditPaymentUser) => {
  const data = await userService.addPaymentUser(form);

  return data;
};
