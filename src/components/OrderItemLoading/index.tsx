import React from "react";

import SkeletonCustom from "../SkeletonCustom";

const OrderItemLoading = () => {
  return (
    <div className="card card-lg mb-5 border">
      <div className="card-body pb-0">
        <div className="card card-sm">
          <SkeletonCustom height={88.75} />
        </div>
      </div>
      <div className="card-footer">
        <div className="row align-items-center">
          <div className="col-12 col-lg-6">
            <SkeletonCustom height={59.72} />
          </div>
          <div className="col-12 col-lg-6 max-sm:mt-4">
            <div className="flex justify-end gap-3">
              <SkeletonCustom height={40.49} width={114.5} />
              <SkeletonCustom height={40.49} width={66.04} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrderItemLoading;
