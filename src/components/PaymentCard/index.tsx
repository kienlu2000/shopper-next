import { message, Popconfirm } from "antd";
import moment from "moment";
import Link from "next/link";
import React, { useRef } from "react";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { PATH } from "src/constants/path";
import { userService } from "src/service/user";
import { TypePaymentItem } from "src/types/payment";
import { handleError } from "src/utils/handleError";

import ButtonCustom from "../Button";
import { PaymentCardStyle } from "./style.index";

interface Props extends TypePaymentItem {
  refetch?: () => void;
}

const PaymentCard = ({
  cardNumber,
  type,
  cardName,
  refetch,
  expired,
  default: paymentDefault,
  _id,
}: Props) => {
  const loadingDefaultRef = useRef<boolean>(false);
  const loadingDeleteRef = useRef<boolean>(false);

  const t = expired.split("/");
  const month = t[0];
  const year = t[1];

  const LinkToPaymentDetail = PATH.profile.editPayment + "/" + _id;

  const handleChangeDefaultAddress = async () => {
    if (loadingDefaultRef.current) return;

    loadingDefaultRef.current = true;
    try {
      const key = KEYS_MESSAGE.DEFAULT_PAYMENT(_id);
      message.loading({
        key,
        content: MESSAGE.LOADING,
      });

      await userService.editPaymentUser(_id, {
        default: true,
      });
      refetch?.();
      message.success({
        key,
        content: MESSAGE.payment.changeSuccess,
      });
    } catch (err: any) {
      handleError(err);
    }
    loadingDefaultRef.current = false;
  };

  const handleRemoveAddress = async () => {
    if (loadingDeleteRef.current) return;
    loadingDeleteRef.current = true;
    try {
      const key = KEYS_MESSAGE.REMOVE_PAYMENT(_id);
      message.loading({
        key,
        content: MESSAGE.LOADING,
      });
      await userService.deletePaymentUser(_id);
      refetch?.();
      message.success({
        key,
        content: MESSAGE.payment.deleted,
      });
    } catch (err: any) {
      handleError(err);
    }
    loadingDeleteRef.current = false;
  };

  return (
    <PaymentCardStyle className="col-12">
      <div className="address-card card card-lg bg-light mb-8">
        <div className="card-body">
          <h6 className="mb-6">
            {type === "card" ? "Debit / Credit Card" : "Paypal"}
          </h6>
          <p className="mb-5">
            <strong>Card Number: </strong>
            <span className="text-muted">{cardNumber}</span>
          </p>
          <p className="mb-5">
            <strong>Expiry Date: </strong>
            <span className="text-muted">
              {moment(`${month}/01/${year}`).format("MMM, YYYY")}
            </span>
          </p>
          <p className="mb-0">
            <strong>Name on Card: </strong>
            <span className="text-muted">{cardName}</span>
          </p>

          {paymentDefault ? (
            <div className="card-action-right-bottom select-none">
              <div className="link color-success">Default address</div>
            </div>
          ) : (
            <div className="card-action-right-bottom hidden">
              <ButtonCustom
                type="outline"
                size="xs"
                onClick={handleChangeDefaultAddress}
              >
                Set as default address
              </ButtonCustom>
            </div>
          )}

          <div className="card-action card-action-right flex gap-2">
            <Link
              href={LinkToPaymentDetail}
              className="btn btn-xs btn-circle btn-white-primary"
            >
              <i className="fe fe-edit-2" />
            </Link>
            {!paymentDefault && (
              <Popconfirm
                placement="topRight"
                title="Warning"
                showCancel={false}
                okButtonProps={{ hidden: true }}
                description={
                  <div>
                    This operation will be non-refundable, are you sure you want
                    to perform this operation?
                    <div className="flex justify-end gap-2 pt-5">
                      <ButtonCustom onClick={handleRemoveAddress}>
                        Still Delete
                      </ButtonCustom>
                    </div>
                  </div>
                }
              >
                <button className="btn btn-xs btn-circle btn-white-primary">
                  <i className="fe fe-x"></i>
                </button>
              </Popconfirm>
            )}
          </div>
        </div>
      </div>
    </PaymentCardStyle>
  );
};

export default PaymentCard;
