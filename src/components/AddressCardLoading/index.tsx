import React from "react";

import SkeletonCustom from "../SkeletonCustom";

const AddressCardLoading = () => {
  return (
    <div className="card card-lg bg-light mb-8" style={{ height: 274 }}>
      <div className="card-body">
        <div className="flex-col flex gap-5 font-size-sm mb-0 leading-[35px]">
          <SkeletonCustom width="65%" height={27} />
          <SkeletonCustom width="40%" height={20} />
          <SkeletonCustom width="54%" height={20} />
          <SkeletonCustom width="35%" height={20} />
          <SkeletonCustom width="35%" height={20} />
          <SkeletonCustom width="25%" height={20} />
        </div>
      </div>
    </div>
  );
};

export default AddressCardLoading;
