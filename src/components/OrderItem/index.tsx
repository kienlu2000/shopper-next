import { message } from "antd";
import moment from "moment";
import React, { useMemo } from "react";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { PATH } from "src/constants/path";
import { useMutationChangeStatusOrder } from "src/mutate/order/mutationHooksCommon";
import { TypeOrderItem } from "src/types/order";
import { handleError } from "src/utils/handleError";

import ButtonCustom from "../Button";
import OrderItemStatus from "../OrderItemStatus";

interface Props extends TypeOrderItem {
  refetch: () => void;
}

const OrderItem = (props: Props) => {
  const { _id, status, listItems, finishedDate, refetch } = props;
  const moreImage = listItems.slice(3);

  const LinkToDetail = PATH.profile.order + "/" + _id;

  const { mutateAsync, isLoading } = useMutationChangeStatusOrder();

  const onCancelOrder = () => {
    try {
      mutateAsync({
        action: {
          status: "cancel",
        },
        id: _id,
      })
        .then(() => {
          message.success({
            key: KEYS_MESSAGE.CHANGE_STATUS_ORDER,
            content: MESSAGE.order.cancelOrder,
          });
          refetch();
        })
        .catch(handleError);
    } catch (error: any) {
      handleError(error);
    }
  };

  const checkoutReturn = useMemo(() => {
    if (status === "finished") {
      return moment(finishedDate) > moment().add(-7, "d");
    }
  }, []);

  return (
    <div className="card card-lg mb-5 border">
      <div className="card-body pb-0">
        <div className="card card-sm">
          <OrderItemStatus {...props} />
        </div>
      </div>
      <div className="card-footer">
        <div className="row align-items-center">
          <div className="col-12 col-lg-6">
            <div className="form-row mb-4 mb-lg-0">
              {listItems.slice(0, 3).map((e) => (
                <div key={e.productId} className="col-3">
                  <div
                    className="embed-responsive embed-responsive-1by1 bg-cover"
                    style={{
                      backgroundImage: `url(${e.product.thumbnail_url})`,
                    }}
                  />
                </div>
              ))}
              {moreImage.length > 0 && (
                <div className="col-3">
                  <div className="embed-responsive embed-responsive-1by1 bg-light">
                    <div className="embed-responsive-item embed-responsive-item-text text-reset">
                      <div className="font-size-xxs font-weight-bold">
                        +{moreImage.length} <br /> ảnh
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
          <div className="col-12 col-lg-6">
            <div className="flex justify-end gap-3">
              {["finished", "cancel"].includes(status) && (
                <ButtonCustom size="xs" type="outline">
                  Repurchase
                </ButtonCustom>
              )}
              {status === "finished" && checkoutReturn && (
                <ButtonCustom size="xs" type="outline" link="#">
                  Returns
                </ButtonCustom>
              )}
              {status === "pending" && (
                <ButtonCustom
                  onClick={() => onCancelOrder()}
                  loading={isLoading}
                  type="outline"
                  size="xs"
                  link="#"
                >
                  Cancel Order
                </ButtonCustom>
              )}
              <ButtonCustom size="xs" type="outline" link={LinkToDetail}>
                Detail
              </ButtonCustom>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrderItem;
