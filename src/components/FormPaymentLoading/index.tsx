import React from "react";

import SkeletonCustom from "../SkeletonCustom";

const FormPaymentLoading = () => {
  return (
    <form>
      <div className="row">
        <div className="col-12 col-md-6">
          <div className="form-group">
            <label>
              <SkeletonCustom height={22} width={137} />
            </label>
            <SkeletonCustom height={50.5} />
          </div>
        </div>

        <div className="col-12 col-md-6">
          <div className="form-group">
            <label>
              <SkeletonCustom height={22} width={137} />
            </label>
            <SkeletonCustom height={50.5} />
          </div>
        </div>
        <div className="col-12">
          <label>
            <SkeletonCustom height={22} width={137} />
          </label>
        </div>
        <div className="col-12 col-md-4">
          <div className="form-group">
            <SkeletonCustom height={50.5} />
          </div>
        </div>
        <div className="col-12 col-md-4">
          <div className="form-group">
            <SkeletonCustom height={50.5} />
          </div>
        </div>
        <div className="col-12 col-md-4">
          <div className="form-group">
            <SkeletonCustom height={50.5} />
          </div>
        </div>
      </div>
    </form>
  );
};

export default FormPaymentLoading;
