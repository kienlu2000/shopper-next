import { Rate } from "antd";
import moment from "moment";
import Link from "next/link";
import React from "react";
import { PATH } from "src/constants/path";
import { useCategory } from "src/hooks/useCategory";
import { TypeTopReviewItem } from "src/types/review";
import { slugify, stringToPath } from "src/utils/stringToPath";

import { ReviewCardStyle } from "./style.index";

const ReviewCard = ({
  product,
  content,
  user,
  createdAt,
  star,
}: TypeTopReviewItem) => {
  const categories = useCategory(product.categories);

  const pathCategories = `${
    PATH.products + "/" + slugify(categories?.slug) + "/" + categories?.id
  }`;

  const pathProduct = `${
    PATH.products + "/" + stringToPath(product.name) + "-p" + product.id
  }`;

  return (
    <ReviewCardStyle>
      <div className="card-lg h-96 max-sm:!h-80 card border">
        <div className="card-body h-full flex flex-col justify-between">
          <div className="row align-items-center mb-6">
            <div className="col-4">
              <img
                src={product.thumbnail_url}
                alt={product.thumbnail_url}
                className="img-fluid"
              />
            </div>
            <div className="col-8 ml-n2">
              <Link href={pathCategories} className="font-size-xs text-muted">
                {categories?.title}
              </Link>
              <Link
                href={pathProduct}
                className="d-block font-weight-bold text-body"
              >
                {product.name}
              </Link>
              <Rate
                className="text-sm rate"
                allowHalf
                disabled
                defaultValue={star}
              />
            </div>
          </div>
          <blockquote className="mb-0">
            <p className="text-muted">{content}</p>
            <footer className="font-size-xs text-muted">
              {user.name},{" "}
              <time dateTime="2019-06-01">
                {moment(createdAt).format("MMM DD, YYYY")}
              </time>
            </footer>
          </blockquote>
        </div>
      </div>
    </ReviewCardStyle>
  );
};

export default ReviewCard;
