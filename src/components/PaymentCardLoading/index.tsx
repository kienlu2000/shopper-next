import React from "react";

import SkeletonCustom from "../SkeletonCustom";

const PaymentCardLoading = () => {
  return (
    <div className="col-12">
      <div
        className="payment-card card card-lg bg-light mb-8"
        style={{ height: 224 }}
      >
        <div className="card-body">
          <h6 className="mb-6">
            <SkeletonCustom height={24} />
          </h6>
          <div className="mb-5">
            <SkeletonCustom height={22} />
          </div>
          <div className="mb-5">
            <SkeletonCustom height={22} />
          </div>
          <div className="mb-0">
            <SkeletonCustom height={22} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PaymentCardLoading;
