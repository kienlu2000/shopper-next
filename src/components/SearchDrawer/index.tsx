import { useQuery } from "@tanstack/react-query";
import { Drawer } from "antd";
import Link from "next/link";
import queryString from "query-string";
import { ChangeEvent } from "react";
import SearchLoadingAnimate from "src/assets/json/search-loading.json";
import { PATH } from "src/constants/path";
import { useAppSelector } from "src/hooks/app-hooks";
import { useCategory } from "src/hooks/useCategory";
import { useDebounce } from "src/hooks/useDebounce";
import { getProductsQueryFn } from "src/query/products/queryFnsCommon";
import { QueryKeysCommon } from "src/query/products/queryKeysCommon";
import { selectCategories } from "src/store/categories";
import { array } from "src/utils/array";
import { slugify } from "src/utils/stringToPath";

import LoadingAnimate from "../LoadingAnimate";
import SearchItem from "../SearchItem";
import SearchItemLoading from "../SearchItemLoading";
import SelectCustom from "../SelectCustom";
import { TextResultStyle } from "./style.index";

interface Props {
  isOpen: boolean;
  isCancel: () => void;
}

const SearchDrawer = ({ isCancel, isOpen }: Props) => {
  const categories = useAppSelector(selectCategories);

  const [value, setValue] = useDebounce("");
  const [categoryId, setCategoryId] = useDebounce(0);

  const category = useCategory(categoryId);

  const query = queryString.stringify({
    limit: 5,
    name: value || undefined,
    categories: categoryId || undefined,
  });

  const { data, isFetching } = useQuery({
    queryFn: () => getProductsQueryFn(query),
    queryKey: [QueryKeysCommon.PRODUCTS, query],
    keepPreviousData: true,
    enabled: !!value,
  });

  const dataProducts = data?.data;

  const queryLink = queryString.stringify({
    search: value || undefined,
  });

  const viewCategoriesLink =
    PATH.products + "/" + slugify(category?.slug) + "/" + category?.id;

  const LinkToProducts =
    (category ? viewCategoriesLink : PATH.products) + `?${queryLink}`;

  const handleChangeInput = (event: ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setValue(event.target.value?.trim());
  };

  const handleChangeSelect = (value: string) => {
    setCategoryId(parseInt(value));
  };

  return (
    <Drawer
      open={isOpen}
      onClose={isCancel}
      styles={{ header: { display: "none" }, body: { padding: 0 } }}
      width={470}
    >
      <div className="modal-content">
        <button
          type="button"
          className="close"
          data-dismiss="modal"
          aria-label="Close"
          onClick={isCancel}
        >
          <i className="fe fe-x" aria-hidden="true" />
        </button>

        <div className="modal-header line-height-fixed font-size-lg">
          <strong className="mx-auto">Tìm kiếm</strong>
        </div>
        <div className="modal-body">
          <form>
            <div className="form-group">
              <label className="sr-only" htmlFor="modalSearchCategories">
                Categories:
              </label>
              <SelectCustom
                onChange={handleChangeSelect}
                defaultValue={"Tất cả sản phẩm"}
                className="custom-select"
                id="modalSearchCategories"
              >
                <option>Tất cả sản phẩm</option>
                {categories?.map((e) => (
                  <option value={e.id} key={e.id}>
                    {e.title}
                  </option>
                ))}
              </SelectCustom>
            </div>
            <div className="input-group input-group-merge">
              <input
                defaultValue={value}
                onChange={handleChangeInput}
                className="form-control"
                type="search"
                placeholder="Search"
                onKeyDown={(e) => {
                  if (e.key === "Enter") e.preventDefault();
                }}
              />
              <div className="input-group-append">
                <div className="btn btn-outline-border">
                  <i className="fe fe-search" />
                </div>
              </div>
            </div>
          </form>
        </div>
        <div className="modal-body border-top font-size-sm">
          {value ? (
            <>
              <p>Kết quả tìm kiếm:</p>
              {isFetching ? (
                array(5).map((_, i) => <SearchItemLoading key={i} />)
              ) : dataProducts && dataProducts?.length > 0 ? (
                <>
                  {dataProducts?.map((e) => (
                    <SearchItem key={e.id} onClose={isCancel} {...e} />
                  ))}
                  <Link
                    href={LinkToProducts}
                    onClick={isCancel}
                    className="px-0 text-reset font-bold"
                  >
                    Xem tất cả <i className="fe fe-arrow-right ml-2" />
                  </Link>
                </>
              ) : (
                <TextResultStyle>Không có kết quả phù hợp 🙁</TextResultStyle>
              )}
            </>
          ) : (
            <>
              <TextResultStyle>Tìm sản phẩm của bạn</TextResultStyle>
              <LoadingAnimate path={SearchLoadingAnimate} />
            </>
          )}
        </div>
      </div>
    </Drawer>
  );
};

export default SearchDrawer;
