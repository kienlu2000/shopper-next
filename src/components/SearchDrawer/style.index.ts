import styled from "styled-components";

export const TextResultStyle = styled.div`
  display: flex;
  justify-content: center;
  font-size: 20px;
`;
