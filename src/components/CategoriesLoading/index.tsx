import React from "react";
import { array } from "src/utils/array";

import SkeletonCustom from "../SkeletonCustom";

const CategoriesLoading = () => {
  return (
    <>
      {array(15).map((_, index) => (
        <li key={index} className="list-styled-item">
          <SkeletonCustom height="24.01px" width={253.02} />
        </li>
      ))}
    </>
  );
};

export default CategoriesLoading;
