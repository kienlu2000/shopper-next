import { message, Popconfirm } from "antd";
import classNames from "classnames";
import Link from "next/link";
import React, { useRef } from "react";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { PATH } from "src/constants/path";
import { userService } from "src/service/user";
import { TypeAddressUser } from "src/types/user";
import { handleError } from "src/utils/handleError";

import ButtonCustom from "../Button";
import { AddressCardStyle } from "./style.index";

interface Props extends TypeAddressUser {
  refetch?: () => void;
  hiddenAction?: boolean;
  action?: React.ReactNode;
  className?: string;
  classDrawer?: string;
  isClose?: () => void;
}

export const AddressCard: React.FC<Props> = ({
  _id,
  default: addressDefault,
  fullName,
  district,
  province,
  address,
  email,
  phone,
  refetch,
  action,
  className,
  classDrawer = "col-12",
  hiddenAction,
  isClose,
}) => {
  const loadingDefaultRef = useRef<boolean>(false);
  const loadingDeleteRef = useRef<boolean>(false);

  const linkToEditAddress = PATH.profile.editAddress + "/" + _id;

  const handleChangeDefaultAddress = async () => {
    if (loadingDefaultRef.current) return;

    loadingDefaultRef.current = true;
    try {
      const key = KEYS_MESSAGE.DEFAULT_ADDRESS(_id);
      message.loading({
        key,
        content: MESSAGE.LOADING,
      });

      await userService.editAddressUser(_id, {
        default: true,
      });
      refetch?.();
      message.success({
        key,
        content: MESSAGE.address.changeSuccess,
      });
    } catch (err: any) {
      handleError(err);
    }
    loadingDefaultRef.current = false;
  };

  const handleRemoveAddress = async () => {
    if (loadingDeleteRef.current) return;

    loadingDeleteRef.current = true;
    try {
      const key = KEYS_MESSAGE.REMOVE_ADDRESS(_id);
      message.loading({
        key,
        content: MESSAGE.LOADING,
      });
      await userService.deleteAddressUser(_id);
      refetch?.();
      message.success({
        key,
        content: MESSAGE.address.deleted,
      });
    } catch (err: any) {
      handleError(err);
    }
    loadingDeleteRef.current = false;
  };

  return (
    <AddressCardStyle className={classDrawer} onClick={isClose}>
      <div
        className={classNames(
          "address-card card card-lg bg-light mb-8",
          className,
        )}
      >
        <div className="card-body max-sm:!px-4 max-sm:!py-4">
          <p className="font-size-sm mb-0 leading-[35px]">
            <span className="text-body text-xl font-bold ">{fullName}</span>{" "}
            <br />
            <b>Number Phone:</b> {phone} <br />
            <b>Email: </b>
            {email}
            <br />
            <b>District:</b> {district} <br />
            <b>Province/City:</b> {province} <br />
            <b>Address:</b> {address}
          </p>
          {!hiddenAction && (
            <>
              {addressDefault ? (
                <div className="card-action-right-bottom select-none">
                  <div className="link color-success">Default address</div>
                </div>
              ) : (
                <div className="card-action-right-bottom hidden">
                  <ButtonCustom
                    type="outline"
                    onClick={handleChangeDefaultAddress}
                    size="xs"
                  >
                    Set as default address
                  </ButtonCustom>
                </div>
              )}
            </>
          )}

          {!hiddenAction && (
            <div className="card-action card-action-right gap-2 flex">
              <Link
                className="btn btn-xs btn-circle btn-white-primary"
                href={linkToEditAddress}
              >
                <i className="fe fe-edit-2" />
              </Link>
              {!addressDefault && (
                <Popconfirm
                  placement="topRight"
                  title="Warning"
                  showCancel={false}
                  okButtonProps={{ hidden: true }}
                  description={
                    <div>
                      This operation will be non-refundable, are you sure you
                      want to perform this operation?
                      <div className="flex justify-end gap-2 pt-5">
                        <ButtonCustom onClick={handleRemoveAddress}>
                          Still Delete
                        </ButtonCustom>
                      </div>
                    </div>
                  }
                >
                  <button className="btn btn-xs btn-circle btn-white-primary">
                    <i className="fe fe-x"></i>
                  </button>
                </Popconfirm>
              )}
            </div>
          )}

          {action}
        </div>
      </div>
    </AddressCardStyle>
  );
};
