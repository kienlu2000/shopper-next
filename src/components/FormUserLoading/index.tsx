import React from "react";

import SkeletonCustom from "../SkeletonCustom";
import { FormUserLoadingStyle } from "./style.index";

const FormUserLoading = () => {
  return (
    <FormUserLoadingStyle>
      <form>
        <div className="row">
          <div className="col-12">
            <div className="profile-avatar">
              <div className="circle">
                <SkeletonCustom height={"100%"} />
              </div>
            </div>
          </div>
          <div className="col-12">
            <div className="form-group">
              <label htmlFor="accountFirstName">
                <SkeletonCustom height={24} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label htmlFor="accountEmail">
                <SkeletonCustom height={24} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label htmlFor="accountEmail">
                <SkeletonCustom height={24} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
          <div className="col-12 col-md-12">
            <div className="form-group">
              <label htmlFor="accountPassword">
                <SkeletonCustom height={24} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label htmlFor="AccountNewPassword">
                <SkeletonCustom height={24} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label htmlFor="AccountNewPassword">
                <SkeletonCustom height={24} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
          <div className="col-12 col-lg-6">
            <div className="form-group">
              <label>
                <SkeletonCustom height={24} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
          <div className="col-12 col-lg-6">
            <div className="form-group mb-8">
              <label>
                <SkeletonCustom height={24} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
        </div>
      </form>
    </FormUserLoadingStyle>
  );
};

export default FormUserLoading;
