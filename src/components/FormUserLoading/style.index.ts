import styled from "styled-components";

export const FormUserLoadingStyle = styled.div`
  .circle {
    position: relative;
    cursor: pointer;
    width: 150px;
    height: 150px;
    border-radius: 100%;
    overflow: hidden;
  }
`;
