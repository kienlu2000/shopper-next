import Link from "next/link";
import { PATH } from "src/constants/path";
import { TypeProductDetail } from "src/types/products";
import { currency } from "src/utils/currency";

interface Props extends TypeProductDetail {
  onClose: () => void;
}

const SearchItem = ({
  real_price,
  images,
  price,
  name,
  slug,
  onClose,
}: Props) => {
  const salePrice = price - real_price;
  const LinkToProduct = PATH.products + "/" + slug;
  return (
    <div className="row align-items-center position-relative mb-5">
      {salePrice > 0 && (
        <div className="card-sale badge badge-dark card-badge card-badge-left text-uppercase">
          - {Math.floor((salePrice / price) * 100)}%
        </div>
      )}
      <div className="col-4 col-md-3">
        <img className="img-fluid" src={images?.[0]?.thumbnail_url} alt="..." />
      </div>
      <div className="col position-static">
        <div className="mb-0 font-weight-bold">
          <Link
            onClick={onClose}
            className="stretched-link text-body"
            href={LinkToProduct}
          >
            {name}
          </Link>
          <br />
          <div className="card-product-price">
            {real_price < price ? (
              <>
                <span className="text-primary sale">
                  {currency(real_price)}
                </span>
                <span className="font-size-xs text-gray-350 text-decoration-line-through ml-1">
                  {currency(price)}
                </span>
              </>
            ) : (
              <>
                <span className="text-xl flex h-full items-end">
                  {currency(real_price)}
                </span>
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchItem;
