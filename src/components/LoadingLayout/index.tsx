import classNames from "classnames";
import React from "react";

interface Props {
  className?: string;
}

const LoadingLayout = ({ className = "fallback-wrap" }: Props) => {
  return (
    <div className={classNames(className)}>
      <div className="loader"></div>
    </div>
  );
};

export default LoadingLayout;
