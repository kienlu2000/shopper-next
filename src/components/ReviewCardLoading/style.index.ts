import styled from "styled-components";

export const ReviewCardLoadingStyle = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 30px;
  @media (max-width: 640px) {
    grid-template-columns: repeat(1, 1fr);
  }
`;
