import React from "react";
import { array } from "src/utils/array";

import SkeletonCustom from "../SkeletonCustom";
import { ReviewCardLoadingStyle } from "./style.index";

const ReviewCardLoading = () => {
  return (
    <div className="card-lg h-96 card border max-sm:!h-80">
      <div className="card-body h-full flex flex-col justify-between">
        <div className="row align-items-center mb-6">
          <div className="col-4">
            <SkeletonCustom height={120} />
          </div>
          <div className="col-8 ml-n2">
            <SkeletonCustom height={120} />
          </div>
        </div>
        <blockquote className="mb-0">
          <div className="text-muted mb-2">
            <SkeletonCustom height={24} />
          </div>
          <footer className="font-size-xs text-muted">
            <SkeletonCustom height={21} />
          </footer>
        </blockquote>
      </div>
    </div>
  );
};

interface Props {
  item: number;
}

export const ListReviewCardLoading = ({ item }: Props) => {
  return (
    <ReviewCardLoadingStyle>
      {array(item).map((_, index) => (
        <ReviewCardLoading key={index} />
      ))}
    </ReviewCardLoadingStyle>
  );
};
