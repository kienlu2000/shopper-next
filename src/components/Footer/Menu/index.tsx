import Link from "next/link";
import React from "react";
import { LIST_MENU_FOOTER } from "src/constants/Footer/menu";

const MenuFooter = () => {
  return (
    <>
      {LIST_MENU_FOOTER.map((item) => (
        <div key={item.key} className="col-6 col-sm">
          {/* Heading */}
          <h6 className="heading-xxs mb-4 text-white">{item.title}</h6>
          {/* Links */}
          <ul className="list-unstyled mb-7 mb-sm-0">
            {item.data.map((itemMenu, index) => (
              <li key={index}>
                <Link className="text-gray-300" href={itemMenu.link}>
                  {itemMenu.name}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      ))}
    </>
  );
};

export default MenuFooter;
