import Link from "next/link";
import React from "react";
import { LIST_SOCIAL } from "src/constants/Header/list-social";

const SocialFooter = () => {
  return (
    <div className="col-12 col-md-3">
      <h1 className="mb-6 text-white text-2xl">Shopper | eCommerce Website</h1>
      <ul className="list-unstyled list-inline mb-7 mb-md-0">
        {LIST_SOCIAL.map((item) => (
          <li key={item.key} className="list-inline-item">
            <Link href={item.link} className="text-gray-350">
              <i className={item.icon} />
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default SocialFooter;
