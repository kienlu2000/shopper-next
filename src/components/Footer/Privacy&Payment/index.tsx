import Image from "next/image";
import React from "react";
import { LIST_PAYMENT } from "src/constants/Footer/list-payment";

const PrivacyAndPayment = () => {
  return (
    <div className="py-6">
      <div className="container">
        <div className="row max-sm:flex-col">
          <div className="col">
            <p className="mb-3 mb-md-0 font-size-xxs text-muted">
              © 2019 All rights reserved. Designed by Unvab.
            </p>
          </div>
          <div className="col-auto flex items-center">
            {LIST_PAYMENT.map((item) => (
              <Image
                className="footer-payment"
                width={50}
                height={50}
                key={item.id}
                src={item.img}
                alt={item.img}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default PrivacyAndPayment;
