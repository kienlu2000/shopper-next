import React from "react";

const SubscribeFooter = () => {
  return (
    <form className="mb-11">
      <div className="form-row align-items-start">
        <div className="col">
          <input
            type="email"
            className="form-control form-control-gray-700 form-control-lg"
            placeholder="Enter Email *"
          />
        </div>
        <div className="col-auto">
          <button type="submit" className="btn btn-gray-500 btn-lg">
            Subscribe
          </button>
        </div>
      </div>
    </form>
  );
};

export default SubscribeFooter;
