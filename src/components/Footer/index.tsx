import MenuFooter from "./Menu";
import PrivacyAndPayment from "./Privacy&Payment";
import SocialFooter from "./Social";
import SubscribeFooter from "./Subscribe";

const Footer = () => {
  return (
    <footer
      className="bg-dark bg-cover @@classList"
      style={{ backgroundImage: "url(./static/img/patterns/pattern-2.svg)" }}
    >
      <div className="py-12 max-sm:!py-8 border-bottom border-gray-700">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 col-md-10 col-lg-8 col-xl-6">
              <h5 className="mb-7 text-center text-white">
                Want style Ideas and Treats?
              </h5>
              <SubscribeFooter />
            </div>
          </div>
          <div className="row">
            <SocialFooter />
            <MenuFooter />
          </div>
        </div>
      </div>
      <PrivacyAndPayment />
    </footer>
  );
};

export default Footer;
