import Link from "next/link";
import React from "react";
import { useQueryProducts } from "src/query/products/queryHooksCommon";
import WithListLoading from "src/utils/withListLoading";

import ProductCard from "../ProductCard";
import { ProductCardLoading } from "../ProductCardLoading";

interface Props {
  query: string;
  link: string;
}

const ListProductCardDiscover = ({ link, query }: Props) => {
  const { data, isLoading } = useQueryProducts(query);

  const dataProductDetail = data?.data;

  const renderItem = () => {
    const ListProductCard = WithListLoading({
      Component: ProductCard,
      data: dataProductDetail,
      ComponentLoading: ProductCardLoading,
      loading: isLoading,
      wrapperClass: "col-12 col-md-3 col-md-4",
      loadingCount: 12,
    });

    return ListProductCard;
  };

  return (
    <div className="row">
      {renderItem()}
      <div className="col-12 max-lg:col-start-1 max-lg:col-end-4">
        <div className="mt-7 text-center">
          <Link className="link-underline" href={link}>
            Discover more
          </Link>
        </div>
      </div>
    </div>
  );
};

export default ListProductCardDiscover;
