import styled from "styled-components";

export const ShortedContentStyle = styled.div`
  .content {
    overflow: hidden;
  }
  .read-more {
    padding-top: 10px;
    button {
      transition: 0.25s;
    }
    span {
      cursor: pointer;
    }

    &:hover {
      color: #ff2915;
      transition: 0.25s;
    }
  }
`;
