import React, { useState } from "react";

import { ShortedContentStyle } from "./style.index";

interface Props {
  description: string;
}

const ShortedContent = ({ description }: Props) => {
  const [open, setOpen] = useState(false);
  return (
    <ShortedContentStyle>
      <div
        style={{ height: open ? "auto" : 300 }}
        className="content"
        dangerouslySetInnerHTML={{ __html: description }}
      />
      <div className="read-more">
        <button
          onClick={() => {
            setOpen(!open);
          }}
          className="btn-sm flex items-center justify-center gap-2 min-w-[300px] btn-xs btn-outline-dark"
        >
          {open ? "Read Less" : "Read More"}
        </button>
      </div>
    </ShortedContentStyle>
  );
};

export default ShortedContent;
