import moment from "moment";
import React, { useMemo } from "react";
import { DATE_FORMAT, STATUS_ORDER } from "src/constants";
import { TypeOrderItem } from "src/types/order";
import { currency } from "src/utils/currency";

const OrderItemStatus = ({
  _id,
  finishedDate,
  createdAt,
  status,
  total,
  confirmDate,
  shippingDate,
}: TypeOrderItem) => {
  const strCreatedAt = useMemo(
    () => moment(createdAt).format(DATE_FORMAT),
    [createdAt],
  );

  return (
    <div className="card-body bg-light">
      <div className="row">
        <div className="col-6 col-lg-3">
          <h6 className="heading-xxxs text-muted">CODE ORDERS:</h6>
          <p className="mb-lg-0 font-size-sm font-weight-bold">
            {_id.substring(_id.length - 6)}
          </p>
        </div>
        {["pending", "cancel"].includes(status) && (
          <div className="col-6 col-lg-3">
            <h6 className="heading-xxxs text-muted">Date of creation:</h6>
            <p className="mb-lg-0 font-size-sm font-weight-bold">
              <time dateTime="2019-09-25">{strCreatedAt}</time>
            </p>
          </div>
        )}
        {status === "confirm" && (
          <div className="col-6 col-lg-3">
            <h6 className="heading-xxxs text-muted">Confirmation date:</h6>
            <p className="mb-lg-0 font-size-sm font-weight-bold">
              <time dateTime="2019-09-25">
                {moment(confirmDate).format(DATE_FORMAT)}
              </time>
            </p>
          </div>
        )}
        {status === "shipping" && (
          <div className="col-6 col-lg-3">
            <h6 className="heading-xxxs text-muted">Day shipping:</h6>
            <p className="mb-lg-0 font-size-sm font-weight-bold">
              <time dateTime="2019-09-25">
                {moment(shippingDate).format(DATE_FORMAT)}
              </time>
            </p>
          </div>
        )}
        {status === "finished" && (
          <div className="col-6 col-lg-3">
            <h6 className="heading-xxxs text-muted">Delivery day:</h6>
            <p className="mb-lg-0 font-size-sm font-weight-bold">
              <time dateTime="2019-09-25">
                {moment(finishedDate).format(DATE_FORMAT)}
              </time>
            </p>
          </div>
        )}

        <div className="col-6 col-lg-3">
          <h6 className="heading-xxxs text-muted">Status:</h6>
          <p className="mb-0 font-size-sm font-weight-bold">
            {STATUS_ORDER[status]}
          </p>
        </div>
        <div className="col-6 col-lg-3">
          <h6 className="heading-xxxs text-muted">Total:</h6>
          <p className="mb-0 font-size-sm font-weight-bold">
            {currency(total)} VND
          </p>
        </div>
      </div>
    </div>
  );
};

export default OrderItemStatus;
