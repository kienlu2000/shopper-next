import React from "react";
import { array } from "src/utils/array";

import SkeletonCustom from "../SkeletonCustom";

const OrderDetailLoading = () => {
  return (
    <>
      <div className="card card-lg mb-5 border">
        <div className="card-body pb-0">
          <div className="card card-sm">
            <SkeletonCustom height={87.69} />
          </div>
        </div>
        <div className="card-footer">
          <h6 className="mb-7">
            <SkeletonCustom height={24} width={150} />
          </h6>
          <hr className="my-5" />
          <ul className="list-group list-group-lg list-group-flush-y list-group-flush-x">
            {array(4).map((_, index) => (
              <li key={index} className="list-group-item px-0">
                <SkeletonCustom height={120} />
              </li>
            ))}
          </ul>
        </div>
      </div>
      <div className="card card-lg mb-5 border">
        <div className="card-body">
          <h6 className="mb-7">
            <SkeletonCustom height={24} width={150} />
          </h6>
          <ul className="list-group list-group-sm list-group-flush-y list-group-flush-x">
            {array(4).map((_, index) => (
              <li key={index} className="list-group-item d-flex">
                <SkeletonCustom height={24} />
              </li>
            ))}
            <li className="list-group-item d-flex font-size-lg font-weight-bold">
              <SkeletonCustom height={27} />
            </li>
          </ul>
        </div>
      </div>
      <div className="card card-lg border">
        <div className="card-body">
          <h6 className="mb-7">
            <SkeletonCustom height={24} />
          </h6>
          <div className="row">
            <div className="col-12">
              <SkeletonCustom height={216} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderDetailLoading;
