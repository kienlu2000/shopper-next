import { Popconfirm } from "antd";
import classNames from "classnames";
import { useRouter } from "next/navigation";
import React from "react";
import { PATH } from "src/constants/path";

import ButtonCustom from "../Button";

interface Props {
  enable: boolean;
  action: () => void;
  icon?: string;
  loadingAction?: boolean;
  type?: "button" | "icon";
  name?: string;
  buttonType?: "default" | "reset" | "outline" | "button";
}

const PopConfirmCustom = (props: Props) => {
  const {
    enable,
    action,
    icon: className = "fe fe-shopping-cart",
    type = "icon",
    loadingAction,
    name = "Add to Cart",
    buttonType = "default",
  } = props;

  const router = useRouter();

  const handleNavigate = () => {
    router.push(PATH.authentication);
  };

  return (
    <Popconfirm
      disabled={enable}
      title="Notification"
      showCancel={false}
      okButtonProps={{ hidden: true }}
      description={
        <>
          <p>You need to log in before using this function</p>
          <div className="flex justify-end gap-2">
            <ButtonCustom onClick={handleNavigate}>Login</ButtonCustom>
          </div>
        </>
      }
    >
      {type === "icon" ? (
        <span className="card-action max-sm:!opacity-100 max-sm:!translate-y-2">
          <button
            className="btn btn-xs btn-circle btn-white-primary"
            data-toggle="button"
            onClick={() => enable && action()}
          >
            <i className={className} />
          </button>
        </span>
      ) : (
        <ButtonCustom
          type={buttonType}
          loading={loadingAction}
          onClick={() => enable && action()}
          className="mb-2"
        >
          {name}
          <i className={classNames(`${className} ml-2`)}></i>
        </ButtonCustom>
      )}
    </Popconfirm>
  );
};

export default PopConfirmCustom;
