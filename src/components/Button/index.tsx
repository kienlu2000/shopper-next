import { LoadingOutlined } from "@ant-design/icons";
import classNames from "classnames";
import Link from "next/link";
import React from "react";

interface Props {
  link?: string;
  type?: "default" | "reset" | "outline" | "button";
  size?: string;
  loading?: boolean;
  className?: string;
  children: React.ReactNode;
  disable?: boolean;
  onClick?: () => void;
}

const ButtonCustom = (props: Props) => {
  const {
    children,
    link,
    loading = false,
    size = "sm",
    type = "default",
    className,
    onClick,
    disable,
  } = props;

  let Component: any = "button";
  if (link) {
    Component = Link;
  }

  return (
    <Component
      href={link}
      type={type}
      onClick={onClick}
      className={classNames(
        "btn flex justify-center gap-2 items-center",
        `btn-${size}`,
        className,
        {
          "disabled pointer-events-none": loading || disable,
          "btn-dark": type === "default",
          "btn-outline-dark": type === "outline",
        },
      )}
    >
      {loading && <LoadingOutlined className="mr-2" />}
      {children}
    </Component>
  );
};

export default ButtonCustom;
