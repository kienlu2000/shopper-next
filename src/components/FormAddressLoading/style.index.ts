import styled from "styled-components";

export const FormAddressLoadingStyle = styled.div`
  label {
    margin-bottom: unset !important;
  }
`;
