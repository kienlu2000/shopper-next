import React from "react";

import SkeletonCustom from "../SkeletonCustom";
import { FormAddressLoadingStyle } from "./style.index";

const FormAddressLoading = () => {
  return (
    <FormAddressLoadingStyle>
      <form>
        <div className="row">
          <div className="col-12">
            <div className="form-group">
              <label>
                <SkeletonCustom height={22} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>

          <div className="col-12 col-md-6">
            <div className="form-group">
              <label>
                <SkeletonCustom height={22} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label>
                <SkeletonCustom height={22} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label>
                <SkeletonCustom height={22} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
          <div className="col-12 col-md-6">
            <div className="form-group">
              <label>
                <SkeletonCustom height={22} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>

          <div className="col-12">
            <div className="form-group">
              <label>
                <SkeletonCustom height={22} width={137} />
              </label>
              <SkeletonCustom height={50.5} />
            </div>
          </div>
        </div>
      </form>
    </FormAddressLoadingStyle>
  );
};

export default FormAddressLoading;
