import styled from "styled-components";

export const SelectCustomStyle = styled.div`
  .ant-form-item {
    margin-bottom: 1.5rem !important;
    &-explain {
      color: red;
      font-size: 0.875rem;
      font-style: italic;
      margin-left: 5px;
    }
  }
  .ant-select {
    &:hover {
      border-color: black !important;
    }
    &-selector {
      border: unset !important;
      border-radius: unset !important;
      padding: 0 1.5rem !important;
    }
  }
`;
export const ErrorSelectStyle = styled.div`
  &.errors {
    .ant-select-selection-placeholder {
      color: red;
    }
    .ant-select-arrow {
      color: red;
    }
  }
`;
export const ErrorTextStyle = styled.div``;
