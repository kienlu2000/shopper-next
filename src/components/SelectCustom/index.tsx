import { Form, Select, SelectProps } from "antd";
import { ValidateStatus } from "antd/es/form/FormItem";
import classNames from "classnames";
import React from "react";
import { Control, Controller, FieldValues } from "react-hook-form";

import {
  ErrorSelectStyle,
  ErrorTextStyle,
  SelectCustomStyle,
} from "./style.index";

export interface Props extends SelectProps {
  margin_bottom?: string;
  name?: string;
  control?: Control<FieldValues | any>;
  errors?: string;
  validate_status?: ValidateStatus;
  defaultValue?: string | number;
  width?: string;
  format?: "number" | "wordNumber" | "word";
  forgot?: boolean;
  renderField?: any;
  label?: string;
  placeholder?: string;
}

const SelectCustom = (props: Props) => {
  const {
    name = "",
    control,
    errors,
    validate_status,
    defaultValue,
    renderField,
    label,
  } = props;

  return (
    <SelectCustomStyle>
      {control ? (
        <Form.Item help={errors} validateStatus={validate_status}>
          <Controller
            name={name}
            defaultValue={defaultValue}
            control={control}
            render={({ field: { onChange, value } }) => {
              return (
                <ErrorSelectStyle className={classNames({ errors })}>
                  {label && <label>{label}</label>}
                  {renderField ? (
                    renderField?.({
                      ...props,
                      onChange,
                      values: value as string,
                    })
                  ) : (
                    <Select
                      className={classNames("custom-select", {
                        "border border-sold text-[red] !border-[red]": errors,
                      })}
                      onChange={onChange}
                      value={value}
                      {...props}
                    />
                  )}
                </ErrorSelectStyle>
              );
            }}
          />
        </Form.Item>
      ) : (
        <ErrorSelectStyle>
          {label && <label>{label}</label>}
          {renderField ? (
            renderField?.(props)
          ) : (
            <Select
              className={classNames("custom-select", {
                "border border-sold text-[red] !border-[red]": errors,
              })}
              defaultValue={defaultValue}
              {...props}
            />
          )}
          {errors && <ErrorTextStyle>{errors}</ErrorTextStyle>}
        </ErrorSelectStyle>
      )}
    </SelectCustomStyle>
  );
};

export default SelectCustom;
