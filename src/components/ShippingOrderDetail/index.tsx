import Link from "next/link";
import React from "react";
import { PAYMENT_METHOD } from "src/constants";
import { TypePaymentOrder, TypeShippingOrder } from "src/types/order";

interface Props {
  shipping?: TypeShippingOrder;
  payment: TypePaymentOrder;
  title?: string;
  description?: string;
}

const ShippingOrderDetail = ({
  shipping,
  description,
  title,
  payment,
}: Props) => {
  return (
    <div className="card card-lg border">
      <div className="card-body">
        <h6 className="mb-7">Shipping Details</h6>
        <div className="row">
          <div className="col-12 col-md-6">
            <p className="font-size-sm mb-0 leading-[35px]">
              <Link className="text-body text-xl font-bold " href={"/"}>
                {shipping?.fullName}
              </Link>
              <br />
              <b>Number Phone: </b> {shipping?.phone} <br />
              <b>Email: </b>
              {shipping?.email}
              <br />
              <b>District:</b> {shipping?.district} <br />
              <b>City:</b> {shipping?.province} <br />
              <b>Address:</b> {shipping?.address} <br />
            </p>
          </div>
          <div className="col-12 col-md-6">
            <p className="mb-4 text-body text-xl font-bold">Shipping Method:</p>
            <p className="mb-4">{title}</p>
            <p className="mb-4 text-gray-500">{description}</p>
            <p className="mb-4 text-body text-xl font-bold">Payment Method:</p>
            <p className="mb-0 text-gray-500">
              {PAYMENT_METHOD[payment.paymentMethod]}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShippingOrderDetail;
