"use client";

import Lottie from "lottie-web";
import React, { useEffect, useRef } from "react";

import { LoadingAnimateStyle } from "./style.index";

interface Props {
  path: any;
  height?: string;
  className?: string;
}

const LoadingAnimate = ({
  path,
  height = "",
  className = "json-animate",
}: Props) => {
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const loading = Lottie.loadAnimation({
      animationData: path,
      autoplay: true,
      container: ref.current as Element,
      loop: true,
      renderer: "svg",
    });

    return () => {
      loading.destroy();
    };
  }, []);

  return (
    <LoadingAnimateStyle
      style={{
        height: height,
      }}
    >
      <div ref={ref} className={className} id="animation-container"></div>
    </LoadingAnimateStyle>
  );
};

export default LoadingAnimate;
