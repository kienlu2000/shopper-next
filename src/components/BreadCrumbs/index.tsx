"use client";

import { Breadcrumb } from "antd";
import { useRouter } from "next/navigation";
import React from "react";
import { PATH } from "src/constants/path";
import useBreadcrumbPath from "src/hooks/useBreadcrumbPath";

const BreadCrumbs = () => {
  const breadcrumb = useBreadcrumbPath();

  const router = useRouter();

  const _breadcrumb = breadcrumb.map((item) => {
    if (item === "") return "home";
    return item;
  });

  const handleNavigate = (link: string) => {
    if (link === "home") return router.push(PATH.home);
    return router.push(`/${link}`);
  };

  return (
    <Breadcrumb>
      {_breadcrumb.map((item, index) => (
        <Breadcrumb.Item key={index}>
          <div onClick={() => handleNavigate(item)}>{item}</div>
        </Breadcrumb.Item>
      ))}
    </Breadcrumb>
  );
};

export default BreadCrumbs;
