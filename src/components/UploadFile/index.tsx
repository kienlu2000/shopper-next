/* eslint-disable no-unused-vars */

import React, { ChangeEvent, useRef, useState } from "react";

interface UploadFileProps {
  children: (props: {
    previewSrc: string | undefined;
    triggerEvent: () => void;
  }) => React.ReactNode;
  onChange?: (file: File) => void;
}

export const UploadFile: React.FC<UploadFileProps> = ({
  children,
  onChange,
}) => {
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [previewSrc, setPreviewSrc] = useState<string | undefined>();

  const onPreview = (ev: ChangeEvent<HTMLInputElement>) => {
    const file = ev.target.files?.[0];
    if (file) {
      onChange?.(file);

      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setPreviewSrc(reader.result as string);
      });

      reader.readAsDataURL(file);
    }
  };

  const triggerEvent = () => {
    inputRef.current?.click();
  };

  return (
    <>
      <input
        accept="image/*"
        ref={inputRef}
        onChange={onPreview}
        type="file"
        hidden
      />
      {children?.({
        previewSrc,
        triggerEvent,
      })}
    </>
  );
};
