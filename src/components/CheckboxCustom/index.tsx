/* eslint-disable no-unused-vars */

import { ChangeEvent, useId } from "react";
interface Props {
  onChange: (checked: boolean) => void;
  checked: boolean;
}

const CheckboxCustom = ({ onChange, checked }: Props) => {
  const id = useId();

  const _onChange = (ev: ChangeEvent<HTMLInputElement>) => {
    onChange(ev.target.checked);
  };

  return (
    <div className="custom-control custom-checkbox">
      <input
        className="custom-control-input"
        id={id}
        type="checkbox"
        onChange={_onChange}
        checked={checked}
      />
      <label className="custom-control-label" htmlFor={id} />
    </div>
  );
};

export default CheckboxCustom;
