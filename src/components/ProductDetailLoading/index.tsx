import React from "react";

import SkeletonCustom from "../SkeletonCustom";

const ProductDetailLoading = () => {
  return (
    <div>
      <nav className="py-5">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <SkeletonCustom height={22.78} />
            </div>
          </div>
        </div>
      </nav>
      <section>
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className="row">
                <div className="col-12 col-md-6">
                  <div className="card">
                    <SkeletonCustom height={538} />
                  </div>
                  <div className="pb-4 mt-1 mb-10 mb-md-0">
                    <SkeletonCustom height={97} />
                  </div>
                </div>
                <div className="col-12 col-md-6 pl-lg-10">
                  <div className="row mb-1">
                    <div className="col">
                      <SkeletonCustom height={24} />
                    </div>
                  </div>
                  <h3 className="mb-2">
                    <SkeletonCustom height={153.56} />
                  </h3>
                  <div className="mb-7">
                    <SkeletonCustom height={36} />
                  </div>
                  <div className="mb-10">
                    <SkeletonCustom height={96} />
                  </div>
                  <div className="form-row mb-7">
                    <div className="col-6">
                      <SkeletonCustom height={50.5} />
                    </div>
                    <div className="col-6">
                      <SkeletonCustom height={50.5} />
                    </div>
                  </div>
                  <SkeletonCustom height={24} />
                  <div className="my-2">
                    <SkeletonCustom height={30.5} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default ProductDetailLoading;
