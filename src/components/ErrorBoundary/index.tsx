"use client";

import React, { Component, ErrorInfo, ReactNode } from "react";
import IconError500 from "src/assets/img/error500";

import { ErrorBoundaryStyle } from "./style.index";
interface Props {
  children: ReactNode;
}

interface State {
  hasError: boolean;
}

class ErrorBoundary extends Component<Props, State> {
  public state: State = {
    hasError: false,
  };

  public static getDerivedStateFromError(): State {
    return { hasError: true };
  }

  public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    console.error("Uncaught error:", error, errorInfo);
  }

  public render() {
    if (this.state.hasError) {
      return (
        <ErrorBoundaryStyle className="main-error-page">
          <IconError500 />
          <h1 className="error-title flex justify-center">
            Woops! <br />
            Something went wrong
          </h1>
          <h2 className="error-subtitle">
            Have you tried turning it off and on again?
          </h2>
        </ErrorBoundaryStyle>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
