import Link from "next/link";
import React from "react";
import CartLoading from "src/assets/json/cart-loading.json";
import LoadingAnimate from "src/components/LoadingAnimate";
import { PATH } from "src/constants/path";

interface Props {
  onClose: () => void;
}

const CartEmpty = ({ onClose }: Props) => {
  return (
    <div className="modal-content">
      <button
        type="button"
        className="close !outline-none"
        data-dismiss="modal"
        aria-label="Close"
        onClick={onClose}
      >
        <i className="fe fe-x" aria-hidden="true" />
      </button>
      <div className="modal-header line-height-fixed font-size-lg">
        <strong className="mx-auto">Giỏ hàng (0)</strong>
      </div>
      <div className="modal-body flex-grow-0 my-auto">
        <h6 className="mb-7 text-center">Hãy thêm gì đó vào giỏ hàng!</h6>
        <LoadingAnimate path={CartLoading} />
        <Link
          onClick={onClose}
          className="btn btn-block btn-outline-dark"
          href={PATH.products}
        >
          Tiếp tục mua sắm
        </Link>
      </div>
    </div>
  );
};

export default CartEmpty;
