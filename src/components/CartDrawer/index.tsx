"use client";

import { Drawer } from "antd";
import Link from "next/link";
import { PATH } from "src/constants/path";
import { useAppSelector } from "src/hooks/app-hooks";
import { selectCart } from "src/store/cart";
import { currency } from "src/utils/currency";

import CartItem from "../CartItem";
import CartEmpty from "./CartEmpty";

interface Props {
  isOpen: boolean;
  isClose: () => void;
}

const CartDrawer = ({ isClose, isOpen }: Props) => {
  const cart = useAppSelector(selectCart);

  return (
    <Drawer
      styles={{ header: { display: "none" }, body: { padding: 0 } }}
      width={470}
      open={isOpen}
      onClose={isClose}
    >
      <>
        {cart?.listItems?.length ? (
          <div className="modal-content">
            <button
              onClick={isClose}
              type="button"
              className="close !outline-none"
              data-dismiss="modal"
              aria-label="Close"
            >
              <i className="fe fe-x" aria-hidden="true" />
            </button>
            <div className="modal-header line-height-fixed font-size-lg">
              <strong className="mx-auto">
                Your Cart ({cart.totalQuantity})
              </strong>
            </div>
            <ul className="list-group list-group-lg list-group-flush">
              {cart?.listItems?.map((item) => (
                <CartItem
                  onClose={isClose}
                  quantity={item.quantity}
                  key={item.product.id}
                  {...item.product}
                />
              ))}
            </ul>
            <div className="modal-footer line-height-fixed font-size-sm bg-light mt-auto">
              <strong>Subtotal</strong>{" "}
              <strong className="ml-auto">
                {currency(cart?.subTotal)} VND
              </strong>
            </div>
            <div className="modal-body">
              <Link
                onClick={isClose}
                className="btn btn-block btn-outline-dark"
                href={PATH.myCart}
              >
                View Cart
              </Link>
            </div>
          </div>
        ) : (
          <CartEmpty onClose={isClose} />
        )}
      </>
    </Drawer>
  );
};

export default CartDrawer;
