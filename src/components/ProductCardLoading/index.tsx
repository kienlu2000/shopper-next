import Link from "next/link";

import SkeletonCustom from "../SkeletonCustom";

export const ProductCardLoading = () => {
  return (
    <div className="product-card card mb-7">
      <div className="card-img">
        <Link href={"#"} className="card-img-hover">
          <SkeletonCustom height={300} />
        </Link>
      </div>
      <div className="card-body px-0">
        <div className="card-product-category font-size-xs">
          <div className="text-muted">
            <SkeletonCustom height="100%" />
          </div>
        </div>
        <div className="card-product-title font-weight-bold">
          <div className="text-body card-product-name">
            <SkeletonCustom height="100%" />
          </div>
        </div>
        <div className="card-product-rating">
          <SkeletonCustom height="100%" />
        </div>
        <div className="card-product-price">
          <SkeletonCustom height="100%" />
        </div>
      </div>
    </div>
  );
};
