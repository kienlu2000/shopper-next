import queryString from "query-string";
import React from "react";
import EmptyOrderAnimate from "src/assets/json/order-empty.json";
import { useSearch } from "src/hooks/useSearch";
import { useQueryOrder } from "src/query/order/queryHooksCommon";
import { array } from "src/utils/array";

import LoadingAnimate from "../LoadingAnimate";
import OrderItem from "../OrderItem";
import OrderItemLoading from "../OrderItemLoading";
import { PaginateCustom } from "../PaginateCustom";

interface Props {
  status: string;
}

const ListOrderItem = ({ status }: Props) => {
  const [search] = useSearch({ page: 1 });
  const query = queryString.stringify({
    status: status || undefined,
    page: search.page,
  });

  const { data, isFetching, refetch } = useQueryOrder(`?${query}`);

  const dataOrder = data?.data;
  const dataPaginateOrder = data?.paginate;

  if (isFetching)
    return (
      <>
        {array(5).map((_, index) => (
          <OrderItemLoading key={index} />
        ))}
      </>
    );

  return (
    <>
      {dataOrder && dataOrder.length > 0 ? (
        dataOrder.map((item) => (
          <OrderItem key={item._id} refetch={refetch} {...item} />
        ))
      ) : (
        <div className="flex items-center flex-col gap-5 text-center">
          <LoadingAnimate path={EmptyOrderAnimate} height="300px" />
          <p className="text-base">There are no orders yet !!</p>
        </div>
      )}

      <PaginateCustom totalPage={dataPaginateOrder?.totalPage} />
    </>
  );
};

export default ListOrderItem;
