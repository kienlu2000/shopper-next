/* eslint-disable no-unused-vars */

import { Rate } from "antd";
import Link from "next/link";
import { ImageError } from "src/constants";
import { MESSAGE } from "src/constants/message";
import { PATH } from "src/constants/path";
import { useAppDispatch, useAppSelector } from "src/hooks/app-hooks";
import { useAction } from "src/hooks/useActions";
import { useCategory } from "src/hooks/useCategory";
import {
  useMutationAddProductToWishlist,
  useMutationRemoveProductToWishlist,
} from "src/mutate/products/mutationHooksCommon";
import { QueryKeysUserCommon } from "src/query/user/queryKeysCommon";
import { selectCart } from "src/store/cart";
import { updateCartItemAction } from "src/store/cart/cart.action";
import { TypeProductDetail } from "src/types/products";
import { TypeProfileUser } from "src/types/user";
import { currency } from "src/utils/currency";

import PopConfirmCustom from "../PopConfirmCustom";
import { queryClient } from "../QueryProvider";

interface Props extends TypeProductDetail {
  showWishlist?: boolean;
  showRemove?: boolean;
  onRemoveWishlistSuccess?: (id: number) => void;
}

const ProductCard = (props: Props) => {
  const {
    categories,
    slug,
    price,
    real_price,
    images,
    name,
    rating_average,
    showRemove,
    showWishlist = false,
    onRemoveWishlistSuccess,
    id,
    review_count,
  } = props;
  const dispatch = useAppDispatch();

  const category = useCategory(categories);

  const cart = useAppSelector(selectCart);

  const user = queryClient.getQueryData<TypeProfileUser>([
    QueryKeysUserCommon.PROFILE_USER,
  ]);

  const { mutateAsync: mutateAddProductWishlist } =
    useMutationAddProductToWishlist();
  const { mutateAsync: mutateRemoveProductWishlist } =
    useMutationRemoveProductToWishlist();

  const _slug = PATH.products + "/" + slug;
  const salePrice = price - real_price;
  const image_1 = images[0].thumbnail_url;
  const image_2 = images?.[1]?.thumbnail_url || image_1;

  const onAddProductCart = useAction({
    action: () => {
      const quantity =
        cart?.listItems?.find((e) => e.productId === id)?.quantity || 0;
      return dispatch(
        updateCartItemAction({
          productId: id,
          quantity: quantity + 1,
          showPopover: true,
        }),
      );
    },
    messageLoading: MESSAGE.cart.addingCart(name),
  });

  const onAddWishlist = useAction({
    action: () => mutateAddProductWishlist(id),
    messageLoading: MESSAGE.wishlist.adding,
    messageSuccess: MESSAGE.wishlist.added(name),
  });

  const _onRemoveWishlistSuccess = useAction({
    action: () => mutateRemoveProductWishlist(id),
    messageLoading: MESSAGE.wishlist.removing,
    messageSuccess: MESSAGE.wishlist.removed(name),
    onSuccess: () => onRemoveWishlistSuccess?.(id),
  });

  return (
    <div className="product-card card mb-7">
      {salePrice > 0 && (
        <div className="card-sale badge badge-dark card-badge card-badge-left text-uppercase">
          -{Math.floor((salePrice / price) * 100)}%
        </div>
      )}
      <div className="card-img">
        <Link href={_slug} className="card-img-hover">
          <img
            onError={({ currentTarget }) => {
              currentTarget.onerror = null;
              currentTarget.src = ImageError.src;
            }}
            className="card-img-top card-img-back"
            src={image_2}
            alt="..."
          />
          <img
            onError={({ currentTarget }) => {
              currentTarget.onerror = null;
              currentTarget.src = ImageError.src;
            }}
            className="card-img-top card-img-front"
            src={image_1}
            alt="..."
          />
        </Link>
        <div className="card-actions">
          <PopConfirmCustom enable={!!user} action={() => onAddProductCart()} />
          {showWishlist && (
            <PopConfirmCustom
              action={() => onAddWishlist()}
              enable={!!user}
              icon="fe fe-heart"
            />
          )}
          {showRemove && (
            <span className="card-action max-sm:!opacity-100 max-sm:!translate-y-2">
              <button
                onClick={_onRemoveWishlistSuccess}
                className="btn btn-xs btn-circle btn-white-primary"
                data-toggle="button"
              >
                <i className="fe fe-x" />
              </button>
            </span>
          )}
        </div>
      </div>
      <div className="card-body px-0">
        <div className="card-product-category font-size-xs">
          {category && <div className="text-muted">{category.title}</div>}
        </div>
        <div className="card-product-title font-weight-bold">
          <Link className="text-body card-product-name" href={_slug}>
            {name}
          </Link>
        </div>
        <div className="card-product-rating">
          {rating_average > 0 && (
            <>
              {rating_average}
              <Rate
                className="text-sm rate"
                allowHalf
                disabled
                defaultValue={rating_average}
              />
              <span className="max-sm:hidden">({review_count} Reviews)</span>
            </>
          )}
        </div>
        <div className="card-product-price flex items-baseline max-sm:flex-col">
          {real_price < price ? (
            <>
              <span className="text-primary sale max-sm:flex max-sm:!text-xl">
                {currency(real_price)}{" "}
                <span className="hidden max-sm:block max-sm:!ml-1">VND</span>
              </span>
              <span className="font-size-xs text-gray-350 text-decoration-line-through ml-1">
                {currency(price)} VND
              </span>
            </>
          ) : (
            <span className="text-xl flex h-full items-end">
              {currency(real_price)} VND
            </span>
          )}
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
