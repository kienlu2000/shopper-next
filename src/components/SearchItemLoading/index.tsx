import React from "react";

import SkeletonCustom from "../SkeletonCustom";

const SearchItemLoading = () => {
  return (
    <div className="row align-items-center position-relative mb-5">
      <div className="col-4 col-md-3">
        <SkeletonCustom height={100} />
      </div>
      <div className="col position-static">
        <div className="mb-0 font-weight-bold">
          <div className="stretched-link text-body">
            <SkeletonCustom height={42.5} />
          </div>
          <br />
          <span className="text-muted">
            <SkeletonCustom height={20} />
          </span>
        </div>
      </div>
    </div>
  );
};

export default SearchItemLoading;
