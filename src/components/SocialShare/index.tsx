import Link from "next/link";
import React from "react";
import { LIST_SOCIAL_SHARE } from "src/constants/social-share";

const SocialShare = () => {
  return (
    <div className="mb-0">
      <span className="mr-4">Share:</span>
      {LIST_SOCIAL_SHARE.map((item) => (
        <Link
          key={item.key}
          target="_blank"
          className="btn btn-xxs btn-circle btn-light font-size-xxxs text-gray-350 mr-2"
          href={item.link}
        >
          <i className={item.icon} />
        </Link>
      ))}
    </div>
  );
};

export default SocialShare;
