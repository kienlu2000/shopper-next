import classNames from "classnames";
import Link from "next/link";
import { usePathname, useSearchParams } from "next/navigation";
import React from "react";

interface PaginateProps {
  totalPage?: number;
  name?: string;
}

export const PaginateCustom: React.FC<PaginateProps> = ({
  totalPage = 0,
  name = "page",
}) => {
  const search = useSearchParams();
  const currentPage = parseInt(search.get(name) || "1");
  const pathname = usePathname();

  const urlSearchParam = new URLSearchParams(search);

  const renderItem = () => {
    let start = currentPage - 2;
    let end = currentPage + 2;

    if (!totalPage) return;

    if (start < 1) {
      start = 1;
      end = 5;
    }

    if (totalPage && end > totalPage) {
      end = totalPage;
      start = totalPage - 5;
      if (start < 1) start = 1;
    }

    let list: JSX.Element[] = [];
    for (let i = start; i <= end; i++) {
      urlSearchParam.set(name, i.toString());
      list.push(
        <li
          key={i}
          className={classNames("page-item", { active: i === currentPage })}
        >
          <Link
            className="page-link"
            href={`${pathname}?${urlSearchParam.toString()}`}
          >
            {i}
          </Link>
        </li>,
      );
    }
    return list;
  };

  urlSearchParam.set(name, (currentPage - 1).toString());
  const prevLink = `${pathname}?${urlSearchParam.toString()}`;

  urlSearchParam.set(name, (currentPage + 1).toString());
  const nextLink = `${pathname}?${urlSearchParam.toString()}`;

  if (totalPage && totalPage <= 1) return null;

  if (currentPage > totalPage) return null;
  return (
    <nav className="d-flex justify-content-center justify-content-md-end">
      <ul className="pagination pagination-sm text-gray-400">
        {currentPage > 1 && (
          <li className="page-item">
            <Link className="page-link page-link-arrow" href={prevLink}>
              <i className="fa fa-caret-left" />
            </Link>
          </li>
        )}

        {renderItem()}

        {currentPage < totalPage && (
          <li className="page-item">
            <Link className="page-link page-link-arrow" href={nextLink}>
              <i className="fa fa-caret-right" />
            </Link>
          </li>
        )}
      </ul>
    </nav>
  );
};
