import { Form, Input } from "antd";
import { ValidateStatus } from "antd/es/form/FormItem";
import { TextAreaProps } from "antd/es/input";
import classNames from "classnames";
import { Control, Controller, FieldValues } from "react-hook-form";

import { ErrorInputStyle, FieldStyle } from "../Field/style.index";

const { TextArea } = Input;

interface TextAreaCustomProps extends TextAreaProps {
  margin_bottom?: string;
  name?: string;
  control?: Control<FieldValues | any>;
  errors?: string;
  validate_status?: ValidateStatus;
  defaultValue?: string | number;
  width?: string;
  format?: "number" | "wordNumber" | "word";
  forgot?: boolean;
  renderField?: any;
  label?: string;
  placeholder?: string;
}

const TextAreaCustom = (props: TextAreaCustomProps) => {
  const {
    name = "",
    control,
    errors,
    validate_status,
    defaultValue,
    label,
  } = props;
  return (
    <FieldStyle>
      {control ? (
        <Form.Item help={errors} validateStatus={validate_status}>
          <Controller
            name={name}
            defaultValue={defaultValue}
            control={control}
            render={({ field: { onChange, value } }) => {
              return (
                <ErrorInputStyle className={classNames({ errors })}>
                  {label && <label>{label}</label>}
                  <TextArea
                    className="form-control form-control-sm"
                    onChange={onChange}
                    value={value}
                    rows={4}
                    {...props}
                  />
                </ErrorInputStyle>
              );
            }}
          />
        </Form.Item>
      ) : (
        <TextArea {...props} />
      )}
    </FieldStyle>
  );
};

export default TextAreaCustom;
