import styled from "styled-components";

export const FieldStyle = styled.div`
  .ant-form-item {
    margin-bottom: 1.5rem !important;
    &-explain {
      color: red;
      font-size: 0.875rem;
      font-style: italic;
      margin-left: 5px;
      text-align: left;
    }
    &-control-input-content {
      input,
      textarea {
        &:hover {
          border-color: black;
        }
      }
    }

    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }

    input[type="number"] {
      -moz-appearance: textfield;
    }
  }
  .ant-input-affix-wrapper {
    display: flex;
    align-items: center;
    &:hover {
      border-color: black !important;
    }
    &:focus-within {
      border-color: black !important;
      box-shadow: unset !important;
    }
  }
`;

export const ErrorTextStyle = styled.span`
  color: red;
  position: absolute;
  font-size: 0.875rem;
  font-style: italic;
`;

export const ErrorInputStyle = styled.div`
  .form-control {
    &:disabled {
      color: black;
      &:hover {
        background-color: #ededed;
        border-color: #d9d9d9;
      }
    }
  }
  &.errors {
    .form-control {
      border-color: red !important;
      color: red;
      ::placeholder {
        color: red;
      }
    }
  }
`;
