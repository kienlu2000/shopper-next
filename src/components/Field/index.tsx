import { Form, Input, InputProps } from "antd";
import { ValidateStatus } from "antd/es/form/FormItem";
import classNames from "classnames";
import { Control, Controller, FieldValues } from "react-hook-form";

import { ErrorInputStyle, ErrorTextStyle, FieldStyle } from "./style.index";

export interface FieldProps extends InputProps {
  margin_bottom?: string;
  name?: string;
  control?: Control<FieldValues | any>;
  errors?: string;
  validate_status?: ValidateStatus;
  defaultValue?: string | number;
  width?: string;
  format?: "number" | "wordNumber" | "word";
  forgot?: boolean;
  renderField?: any;
  label?: string;
  placeholder?: string;
}

const Field = (props: FieldProps) => {
  const {
    name = "",
    control,
    errors,
    validate_status,
    defaultValue,
    renderField,
    label,
  } = props;

  return (
    <FieldStyle>
      {control ? (
        <Form.Item help={errors} validateStatus={validate_status}>
          <Controller
            name={name}
            defaultValue={defaultValue}
            control={control}
            render={({ field: { onChange, value } }) => {
              return (
                <ErrorInputStyle className={classNames({ errors })}>
                  {label && <label>{label}</label>}
                  {renderField ? (
                    renderField?.({
                      ...props,
                      onChange,
                      values: value as string,
                    })
                  ) : (
                    <Input
                      className="form-control form-control-sm"
                      onChange={onChange}
                      value={value}
                      {...props}
                    />
                  )}
                </ErrorInputStyle>
              );
            }}
          />
        </Form.Item>
      ) : (
        <ErrorInputStyle className={classNames("form-group", { errors })}>
          {label && <label>{label}</label>}
          {renderField ? (
            renderField?.(props)
          ) : (
            <Input
              className="form-control form-control-sm"
              defaultValue={defaultValue}
              {...props}
            />
          )}
          {errors && <ErrorTextStyle>{errors}</ErrorTextStyle>}
        </ErrorInputStyle>
      )}
    </FieldStyle>
  );
};

export default Field;
