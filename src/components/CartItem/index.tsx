import { Popconfirm, Spin } from "antd";
import classNames from "classnames";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import LoaderSvg from "src/assets/img/loader";
import { ImageError } from "src/constants";
import { PATH } from "src/constants/path";
import { useAppDispatch, useAppSelector } from "src/hooks/app-hooks";
import { selectPreCheckoutResponse } from "src/store/cart";
import {
  removeCartItemAction,
  selectCartItemAction,
  updateCartItemAction,
} from "src/store/cart/cart.action";
import { TypeProduct } from "src/types/products";
import { currency } from "src/utils/currency";

import ButtonCustom from "../Button";
import CheckboxCustom from "../CheckboxCustom";
import { CartItemStyle } from "./style.index";

interface State {
  isLoading: boolean;
  _quantity: number;
  popRemove: boolean;
  popQuantity: boolean;
}

interface Props extends TypeProduct {
  quantity: number;
  className?: string;
  onClose?: () => void;
  allowSelect?: boolean;
  hiddenActionQuantity?: boolean;
  footer?: React.ReactNode;
}

const CartItem = ({
  thumbnail_url,
  name,
  real_price,
  quantity,
  id,
  footer,
  slug,
  onClose,
  className,
  hiddenActionQuantity,
  allowSelect,
}: Props) => {
  const dispatch = useAppDispatch();

  const { listItems } = useAppSelector(selectPreCheckoutResponse);

  const [state, setState] = useState<State>({
    _quantity: quantity,
    isLoading: false,
    popQuantity: false,
    popRemove: false,
  });

  const onUpdateQuantityCurry = (quantity: number) => () => {
    if (quantity === 0) {
      setState((prev) => ({
        ...prev,
        isLoading: false,
      }));
      dispatch(
        removeCartItemAction({
          productId: id,
        }),
      );
      setState((prev) => ({
        ...prev,
        isLoading: true,
      }));
    } else {
      setState((prev) => ({
        ...prev,
        _quantity: quantity,
      }));
      dispatch(
        updateCartItemAction({
          productId: id,
          quantity: quantity,
        }),
      );
    }
  };

  const onSelectCartItem = (checked: boolean) => {
    dispatch(
      selectCartItemAction({
        productId: id,
        checked,
      }),
    );
  };

  const selected = !!listItems.find((e) => e.productId === id);

  const LinkToProduct = PATH.products + "/" + slug;

  useEffect(() => {
    if (state._quantity !== quantity) {
      setState((prev) => ({
        ...prev,
        _quantity: quantity,
      }));
    }
  }, [quantity]);

  return (
    <Spin indicator={<LoaderSvg />} spinning={state.isLoading}>
      <li
        className={classNames(
          "list-group-item max-sm:!py-6 max-sm:!px-0",
          className,
        )}
      >
        <div className="row align-items-center max-sm:!mx-0">
          <div className="col-4 flex items-center gap-4 max-sm:!px-2 max-sm:gap-1">
            {allowSelect && (
              <CheckboxCustom checked={selected} onChange={onSelectCartItem} />
            )}
            <Link href={LinkToProduct} onClick={onClose}>
              <img
                onError={({ currentTarget }) => {
                  currentTarget.onerror = null;
                  currentTarget.src = ImageError.src;
                }}
                className="img-fluid"
                src={thumbnail_url}
                alt="..."
              />
            </Link>
          </div>
          <div className="col-8">
            <div className="font-size-sm font-weight-bold mb-6">
              <Link
                href={LinkToProduct}
                className="text-body"
                onClick={onClose}
              >
                {name}
              </Link>
              <br />
              <span className="text-muted">{currency(real_price)}VND</span>
            </div>
            {!hiddenActionQuantity && (
              <div className="d-flex align-items-center">
                <CartItemStyle>
                  <Popconfirm
                    open={state.popQuantity}
                    onOpenChange={(visible) =>
                      setState((prev) => ({
                        ...prev,
                        popQuantity: visible,
                      }))
                    }
                    onConfirm={() => {
                      setState((prev) => ({
                        ...prev,
                        popQuantity: false,
                      }));
                      onUpdateQuantityCurry(0)();
                    }}
                    disabled={state._quantity > 1}
                    okButtonProps={{ hidden: true }}
                    okText="Remove"
                    showCancel={false}
                    placement="bottomLeft"
                    title="Warning"
                    description={
                      <div>
                        You definitely want to delete this product
                        <div className="flex justify-end gap-2 pt-5">
                          <ButtonCustom
                            onClick={() => {
                              setState((prev) => ({
                                ...prev,
                                popQuantity: false,
                              }));
                              onUpdateQuantityCurry(0)();
                            }}
                          >
                            Deleted
                          </ButtonCustom>
                        </div>
                      </div>
                    }
                  >
                    <button
                      onClick={() =>
                        state._quantity > 1 &&
                        onUpdateQuantityCurry(state._quantity - 1)()
                      }
                    >
                      -
                    </button>
                  </Popconfirm>
                  <input
                    value={state._quantity}
                    onChange={(ev) =>
                      setState((prev) => ({
                        ...prev,
                        _quantity: parseInt(ev.target?.value),
                      }))
                    }
                    onBlur={(ev) => {
                      let val = parseInt(ev.target?.value);
                      if (!val) {
                        val = 1;
                        setState((prev) => ({
                          ...prev,
                          _quantity: val,
                        }));
                      }
                      if (val !== quantity) {
                        onUpdateQuantityCurry(val);
                      }
                    }}
                  />
                  <button onClick={onUpdateQuantityCurry(state._quantity + 1)}>
                    +
                  </button>
                </CartItemStyle>
                <Popconfirm
                  open={state.popRemove}
                  okButtonProps={{ hidden: true }}
                  onOpenChange={(visible) =>
                    setState((prev) => ({
                      ...prev,
                      popRemove: visible,
                    }))
                  }
                  okText="Remove"
                  showCancel={false}
                  placement="bottomRight"
                  title="Warning"
                  description={
                    <div>
                      You definitely want to delete this product
                      <div className="flex justify-end gap-2 pt-5">
                        <ButtonCustom
                          onClick={() => {
                            setState((prev) => ({
                              ...prev,
                              popRemove: false,
                            }));
                            onUpdateQuantityCurry(0)();
                          }}
                        >
                          Deleted
                        </ButtonCustom>
                      </div>
                    </div>
                  }
                  onConfirm={() => {
                    setState((prev) => ({
                      ...prev,
                      popRemove: false,
                    }));
                    onUpdateQuantityCurry(0)();
                  }}
                >
                  <div
                    onClick={(ev) => ev.preventDefault()}
                    className="font-size-xs text-gray-400 ml-auto cursor-pointer"
                  >
                    <i className="fe fe-x" /> Remove
                  </div>
                </Popconfirm>
              </div>
            )}
            {footer}
          </div>
        </div>
      </li>
    </Spin>
  );
};
export default CartItem;
