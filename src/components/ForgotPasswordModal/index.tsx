import { yupResolver } from "@hookform/resolvers/yup";
import { Form, message, Modal } from "antd";
import { SubmitHandler, useForm } from "react-hook-form";
import { KEYS_MESSAGE } from "src/constants/keys-message";
import { MESSAGE } from "src/constants/message";
import { useMutationSendEmailResetPassword } from "src/mutate/user/mutationHooksCommon";
import { TypeSendEmailResetPasswordForm } from "src/types/user";
import { handleError } from "src/utils/handleError";
import { REGEXP } from "src/utils/RegExp";
import * as yup from "yup";

import ButtonCustom from "../Button";
import Field from "../Field";

interface Props {
  isOpen: boolean;
  isCancel: () => void;
}

const schema = yup
  .object({
    username: yup
      .string()
      .trim()
      .required(MESSAGE.validate.required)
      .matches(REGEXP.EMAIL, MESSAGE.validate.email)
      .test("emoji", MESSAGE.validate.emoji, (username: string): boolean => {
        if (!username) return false;
        const regex = new RegExp(REGEXP.EMOJI);
        return !regex.test(username);
      }),
  })
  .required();

const ForgotPasswordModal = ({ isCancel, isOpen }: Props) => {
  const { mutateAsync, isLoading } = useMutationSendEmailResetPassword();

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
    mode: "all",
  });

  const onSubmit: SubmitHandler<TypeSendEmailResetPasswordForm> = async (
    data: TypeSendEmailResetPasswordForm,
  ) => {
    try {
      mutateAsync(data)
        .then(() => {
          message.success({
            key: KEYS_MESSAGE.SEND_EMAIL_RESET_PASSWORD,
            content: MESSAGE.sendEmailResetPassword.success,
          });
          reset();
          isCancel();
        })
        .catch((err: any) => {
          handleError(err);
        });
    } catch (error: any) {
      handleError(error);
    }
  };

  return (
    <Modal
      closable={false}
      okButtonProps={{ style: { display: "none" } }}
      cancelButtonProps={{ style: { display: "none" } }}
      open={isOpen}
      onCancel={isCancel}
      styles={{ body: { padding: 0 } }}
    >
      <div className="modal-content">
        <button
          type="button"
          className="close"
          data-dismiss="modal"
          aria-label="Close"
        >
          <i onClick={isCancel} className="fe fe-x" aria-hidden="true" />
        </button>
        <div className="modal-header line-height-fixed font-size-lg max-sm:!p-2">
          <strong className="mx-auto">Forgot Password?</strong>
        </div>
        <div className="modal-body text-center max-sm:!p-4">
          <p className="mb-7 font-size-sm text-gray-500">
            Please enter your registered Email. You will receive an Email with a
            link to create a new password.
          </p>
          <Form onSubmitCapture={handleSubmit(onSubmit)}>
            <Field
              control={control}
              errors={errors.username?.message}
              name="username"
              placeholder="Email Address *"
            />
            <ButtonCustom loading={isLoading} className="inline-flex mt-1">
              Reset Password
            </ButtonCustom>
          </Form>
        </div>
      </div>
    </Modal>
  );
};

export default ForgotPasswordModal;
