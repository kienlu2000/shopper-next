import classNames from "classnames";
import React from "react";

import { SkeletonCustomStyle } from "./style.index";

interface Props {
  shape?: string;
  width?: number | string;
  height?: number | string;
  children?: React.ReactNode;
}

export default function SkeletonCustom({
  children,
  height,
  shape = "rectangle",
  width,
}: Props) {
  return (
    <SkeletonCustomStyle
      className={classNames(shape)}
      style={{ width, height }}
    >
      {children}
    </SkeletonCustomStyle>
  );
}
