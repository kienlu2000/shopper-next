/* eslint-disable no-unused-vars */

import { Drawer } from "antd";
import classNames from "classnames";
import Link from "next/link";
import { useMemo } from "react";
import { initialAddressValue } from "src/constants/initialValue";
import { PATH } from "src/constants/path";
import { useAddressUser } from "src/query/address/queryHooksCommon";
import { TypeAddressUser } from "src/types/user";

import { AddressCard } from "../AddressCard";
import AddressCardLoading from "../AddressCardLoading";

interface Props {
  isClose: () => void;
  isOpen: boolean;
  selected?: TypeAddressUser;
  onSelect?: (address: TypeAddressUser) => void;
}

const AddressDrawer = ({
  isOpen,
  isClose,
  selected,
  onSelect = () => initialAddressValue,
}: Props) => {
  const { data, isLoading } = useAddressUser("");

  const dataSort = useMemo(() => {
    if (!data) return null;

    const sortedData = [...data].sort((e) => (e.default ? -1 : 0));

    return sortedData;
  }, [data]);

  return (
    <Drawer
      width={470}
      open={isOpen}
      onClose={isClose}
      styles={{ header: { display: "none" }, body: { padding: 0 } }}
    >
      <div className="modal-content">
        <button
          type="button"
          className="close !outline-none"
          data-dismiss="modal"
          aria-label="Close"
          onClick={isClose}
        >
          <i className="fe fe-x" aria-hidden="true" />
        </button>
        <div className="modal-header line-height-fixed font-size-lg">
          <strong className="mx-auto">Select your address</strong>
        </div>
        <div className="list-group list-group-lg list-group-flush">
          {isLoading
            ? Array.from(Array(3)).map((_, i) => <AddressCardLoading key={i} />)
            : dataSort?.map((e) => (
                <AddressCard
                  {...e}
                  isClose={() => {
                    isClose();
                    onSelect(e);
                  }}
                  className={classNames(
                    "bg-white border-b !mb-0 hover:!bg-[#d9f5e0] cursor-pointer",
                    {
                      "!bg-[#d9f5e0]": selected?._id === e._id,
                    },
                  )}
                  hiddenAction
                  classDrawer=""
                  key={e._id}
                />
              ))}
        </div>
        <div className="modal-body mt-auto">
          <Link
            className="btn btn-block btn-outline-dark"
            href={PATH.profile.newAddress}
          >
            Thêm mới
          </Link>
        </div>
      </div>
    </Drawer>
  );
};

export default AddressDrawer;
