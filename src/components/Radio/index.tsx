/* eslint-disable no-unused-vars */

import classNames from "classnames";
import React, {
  createContext,
  ReactNode,
  useContext,
  useId,
  useRef,
} from "react";

type RadioProps = {
  children: ReactNode;
  value: string;
};

type GroupProps = {
  children: ReactNode;
  value: string;
  toggle?: boolean;
  onChange?: (ev: React.ChangeEvent<HTMLInputElement>) => void;
  onCheckedWhen2nd?: () => void;
};

interface RadioToggleProps {
  children: React.ReactNode;
  value: string | number;
}

type ContextType = {
  selected: string;
  onChange: (ev: React.ChangeEvent<HTMLInputElement>) => void;
  onClick: (radioValue: string) => void;
};

const Context = createContext<ContextType>({
  selected: "",
  onChange: (ev) => {},
  onClick: (radioValue) => {},
});

export const Radio = ({ children, ...props }: RadioProps) => {
  const id = useId();
  const { selected, onChange, onClick } = useContext(Context);

  return (
    <div className="custom-control custom-radio ">
      <input
        onChange={onChange}
        {...props}
        checked={props.value == selected}
        className="custom-control-input"
        type="radio"
        id={id}
      />
      <label
        onClick={() => onClick(props.value)}
        className="custom-control-label flex items-center"
        htmlFor={id}
      >
        {children}
      </label>
    </div>
  );
};

export const RadioGroup = ({
  children,
  value,
  toggle,
  onChange,
  onCheckedWhen2nd,
}: GroupProps) => {
  const uncheckRef = useRef(false);

  const newOnChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    if (!uncheckRef.current) {
      onChange?.(ev);
    }

    uncheckRef.current = false;
  };

  const onClick = (radioValue: string) => {
    if (onCheckedWhen2nd) {
      if (radioValue === value) {
        onCheckedWhen2nd?.();
        uncheckRef.current = true;
      }
    }
  };

  return (
    <div className={classNames({ "btn-group-toggle": toggle })}>
      <Context.Provider
        value={{ selected: value, onChange: newOnChange, onClick }}
      >
        {children}
      </Context.Provider>
    </div>
  );
};

export const RadioToggle: React.FC<RadioToggleProps> = ({
  children,
  ...props
}) => {
  const { selected, onChange } = useContext(Context);
  return (
    <label
      className={classNames("btn btn-sm !font-normal btn-outline-border", {
        active: props.value === selected,
      })}
    >
      <input
        onChange={onChange}
        checked={props.value === selected}
        type="radio"
        {...props}
      />
      {children}
    </label>
  );
};
