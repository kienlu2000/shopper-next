import { CheckCircleFilled } from "@ant-design/icons";
import { Dropdown, Popover } from "antd";
import Link from "next/link";
import { useSession } from "next-auth/react";
import { useMemo, useState } from "react";
import ButtonCustom from "src/components/Button";
import CartDrawer from "src/components/CartDrawer";
import SearchDrawer from "src/components/SearchDrawer";
import { AvatarDefault } from "src/constants";
import { PATH } from "src/constants/path";
import { handleIsAuthentic } from "src/helper/handleAuthentic";
import { handleLogout } from "src/helper/handleLogout";
import { useAppDispatch, useAppSelector } from "src/hooks/app-hooks";
import { useQueryProfileUser } from "src/query/user/queryHooksCommon";
import { selectCart, selectIsOpenPopover, togglePopover } from "src/store/cart";

interface State {
  searchDrawer: boolean;
  cartDrawer: boolean;
}

const Nav = () => {
  const { status } = useSession();
  const dispatch = useAppDispatch();

  const cart = useAppSelector(selectCart);
  const isOpenPopover = useAppSelector(selectIsOpenPopover);
  const isAuthentic = handleIsAuthentic(status);

  const { data: user } = useQueryProfileUser(isAuthentic);

  const [state, setState] = useState<State>({
    cartDrawer: false,
    searchDrawer: false,
  });

  const items = useMemo(() => {
    return [
      {
        key: 1,
        label: <Link href={PATH.profile.index}>Tài khoản</Link>,
      },
      {
        key: 3,
        label: <Link href={PATH.profile.order}>Đơn hàng</Link>,
      },
      {
        key: 2,
        label: <div onClick={handleLogout()}>Đăng xuất</div>,
      },
    ];
  }, []);

  return (
    <ul className="navbar-nav flex-row items-center">
      <li
        onClick={() =>
          setState((prev) => ({
            ...prev,
            searchDrawer: true,
          }))
        }
        className="nav-item"
      >
        <div className="nav-link cursor-pointer" data-toggle="modal">
          <i className="fe fe-search" />
        </div>
      </li>
      <li className="nav-item ml-lg-n4">
        <Link className="nav-link" href={PATH.profile.collection}>
          <i className="fe fe-heart" />
        </Link>
      </li>
      <li className="nav-item ml-lg-n4">
        <Popover
          onOpenChange={(visible) => {
            if (!visible) {
              dispatch(togglePopover(false));
            }
          }}
          trigger="click"
          open={isOpenPopover}
          placement="bottomRight"
          content={
            <>
              <p className="mb-0 flex gap-2 items-center">
                <span className="text-green-500">
                  <CheckCircleFilled />
                </span>
                Thêm sản phẩm vào giỏ hàng thành công
              </p>
              <ButtonCustom
                onClick={() => dispatch(togglePopover(false))}
                link={PATH.myCart}
                className="w-full btn-xs mt-2"
              >
                Xem giỏ hàng và thanh toán
              </ButtonCustom>
            </>
          }
        >
          <div
            onClick={() =>
              setState((prev) => ({
                ...prev,
                cartDrawer: true,
              }))
            }
            className="nav-link cursor-pointer"
          >
            <span data-cart-items={cart?.totalQuantity || undefined}>
              <i className="fe fe-shopping-cart" />
            </span>
          </div>
        </Popover>
      </li>
      <li className="nav-item ml-lg-n4">
        {user ? (
          <Dropdown menu={{ items }} placement="bottomRight" arrow>
            <div className="header-avatar nav-link max-sm:ml-1">
              <img src={user.avatar || AvatarDefault.src} alt="avatar-user" />
            </div>
          </Dropdown>
        ) : (
          <Link className="nav-link" href={PATH.authentication}>
            <i className="fe fe-user" />
          </Link>
        )}
      </li>
      <SearchDrawer
        isOpen={state.searchDrawer}
        isCancel={() =>
          setState((prev) => ({
            ...prev,
            searchDrawer: false,
          }))
        }
      />
      <CartDrawer
        isOpen={state.cartDrawer}
        isClose={() =>
          setState((prev) => ({
            ...prev,
            cartDrawer: false,
          }))
        }
      />
    </ul>
  );
};

export default Nav;
