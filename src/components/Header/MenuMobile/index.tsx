import classNames from "classnames";
import { useRouter } from "next/navigation";
import React, { useState } from "react";
import { LIST_SUB_HEADER } from "src/constants/Header/list-sub-header";

import { MenuMobileStyle } from "./style.index";

const MenuMobile = () => {
  const router = useRouter();

  const [isShow, setIsShow] = useState<boolean>(false);

  const handleNavigateMobile = (link: string) => {
    router.push(link);
    setIsShow(false);
  };

  return (
    <MenuMobileStyle className="hidden max-sm:flex flex-1">
      <div className="dropdown">
        <label className="dropdown__face" onClick={() => setIsShow(!isShow)}>
          <div className="dropdown__text">Menu</div>
          <div className="dropdown__arrow" />
        </label>
        <ul
          className={classNames("dropdown__items bg-light", {
            "--active": isShow,
          })}
        >
          {LIST_SUB_HEADER.map((item) => (
            <div
              onClick={() => handleNavigateMobile(item.link)}
              className="nav-link"
              key={item.key}
            >
              <li>{item.name}</li>
            </div>
          ))}
        </ul>
      </div>
      <svg>
        <filter id="goo">
          <feGaussianBlur in="SourceGraphic" stdDeviation={10} result="blur" />
          <feColorMatrix
            in="blur"
            type="matrix"
            values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7"
            result="goo"
          />
          <feBlend in="SourceGraphic" in2="goo" />
        </filter>
      </svg>
    </MenuMobileStyle>
  );
};

export default MenuMobile;
