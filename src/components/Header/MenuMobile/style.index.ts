import styled from "styled-components";

export const MenuMobileStyle = styled.div`
  z-index: 9;
  flex: 1;
  .dropdown {
    position: relative;
    filter: url(#goo);
    flex: 1;
    &__face,
    &__items {
      padding: 20px;
      border-radius: 25px;
      background-color: #f5f5f5;
    }

    &__face {
      display: block;
      position: relative;
    }

    &__items {
      margin: 0;
      position: absolute;
      right: 0;
      top: 50%;
      width: calc(100% + 65px);
      list-style: none;
      list-style-type: none;
      display: flex;
      justify-content: space-between;
      visibility: hidden;
      z-index: -1;
      opacity: 0;
      transition: all 0.4s cubic-bezier(0.93, 0.88, 0.1, 0.8);
      flex-direction: column;

      &.--active {
        top: calc(100% + 15px);
        visibility: visible;
        opacity: 1;
      }
      &::before {
        content: "";
        background-color: #f5f5f5;
        position: absolute;
        bottom: 100%;
        right: 20%;
        height: 20px;
        width: 20px;
      }
    }

    &__arrow {
      border-bottom: 2px solid #000;
      border-right: 2px solid #000;
      position: absolute;
      top: 50%;
      right: 30px;
      width: 10px;
      height: 10px;
      transform: rotate(45deg) translateY(-50%);
      transform-origin: right;
    }

    .dropdown__items {
    }
  }

  svg {
    display: none;
  }
`;
