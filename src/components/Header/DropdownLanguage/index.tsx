import React from "react";
import { LIST_LANGUAGE } from "src/constants/Header/list-language";

const DropdownLanguage = () => {
  return (
    <li className="nav-item flex items-center pr-3">
      <div
        className="dropdown-toggle cursor-pointer flex"
        data-toggle="dropdown"
      >
        <img
          className="mb-1 mr-1"
          src="./static/img/flags/usa.svg"
          alt="United States"
        />
        United States
      </div>
      <div className="dropdown-menu minw-0">
        {LIST_LANGUAGE.map((item) => (
          <div key={item.key} className="dropdown-item">
            <img className="mb-1 mr-2" src={item.img} alt={item.name} />
            {item.name}
          </div>
        ))}
      </div>
      {/* <li className="nav-item">
                <Dropdown
                  arrow
                  placement="topRight"
                  menu={{
                    items: _.map(LANG, (title, code) => ({
                      key: code,
                      label: title,
                      onClick: () => setLang(code),
                    })),
                    [
                        {
                            label: 'English',
                            onClick: () => setLang('en')
                        },
                        {
                            label: 'Tiếng Việt',
                            onClick: () => setLang('vi')
                        },
                        {
                            label: 'China',
                            onClick: () => setLang('zh')
                        },
                    ]
                  }}
                >
                  <Link className="nav-link dropdown-toggle" href="#">
                    {LANG[lang]}
                  </Link>
                </Dropdown>
              </li> */}
    </li>
  );
};

export default DropdownLanguage;
