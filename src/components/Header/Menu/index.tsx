import classNames from "classnames";
import Link from "next/link";
import { usePathname } from "next/navigation";
import React from "react";
import { LIST_SUB_HEADER } from "src/constants/Header/list-sub-header";

const Menu = () => {
  const pathname = usePathname();

  return (
    <ul className="navbar-nav mx-auto max-sm:hidden max-lg:flex-1">
      {LIST_SUB_HEADER.map((item) => (
        <li key={item.key} className="nav-item">
          <Link
            className={classNames(
              "nav-link",
              pathname === item.link ? "active" : "",
            )}
            href={item.link}
          >
            {item.name}
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default Menu;
