"use client";

import Link from "next/link";
import { PATH } from "src/constants/path";

import HelperUser from "./HelperUser";
import Menu from "./Menu";
import MenuMobile from "./MenuMobile";
import Nav from "./Nav";
import Social from "./Social";

const Header = () => {
  return (
    <>
      <div className="navbar navbar-topbar navbar-expand-xl navbar-light bg-light">
        <div className="container">
          <div className="mr-xl-8">
            <i className="fe fe-truck mr-2" />
            <span className="heading-xxxs">WORLDWIDE SHIPPING</span>
          </div>

          <div className="navbar-collapse" id="topbarCollapse">
            <ul className="nav nav-divided navbar-nav mr-auto z-10"></ul>

            <HelperUser />

            <Social />
          </div>
        </div>
      </div>
      <nav className="navbar navbar-expand-lg navbar-light bg-white">
        <div className="container">
          <div className="flex items-center max-sm:w-full">
            <Link
              className="navbar-brand !flex items-center gap-3 !font-medium"
              href={PATH.home}
            >
              <img
                style={{
                  height: "50px",
                }}
                loading="lazy"
                src="https://spacedev.vn/images/LOGO-image-full.svg"
                alt="shopper-logo"
              />
              <span className="navbar-brand max-sm:hidden">Shopper</span>
            </Link>

            <MenuMobile />
          </div>

          <div
            className="navbar-collapse max-sm:flex justify-between max-lg:flex"
            id="navbarCollapse"
          >
            <Menu />

            <Nav />
          </div>
        </div>
      </nav>
    </>
  );
};

export default Header;
