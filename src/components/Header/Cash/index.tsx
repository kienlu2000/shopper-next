import React from "react";
import { LIST_CASH } from "src/constants/Header/list-cash";

const Cash = () => {
  return (
    <li className="nav-item dropdown">
      <div className="nav-link dropdown-toggle" data-toggle="dropdown">
        USD
      </div>
      <div className="dropdown-menu minw-0">
        {LIST_CASH.map((item) => (
          <div key={item.key} className="dropdown-item">
            {item.name}
          </div>
        ))}
      </div>
    </li>
  );
};

export default Cash;
