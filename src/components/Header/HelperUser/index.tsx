import Link from "next/link";
import React from "react";
import { LIST_HELPER_USER } from "src/constants/Header/list-helper-user";

const HelperUser = () => {
  return (
    <ul className="nav navbar-nav mr-8">
      {LIST_HELPER_USER.map((item) => (
        <li key={item.key} className="nav-item">
          <Link className="nav-link" href={item.link}>
            {item.name}
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default HelperUser;
