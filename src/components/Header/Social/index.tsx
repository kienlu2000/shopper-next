import Link from "next/link";
import React from "react";
import { LIST_SOCIAL } from "src/constants/Header/list-social";

const Social = () => {
  return (
    <ul className="nav navbar-nav flex-row">
      {LIST_SOCIAL.map((item) => (
        <li key={item.key} className="nav-item ml-xl-n4">
          <Link className="nav-link text-gray-350" href={item.link}>
            <i className={item.icon} />
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default Social;
