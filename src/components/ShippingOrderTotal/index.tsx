import React from "react";
import { TypeOrderItem } from "src/types/order";
import { currency } from "src/utils/currency";

const ShippingOrderTotal = ({
  subTotal,
  tax,
  promotion,
  shipping,
  total,
}: TypeOrderItem) => {
  return (
    <div className="card card-lg mb-5 border">
      <div className="card-body">
        <h6 className="mb-7">Order Total</h6>
        <ul className="list-group list-group-sm list-group-flush-y list-group-flush-x">
          <li className="list-group-item d-flex">
            <span>Subtotal</span>
            <span className="ml-auto">{currency(subTotal)}</span>
          </li>
          <li className="list-group-item d-flex">
            <span>Tax</span>
            <span className="ml-auto">{currency(tax)}</span>
          </li>
          <li className="list-group-item d-flex">
            <span>Promotion</span>
            <span className="ml-auto">
              {currency(promotion && promotion.discount)}
            </span>
          </li>
          <li className="list-group-item d-flex">
            <span>Shipping</span>
            <span className="ml-auto">{currency(shipping.shippingPrice)}</span>
          </li>
          <li className="list-group-item d-flex font-size-lg font-weight-bold">
            <span>Total</span>
            <span className="ml-auto">{currency(total)}</span>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default ShippingOrderTotal;
