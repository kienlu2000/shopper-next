import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { all } from "redux-saga/effects";
import { TypeCategoriesState } from "src/types/categories";

import { DEVELOPMENT } from "../service";
import { TypeCartState } from "../types/cart";
import cartReducer from "./cart";
import { getCartAction } from "./cart/cart.action";
import { cartSaga } from "./cart/cart.saga";
import categoriesReducer from "./categories";
import { getCategoriesAction } from "./categories/categories.action";
import { categoriesSaga } from "./categories/categories.saga";

const sagaMIddleware = createSagaMiddleware();

function* rootSaga() {
  yield all([cartSaga(), categoriesSaga()]);
}

const rootReducers = {
  cart: cartReducer,
  categories: categoriesReducer,
};

const store = configureStore({
  reducer: rootReducers,
  middleware: (getMiddleware) => getMiddleware().concat(sagaMIddleware),
  devTools: DEVELOPMENT === "development",
});

export type AppDispatch = typeof store.dispatch;

export interface ApplicationRootState {
  readonly cart: TypeCartState;
  readonly categories: TypeCategoriesState;
}

sagaMIddleware.run(rootSaga);

store.dispatch(getCategoriesAction());
store.dispatch(getCartAction());

export default store;
