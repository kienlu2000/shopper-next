import { PayloadAction } from "@reduxjs/toolkit";
import { call, delay, put, race, select, takeLatest } from "redux-saga/effects";
import { cartService } from "src/service/cart";
import { TypeError } from "src/types";
import {
  TypeCartState,
  TypeListItemPreCheckoutResponse,
  TypePreCheckoutCartAction,
  TypeRemoveCartItemAction,
  TypeSelectCartItemAction,
  TypeUpdateCartQuantityAction,
} from "src/types/cart";
import { TypePromotionItemAction } from "src/types/promotion";
import { getToken } from "src/utils/cookies";
import { handleError } from "src/utils/handleError";

import {
  actionAddProductLoading,
  actionProductLoading,
  changeShippingMethod,
  clearCart,
  isActionPromotionLoading,
  isPreCheckoutLoading,
  setCart,
  setPreCheckoutData,
  setPreCheckoutResponse,
  togglePopover,
  togglePromotionCode,
} from ".";
import {
  addPromotionCodeAction,
  getCartAction,
  removeCartItemAction,
  removePromotionCodeAction,
  selectCartItemAction,
  updateCartItemAction,
  updateQuantityCartSuccessAction,
} from "./cart.action";

function* fetchCart() {
  if (getToken()) {
    const { cart } = yield race({
      cart: call(cartService.getCart),
    });

    if (cart) {
      yield put(setCart(cart.data));
    }
  } else {
    yield put(
      setCart({
        listItems: [],
        subTotal: 0,
        totalQuantity: 0,
      }),
    );
  }
}

function* fetchCartItem(action: PayloadAction<TypeUpdateCartQuantityAction>) {
  try {
    yield put(actionAddProductLoading(true));
    yield put(
      actionProductLoading({
        productId: action.payload.productId,
        loading: true,
      }),
    );
    yield delay(200);
    if (action.payload.quantity >= 1) {
      yield call(
        cartService.updateCart,
        action.payload.productId,
        action.payload.quantity,
      );

      yield put(getCartAction());

      if (action.payload.showPopover) {
        yield put(togglePopover(true));

        window.scroll({
          top: 0,
          behavior: "smooth",
        });
      }
      yield put(
        updateQuantityCartSuccessAction({
          productId: action.payload.productId,
        }),
      );
    } else {
      yield put(
        removeCartItemAction({
          productId: action.payload.productId,
        }),
      );
    }
  } catch (err: any) {
    const _error: TypeError = err;
    throw new Error(_error.response.data.message);
  } finally {
    yield put(actionAddProductLoading(false));
    yield put(
      actionProductLoading({
        productId: action.payload.productId,
        loading: false,
      }),
    );
  }
}

function* fetchRemoveCartItem(action: PayloadAction<TypeRemoveCartItemAction>) {
  try {
    yield call(cartService.removeItem, action.payload.productId);
    yield put(getCartAction());
    yield put(
      updateQuantityCartSuccessAction({
        productId: action.payload.productId,
      }),
    );
  } catch (error: any) {
    const _error: TypeError = error;
    throw new Error(_error.response.data.message);
  }
}

function* fetchSelectCartItem(action: PayloadAction<TypeSelectCartItemAction>) {
  try {
    const { cart }: { cart: TypeCartState } = yield select();

    if (cart) {
      const { checked, productId } = action.payload;
      let { listItems } = cart.preCheckoutData;

      const preCheckoutData = cart.preCheckoutData;

      listItems = [...listItems];

      if (checked) {
        listItems.push(productId);
      } else {
        listItems = listItems.filter((e) => e != productId);
      }

      yield put(
        setPreCheckoutData({
          ...preCheckoutData,
          listItems,
        }),
      );
    }
  } catch (error: any) {
    handleError(error);
  }
}

function* fetchPreCheckout(action: TypePreCheckoutCartAction) {
  try {
    const { cart }: { cart: TypeCartState } = yield select();

    if (cart && cart.preCheckoutData) {
      const preCheckoutData = cart.preCheckoutData;

      if (action.type === updateQuantityCartSuccessAction.toString()) {
        let { productId } = action.payload;

        if (!preCheckoutData.listItems.find((e) => e === productId)) return;
      }
      yield put(isPreCheckoutLoading(true));

      const res: TypeListItemPreCheckoutResponse = yield call(
        cartService.preCheckout,
        preCheckoutData,
      );

      yield put(setPreCheckoutResponse(res.data));

      yield put(isPreCheckoutLoading(false));
    }
  } catch (error: any) {
    handleError(error);
  }
}

function* fetchPromotionCode(action: PayloadAction<TypePromotionItemAction>) {
  try {
    yield put(isActionPromotionLoading(true));
    yield call(cartService.getPromotion, action.payload.code);
    yield put(togglePromotionCode(action.payload.code));
  } catch (error: any) {
    handleError(error);
  } finally {
    yield put(isActionPromotionLoading(false));
  }
}

function* fetchRemovePromotionCode() {
  yield put(togglePromotionCode(""));
}

export function* cartSaga() {
  yield takeLatest(updateCartItemAction, fetchCartItem);
  yield takeLatest(removeCartItemAction, fetchRemoveCartItem);
  yield takeLatest([getCartAction, clearCart], fetchCart);
  yield takeLatest(selectCartItemAction, fetchSelectCartItem);
  yield takeLatest(
    [
      setPreCheckoutData,
      updateQuantityCartSuccessAction,
      togglePromotionCode,
      changeShippingMethod,
    ],
    fetchPreCheckout,
  );
  yield takeLatest(addPromotionCodeAction, fetchPromotionCode);
  yield takeLatest(removePromotionCodeAction, fetchRemovePromotionCode);
}
