import { createSlice } from "@reduxjs/toolkit";
import { PayloadAction } from "@reduxjs/toolkit";
import {
  TypeActionProductLoading,
  TypeCartItem,
  TypeCartState,
  TypePreCheckoutDataForm,
  TypePreCheckoutItem,
} from "src/types/cart";

import { ApplicationRootState } from "..";

export const initialState: TypeCartState = {
  preCheckoutData: {
    listItems: [],
    promotionCode: [],
    shippingMethod: "mien-phi",
  },
  preCheckoutLoading: false,
  actionPromotionLoading: false,
  preCheckoutResponse: {
    total: 0,
    subTotal: 0,
    tax: 0,
    promotion: null,
    shipping: {
      shippingMethod: "",
      shippingPrice: 0,
    },
    totalQuantity: 0,
    viewCartTotal: 0,
    listItems: [],
  },
  openPopover: false,
  cart: {
    listItems: [],
    subTotal: 0,
    totalQuantity: 0,
  },
  actionOrderComplete: "",
  loading: {},
  productLoading: false,
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    clearCart(state) {
      return {
        ...state,
        preCheckoutData: {
          listItems: [],
          promotionCode: [],
          shippingMethod: "mien-phi",
        },
        preCheckoutLoading: false,
        actionPromotionLoading: false,
        preCheckoutResponse: {
          total: 0,
          subTotal: 0,
          tax: 0,
          promotion: null,
          shipping: {
            shippingMethod: "",
            shippingPrice: 0,
          },
          totalQuantity: 0,
          viewCartTotal: 0,
          listItems: [],
        },
        openPopover: false,
      };
    },
    actionProductLoading(
      state,
      action: PayloadAction<TypeActionProductLoading>,
    ) {
      state.loading[action.payload.productId] = action.payload.loading;
    },
    actionAddProductLoading(state, action: PayloadAction<boolean>) {
      state.productLoading = action.payload;
    },
    actionIdOrderComplete(state, action: PayloadAction<string>) {
      state.actionOrderComplete = action.payload;
    },
    togglePopover(state, action: PayloadAction<boolean>) {
      state.openPopover = action.payload;
    },
    setCart(state, action: PayloadAction<TypeCartItem>) {
      state.cart = action.payload;
    },
    setPreCheckoutData(state, action: PayloadAction<TypePreCheckoutDataForm>) {
      state.preCheckoutData = action.payload;
    },
    setPreCheckoutResponse(state, action: PayloadAction<TypePreCheckoutItem>) {
      state.preCheckoutResponse = action.payload;
    },
    isPreCheckoutLoading(state, action: PayloadAction<boolean>) {
      state.preCheckoutLoading = action.payload;
    },
    togglePromotionCode(state, action) {
      if (action.payload) {
        state.preCheckoutData.promotionCode = [action.payload];
      } else {
        state.preCheckoutData.promotionCode = [];
      }
    },
    isActionPromotionLoading(state, action: PayloadAction<boolean>) {
      state.actionPromotionLoading = action.payload;
    },
    changeShippingMethod(state, action: PayloadAction<string>) {
      state.preCheckoutData.shippingMethod = action.payload;
    },
  },
});

export const { togglePopover } = cartSlice.actions;

export const { setCart } = cartSlice.actions;

export const { setPreCheckoutData } = cartSlice.actions;

export const { setPreCheckoutResponse } = cartSlice.actions;

export const { isPreCheckoutLoading } = cartSlice.actions;

export const { isActionPromotionLoading } = cartSlice.actions;

export const { togglePromotionCode } = cartSlice.actions;

export const { changeShippingMethod } = cartSlice.actions;

export const { clearCart } = cartSlice.actions;

export const { actionIdOrderComplete } = cartSlice.actions;

export const { actionProductLoading } = cartSlice.actions;

export const { actionAddProductLoading } = cartSlice.actions;

export const selectIsOpenPopover = (state: ApplicationRootState) =>
  state.cart.openPopover;

export const selectPreCheckoutResponse = (state: ApplicationRootState) =>
  state.cart.preCheckoutResponse;

export const selectIsPreCheckoutLoading = (state: ApplicationRootState) =>
  state.cart.preCheckoutLoading;

export const selectIsActionPromotionLoading = (state: ApplicationRootState) =>
  state.cart.actionPromotionLoading;

export const selectPreCheckoutData = (state: ApplicationRootState) =>
  state.cart.preCheckoutData;

export const selectIdOrderComplete = (state: ApplicationRootState) =>
  state.cart.actionOrderComplete;

export const selectAddProductLoading = (state: ApplicationRootState) =>
  state.cart.productLoading;

export const selectCart = (state: ApplicationRootState) => state.cart.cart;

export default cartSlice.reducer;
