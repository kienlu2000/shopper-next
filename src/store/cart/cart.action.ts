import { createAction } from "@reduxjs/toolkit";
import {
  TypeRemoveCartItemAction,
  TypeSelectCartItemAction,
  TypeUpdateCartQuantityAction,
} from "src/types/cart";
import { TypePromotionItemAction } from "src/types/promotion";

export const updateCartItemAction = createAction<TypeUpdateCartQuantityAction>(
  "cart/updateQuantity",
);

export const getCartAction = createAction("cart/getCart");

export const removeCartItemAction =
  createAction<TypeRemoveCartItemAction>("cart/removeItem");

export const selectCartItemAction = createAction<TypeSelectCartItemAction>(
  "cart/selectCartItem",
);

export const updateQuantityCartSuccessAction =
  createAction<TypeRemoveCartItemAction>(
    "cart/updateQuantityCartSuccessAction",
  );

export const addPromotionCodeAction = createAction<TypePromotionItemAction>(
  "cart/addPromotionCode",
);

export const removePromotionCodeAction = createAction(
  "cart/removePromotionCode",
);
