import { call, put, race, takeLatest } from "redux-saga/effects";
import { productService } from "src/service/products";
import { TypeError } from "src/types";

import { setCategories, setLoadingCategories } from ".";
import { getCategoriesAction } from "./categories.action";

function* fetchCategories() {
  try {
    yield put(setLoadingCategories(true));
    const { categories } = yield race({
      categories: call(productService.getCategories),
    });

    if (categories) {
      yield put(setCategories(categories.data));
      yield put(setLoadingCategories(false));
    }
  } catch (error: any) {
    const _error: TypeError = error;
    throw new Error(_error.response.data.message);
  } finally {
    yield put(setLoadingCategories(false));
  }
}

export function* categoriesSaga() {
  yield takeLatest(getCategoriesAction, fetchCategories);
}
