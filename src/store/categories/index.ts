import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  TypeCategoriesDetail,
  TypeCategoriesState,
} from "src/types/categories";

import { ApplicationRootState } from "..";

export const initialState: TypeCategoriesState = {
  categories: [
    {
      _id: "",
      id: 0,
      parent: null,
      parent_id: 0,
      position: 0,
      slug: "",
      status: 0,
      title: "",
    },
  ],
  loading: false,
};

const categoriesSlice = createSlice({
  name: "categories",
  initialState,
  reducers: {
    setCategories(state, action: PayloadAction<TypeCategoriesDetail[]>) {
      state.categories = action.payload;
    },
    setLoadingCategories(state, action: PayloadAction<boolean>) {
      state.loading = action.payload;
    },
  },
});

export const { setCategories } = categoriesSlice.actions;

export const { setLoadingCategories } = categoriesSlice.actions;

export const selectCategories = (state: ApplicationRootState) =>
  state.categories.categories;

export const selectLoadingCategories = (state: ApplicationRootState) =>
  state.categories.loading;

export default categoriesSlice.reducer;
