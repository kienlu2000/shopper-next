import { createAction } from "@reduxjs/toolkit";

export const getCategoriesAction = createAction("categories/getCategories");
