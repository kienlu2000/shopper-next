export const handleIsAuthentic = (
  status: "authenticated" | "loading" | "unauthenticated",
) => {
  if (status === "authenticated") return true;

  return false;
};
