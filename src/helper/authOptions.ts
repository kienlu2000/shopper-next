import { cookies } from "next/headers";
import { AuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { authService } from "src/service/auth";
import { TypeError } from "src/types";
import { TypeLoginForm, TypeToken } from "src/types/auth";

const authOptions: AuthOptions = {
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        username: {
          label: "Email",
          type: "email",
        },
        password: { label: "Password", type: "password" },
      },

      async authorize(credentials) {
        const { username, password } = credentials as TypeLoginForm;

        try {
          const response = await authService.login({
            username,
            password,
          });

          const dataResponse: TypeToken = response.data;

          cookies().set("token", dataResponse.accessToken);
          cookies().set("refreshToken", dataResponse.refreshToken);

          return {
            id: username,
            ...dataResponse,
          };
        } catch (error: any) {
          const _error: TypeError = error;
          throw new Error(_error.response.data.message);
        }
      },
    }),
  ],
  pages: {
    signIn: "/auth",
  },
  session: {
    strategy: "jwt",
  },
  callbacks: {
    async session({ session }) {
      return session;
    },
  },
  secret: process.env.AUTH_SECRET,
};

export default authOptions;
