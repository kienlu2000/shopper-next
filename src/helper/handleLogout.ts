import { signOut } from "next-auth/react";

import { deleteRefreshToken, deleteToken } from "../utils/cookies";

export const handleLogout = () => () => {
  signOut();
  deleteToken();
  deleteRefreshToken();
};
