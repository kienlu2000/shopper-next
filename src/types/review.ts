import { TypePaginateResponse } from "./paginate";
import { TypeProductItem } from "./products";

interface TypeReviewer {
  avatar: string;
  name: string;
  _id: string;
}

export interface TypeReviewItem {
  content: string;
  createdAt: number;
  orderId: string;
  productId: number;
  senderId: string;
  star: number;
  user: TypeReviewer;
  _id: string;
}

export interface TypeTopReviewItem {
  content: string;
  createdAt: number;
  orderId: string;
  product: TypeProductItem;
  productId: number;
  senderId: string;
  star: number;
  user: TypeReviewer;
  _id: string;
}

export interface TypeReviewResponse extends TypePaginateResponse {
  data: TypeReviewItem[];
}

export interface TypeTopReviewResponse extends TypePaginateResponse {
  data: TypeTopReviewItem[];
}

export interface TypeAddReviewForm {
  orderId: string;
  content: string;
  star: number;
}

export interface TypeAddReview {
  _id: string;
  orderId: string;
  productId: number;
  senderId: string;
  createdAt: number;
  content: string;
  star: number;
}

export interface TypeAddReviewResponse {
  data: TypeAddReview;
}
