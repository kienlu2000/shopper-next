export interface TypePromotionItem {
  code: string;
  title: string;
  description: string;
  value: number;
  discount?: number;
}

export interface TypePromotionResponse {
  data: TypePromotionItem;
}

export interface TypePromotionItemAction {
  code: string;
}
