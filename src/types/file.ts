export interface TypeUploadFileResponse {
  success: boolean;
  link: string;
}
