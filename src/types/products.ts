import { TypePaginateResponse } from "./paginate";

export interface TypeProduct {
  id: number;
  name: string;
  price: number;
  rating_average: number;
  real_price: number;
  review_count: number;
  slug: string;
  thumbnail_url: string;
  _id: string;
}

interface TypeConfigurableOptions {
  code: string;
  name: string;
  position: number;
  values: [
    {
      label: string;
    },
  ];
}

export interface TypeImageProductItem {
  label: null;
  position: null;
  base_url: string;
  thumbnail_url: string;
  small_url: string;
  medium_url: string;
  large_url: string;
  is_gallery: true;
}

interface TypeSpecificationsProductItem {
  name: string;
  attributes: [
    {
      name: string;
      value: string;
    },
  ];
}

export interface TypeStockItem {
  qty: number;
  min_sale_qty: number;
  max_sale_qty: number;
  preorder_date: boolean;
}

export interface TypeProductItem {
  categories: number;
  id: number;
  name: string;
  thumbnail_url: string;
}

export interface TypeProductDetail {
  _id: string;
  id: number;
  sku: string;
  brand_name: string;
  categories: number;
  configurable_options: Array<TypeConfigurableOptions>;
  description: string;
  discount: number;
  discount_rate: number;
  images: Array<TypeImageProductItem>;
  inventory_status: string;
  name: string;
  price: number;
  real_price: number;
  price_usd: number;
  rating_average: number;
  review_count: number;
  short_description: string;
  specifications: Array<TypeSpecificationsProductItem>;
  stock_item: TypeStockItem;
  thumbnail_url: string;
  top_features: Array<string>;
  type: string;
  slug: string;
  current_seller: null;
}

export interface TypeProductResponse extends TypePaginateResponse {
  data: Array<TypeProductDetail>;
}
