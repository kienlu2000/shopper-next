import { TypeListItemOrder } from "./order";
import { TypePromotionItem } from "./promotion";

export interface TypeFormCheckout {
  payment: {
    paymentMethod: string;
  };
  promotionCode: Array<string>;
  listItems: Array<number>;
  shipping: TypeShippingCheckout;
  note: string;
}
interface TypeShippingCheckout {
  shippingMethod: string;
  shippingPrice?: number;
  fullName: string;
  phone: string;
  email: string;
  province: string;
  district: string;
  address: string;
}

interface TypeCheckoutItem {
  total: number;
  subTotal: number;
  tax: number;
  promotion: TypePromotionItem;
  shipping: TypeShippingCheckout;
  totalQuantity: number;
  viewCartTotal: number;
  listItems: Array<TypeListItemOrder>;
  user: string;
  payment: {
    paymentMethod: string;
  };
  note: string;
  type: string;
  status: "pending" | "confirm" | "shipping" | "finished" | "cancel" | string;
  createdAt: number;
  _id: string;
}

export interface TypeCheckoutResponse {
  data: TypeCheckoutItem;
}

export interface TypeAddUserCheckout {
  fullName: string;
  phone: string;
  email: string;
  province: string;
  district: string;
  address: string;
}
