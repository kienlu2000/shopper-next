import { TypeProduct } from "./products";
import { TypePromotionItem } from "./promotion";

export interface TypeCartState {
  cart: TypeCartItem;
  openPopover: boolean;
  preCheckoutData: TypePreCheckoutDataForm;
  preCheckoutLoading: boolean;
  preCheckoutResponse: TypePreCheckoutItem;
  actionPromotionLoading: boolean;
  actionOrderComplete: string;
  loading: TypeProductLoading;
  productLoading: boolean;
}
type TypeProductLoading = Record<number, boolean>;

export interface TypeUpdateCartQuantityAction {
  quantity: number;
  productId: number;
  showPopover?: boolean;
}

export interface TypeSelectCartItemAction {
  productId: number;
  checked: boolean;
}

export interface TypeRemoveCartItemAction {
  productId: number;
}

interface TypeListCartItem {
  productId: number;
  quantity: number;
  product: TypeProduct;
}

export interface TypeCartItem {
  subTotal: number;
  totalQuantity: number;
  listItems: TypeListCartItem[];
}

export interface TypePreCheckoutDataForm {
  listItems: Array<number>;
  promotionCode: Array<string>;
  shippingMethod: string;
}

export interface TypeShippingMethod {
  price: number;
  title: string;
  description: string;
  code: string;
}

export interface TypeShippingMethodResponse {
  data: TypeShippingMethod[];
}

export interface TypeCartItemResponse {
  data: TypeCartItem;
}

export interface TypePreCheckoutItem {
  total: number;
  subTotal: number;
  tax: number;
  promotion: TypePromotionItem | null;
  shipping: TypeShippingDetail;
  totalQuantity: number;
  viewCartTotal: number;
  listItems: TypeListItemPreCheckout[];
}

export interface TypeListItemPreCheckout {
  productId: number;
  quantity: number;
  price: number;
  product: TypeProduct;
}

export interface TypeListItemPreCheckoutResponse {
  data: TypePreCheckoutItem;
}

export interface TypePreCheckoutCartAction {
  type: string;
  payload: {
    productId: number;
    quantity: number;
    listItems: Array<number>;
    promotion: TypePromotionItem;
  };
}

export interface TypeAddPromotionForm {
  code?: string;
}

export interface TypeShippingDetail {
  shippingMethod: string;
  shippingPrice: number;
}

export interface TypeActionProductLoading {
  productId: number;
  loading: boolean;
}
