export interface TypeContactForm {
  name: string;
  email: string;
  phone: string;
  website: string;
  title: string;
  content: string;
}
