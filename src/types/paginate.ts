export interface TypePaginateResponse {
  paginate: TypePaginate;
}

interface TypePaginate {
  count: number;
  currentPage: number;
  nextPage: number;
  perPage: number;
  totalPage?: number;
}
