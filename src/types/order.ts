import { TypeCheckoutResponse } from "./checkout";
import { TypePaginateResponse } from "./paginate";
import { TypeProduct } from "./products";

export interface TypeShippingOrder {
  fullName: string;
  phone: string;
  email: string;
  province: string;
  district: string;
  address: string;
  shippingMethod: string;
  shippingPrice: number;
}

export interface TypeListItemOrder {
  productId: number;
  quantity: number;
  price: number;
  review: string | null;
  product: TypeProduct;
}

export interface TypePaymentOrder {
  paymentMethod: "card" | "money";
}

interface TypePromotionOrder {
  discount: number;
}

export interface TypeOrderItem {
  _id: string;
  total: number;
  subTotal: number;
  tax: number;
  promotion: TypePromotionOrder;
  shipping: TypeShippingOrder;
  totalQuantity: number;
  viewCartTotal: number;
  user: string;
  listItems: TypeListItemOrder[];
  payment: TypePaymentOrder;
  note: string;
  type: string;
  status: "pending" | "confirm" | "shipping" | "finished" | "cancel";
  createdAt: number;
  finishedDate: number;
  shippingDate: number;
  confirmDate: number;
}

export interface TypeOrderResponse extends TypePaginateResponse {
  data: Array<TypeOrderItem>;
}

export interface TypeOrderDetailResponse {
  data: TypeOrderItem;
}

export interface TypeCountOrderResponse {
  count: number;
}

export interface TypeActionChangeStatusOrder {
  status: string;
}

export interface TypeFormChangeStatusOrderResponse
  extends TypeCheckoutResponse {
  updateCount: number;
}
