export interface TypePaymentItem {
  _id: string;
  cvv: string;
  cardName: string;
  cardNumber: string;
  expired: string;
  type: string;
  default: boolean;
  user_id: string;
  month: string;
  year: string;
}

export interface TypePaymentResponse {
  data: Array<TypePaymentItem>;
}

export interface TypeFormEditPaymentUser {
  cardNumber?: string;
  cardName?: string;
  expired?: string;
  month?: string;
  year?: string;
  default?: boolean;
  cvv?: string;
  type?: string;
}

export interface TypePaymentDetailUserResponse {
  data: TypePaymentItem;
}
