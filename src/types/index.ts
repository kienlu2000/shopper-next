import { AxiosError, AxiosResponse } from "axios";

export interface TypeErrorResponsive<T = string> extends AxiosError<T> {
  response?: AxiosResponse<T> & {
    data: {
      message: string;
    };
  };
}

interface TypeDateError {
  error: number;
  message: string;
  error_code?: string;
  detail?: {
    username: string;
  };
}

export interface TypeError {
  response: {
    data: TypeDateError;
    status?: number;
  };
  message: string;
}
