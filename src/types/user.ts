export interface TypeProfileUser {
  _id: string;
  username: string;
  name: string;
  avatar: string;
  fb: string | null;
  birthday: string;
  gender: string;
  phone: string;
}

export interface TypeProfileUserResponse {
  data: TypeProfileUser;
}

export interface TypeRegisterForm {
  username: string;
  name: string;
  password: string;
}

export interface TypeRegisterResponse {
  data: TypeProfileUser[];
}

export interface TypeUpdateProfileForm {
  name?: string;
  phone?: string;
  avatar?: string;
  gender?: string;
  birthday?: string;
}

export interface TypeChangePasswordForm {
  currentPassword: string;
  newPassword: string;
}

export interface TypeAddressUser {
  _id: string;
  phone: string;
  email: string;
  address: string;
  province: string;
  district: string;
  fullName: string;
  default: boolean;
  user_id?: string;
}

export interface TypeAddressUserResponse {
  data: TypeAddressUser[];
}

export interface TypeAddressDetailUserResponse {
  data: TypeAddressUser;
}

export interface TypeFormEditAddressUser {
  phone?: string;
  email?: string;
  address?: string;
  province?: string;
  district?: string;
  fullName?: string;
  default?: boolean;
}

export interface TypeSendEmailResetPasswordForm {
  username: string;
}

export interface TypeChangePasswordByCodeForm {
  password: string;
  code: string;
}
