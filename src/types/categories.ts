export interface TypeCategoriesDetail {
  id: number;
  parent: null;
  parent_id: number;
  position: number;
  slug: string;
  status: number;
  title: string;
  _id: string;
}

export interface TypeCategoriesResponse {
  data: TypeCategoriesDetail[];
}

export interface TypeCategoriesDetailResponse {
  data: TypeCategoriesDetail;
}

export interface TypeCategoriesState {
  categories: TypeCategoriesDetail[];
  loading: boolean;
}
