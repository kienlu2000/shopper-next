import { CookieValueTypes } from "cookies-next";

export interface TypeLoginForm {
  username: string;
  password: string;
}

export interface TypeRefreshToken {
  refreshToken: CookieValueTypes;
}

export interface TypeToken {
  accessToken: string;
  refreshToken: string;
}

export interface TypeTokenResponse {
  data: TypeToken;
}

export interface TypeStatusResponse {
  error: number;
  message: string;
}

export type TypeRegisterForm = {
  name: string;
  username: string;
  password: string;
};
