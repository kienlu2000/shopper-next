export const MESSAGE = {
  validate: {
    required: "Please fill in this field",
    emoji: "Please do not enter emoji",
    min: "Minimum length 6 characters",
    max: "Maximum length 32 characters",
    notMatch: "Confirm password does not match",
    specialCharacter: "Do not use special characters",
    email: "Please enter the correct email format",
    notOverlap: "Do not enter the same old password",
    overlap: "Enter the same new password",
    phone: "Enter the correct phone number format",
    facebook: "Please enter your correct Facebook URL",
  },

  announcement: {
    register:
      "Account registration successful, please confirm account via your registration email",
    login: "Successfully logged in, welcome to Shopper !!",
    contactUs:
      "Contact information has been sent successfully. We will contact you as soon as possible",
  },

  wishlist: {
    removing: "Removing product from favorites list",
    removed: (name: string) =>
      `Product removed "${name}" successfully removed from the favorites list.`,
    adding: "Adding product to favorites list",
    added: (name: string) =>
      `Product added "${name}" successfully added to the favorites list.`,
  },

  cart: {
    addingCart: (name: string) => `Adding "${name}" to cart`,
    addedCart: (name: string) => `Add "${name}" successfully added to cart`,
  },

  warning: {
    notChange: "Please change the information below before clicking update",
  },

  profile: {
    updateProfileSuccess: "Updated account information successfully",
    changePasswordSuccess: "Password changed successfully",
  },

  address: {
    deleted: "Successfully deleted address book",
    changeSuccess: "Default address changed successfully",
    editSuccess: "Successfully updated registry address",
    addSuccess: "Address book added successfully",
  },

  payment: {
    deleted: "Successfully deleted payment] book",
    changeSuccess: "Default payment changed successfully",
    editSuccess: "Successfully updated registry payment",
    addSuccess: "Payment book added successfully",
  },
  order: {
    cancelOrder: "Order canceled successfully",
  },
  review: {
    addSuccess: "Add product review successfully",
  },
  sendEmailResetPassword: {
    success:
      "We have sent a password activation email to your registered email, please check and change your password.",
  },
  changePassword: {
    success: "Changed password successfully",
  },

  LOADING: "The operation is being processed",
};
