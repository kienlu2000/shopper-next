import { PATH } from "./path";

interface TypeMenuProfile {
  link: string;
  key: number;
  name: string;
}

export const LIST_MENU_PROFILE: TypeMenuProfile[] = [
  {
    link: PATH.profile.order,
    key: 1,
    name: "Sổ thanh toán",
  },
  {
    link: PATH.profile.index,
    key: 2,
    name: "Tài khoản",
  },
  {
    link: PATH.profile.collection,
    key: 3,
    name: "Sổ yêu thích",
  },
  {
    link: PATH.profile.address,
    key: 4,
    name: "Sổ địa chỉ",
  },
  {
    link: PATH.profile.payment,
    key: 5,
    name: "Sổ thanh toán",
  },
];
