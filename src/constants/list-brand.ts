interface TypeBrandProducts {
  img: string;
  like: number;
  comment: number;
  link: string;
  key: number;
}

interface TypeBrand {
  img: string;
  key: number;
}

export const LIST_BRAND_PRODUCTS: TypeBrandProducts[] = [
  {
    img: "./static/img/products/product-16.jpg",
    like: 146,
    comment: 20,
    link: "",
    key: 1,
  },
  {
    img: "./static/img/products/product-17.jpg",
    like: 543,
    link: "",
    comment: 56,
    key: 2,
  },
  {
    img: "./static/img/products/product-18.jpg",
    like: 76,
    comment: 12,
    link: "",
    key: 3,
  },
  {
    img: "./static/img/products/product-19.jpg",
    like: 2367,
    comment: 453,
    link: "",
    key: 4,
  },
  {
    img: "./static/img/products/product-20.jpg",
    like: 78,
    comment: 11,
    link: "",
    key: 5,
  },
  {
    img: "./static/img/products/product-21.jpg",
    like: 99,
    comment: 68,
    link: "",
    key: 6,
  },
];

export const LIST_BRAND: TypeBrand[] = [
  {
    img: "./static/img/brands/gray-350/mango.svg",
    key: 1,
  },
  {
    img: "./static/img/brands/gray-350/zara.svg",
    key: 2,
  },
  {
    img: "./static/img/brands/gray-350/reebok.svg",
    key: 3,
  },
  {
    img: "./static/img/brands/gray-350/asos.svg",
    key: 4,
  },
  {
    img: "./static/img/brands/gray-350/stradivarius.svg",
    key: 5,
  },
  {
    img: "./static/img/brands/gray-350/adidas.svg",
    key: 6,
  },
  {
    img: "./static/img/brands/gray-350/bershka.svg",
    key: 7,
  },
];
