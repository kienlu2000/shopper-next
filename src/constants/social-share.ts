interface TypeSocialShare {
  key: number;
  link: string;
  icon: string;
}
export const LIST_SOCIAL_SHARE: TypeSocialShare[] = [
  {
    key: 1,
    link: "https://twitter.com/",
    icon: "fab fa-twitter",
  },
  {
    key: 2,
    link: "https://www.facebook.com/",
    icon: "fab fa-facebook-f",
  },
  {
    key: 3,
    link: "https://www.pinterest.com/",
    icon: "fab fa-pinterest-p",
  },
];
