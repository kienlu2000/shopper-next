interface TypeBannerCategory {
  bgImg: StaticImageData;
  name: string;
  btnName: string;
  link: string;
  key: number;
}

import { StaticImageData } from "next/image";
import ImageBanner_1 from "public/static/img/covers/cover-1.jpg";
import ImageBanner_2 from "public/static/img/covers/cover-2.jpg";
import ImageBanner_3 from "public/static/img/covers/cover-3.jpg";

import { PATH } from "./path";

export const LIST_BANNER_CATEGORY: TypeBannerCategory[] = [
  {
    bgImg: ImageBanner_1,
    name: "Women",
    btnName: "Shop Women",
    link: PATH.products,
    key: 1,
  },
  {
    bgImg: ImageBanner_2,
    name: "Men",
    btnName: "Shop Men",
    link: PATH.products,
    key: 2,
  },
  {
    bgImg: ImageBanner_3,
    name: "Kids",
    btnName: "Shop Kids",
    link: PATH.products,
    key: 3,
  },
];
