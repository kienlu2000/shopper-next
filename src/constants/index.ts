export const ACCOUNT_DEMO = {
  username: "demo@spacedev.com",
  password: "Spacedev@123",
};

export const STATUS_ORDER = {
  pending: "Wait for confirm",
  confirm: "Waiting for delivery",
  shipping: "Delivering",
  finished: "Complete",
  cancel: "Cancelled",
};

export const PAYMENT_METHOD = {
  card: "Credit Card",
  money: "Trả tiền khi nhận hàng",
};

export const DATE_FORMAT = "MMM DD, YYYY";

export const TOKEN = "token";
export const REFRESH_TOKEN = "refreshToken";

export { default as ImageError } from "public/static/img/image-error.svg";
export { default as AvatarDefault } from "src/assets/img/avatarDefault.png";
