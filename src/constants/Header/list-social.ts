interface TypeListSocial {
  icon: string;
  link: string;
  key: number;
}

export const LIST_SOCIAL: TypeListSocial[] = [
  {
    icon: "fab fa-facebook-f",
    link: "#!",
    key: 1,
  },
  {
    icon: "fab fa-twitter",
    link: "#!",
    key: 2,
  },
  {
    icon: "fab fa-instagram",
    link: "#!",
    key: 3,
  },
  {
    icon: "fab fa-medium",
    link: "#!",
    key: 4,
  },
  {
    icon: "fab fa-youtube",
    link: "#!",
    key: 5,
  },
];
