import { PATH } from "../path";

interface TypeLink {
  key: number;
  link: string;
  name: string;
}

export const LIST_SUB_HEADER: TypeLink[] = [
  {
    key: 1,
    link: PATH.home,
    name: "Home",
  },
  {
    key: 2,
    link: PATH.products,
    name: "Products",
  },
  {
    key: 3,
    link: `${PATH.products + "/dien-thoai-may-tinh-bang/1789"}`,
    name: "Laptop",
  },
  {
    key: 4,
    link: `${PATH.products + "/laptop-thiet-bi-it/1846"}`,
    name: "Computer",
  },
  {
    key: 5,
    link: `${PATH.products + "/hang-quoc-te/17166"}`,
    name: "International Goods",
  },
];
