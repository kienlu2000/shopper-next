interface TypeLink {
  key: number;
  name: string;
  link: string;
}

import { PATH } from "../path";

export const LIST_HELPER_USER: TypeLink[] = [
  {
    key: 1,
    name: "Delivery Regulations",
    link: PATH.deliveryRegulations,
  },
  {
    key: 2,
    name: "Questions",
    link: PATH.FAQ,
  },
  {
    key: 3,
    name: "Contact Us",
    link: PATH.contactUs,
  },
];
