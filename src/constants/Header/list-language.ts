interface TypeListLanguage {
  name: string;
  img: string;
  key: number;
}

export const LIST_LANGUAGE: TypeListLanguage[] = [
  {
    name: "United States",
    img: "./static/img/flags/usa.svg",
    key: 1,
  },
  {
    name: "United States",
    img: "./static/img/flags/usa.svg",
    key: 2,
  },
  {
    name: "United States",
    img: "./static/img/flags/usa.svg",
    key: 3,
  },
];
