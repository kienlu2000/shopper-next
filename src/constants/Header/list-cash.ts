interface TypeCash {
  key: number;
  name: string;
}

export const LIST_CASH: TypeCash[] = [
  {
    name: "USD",
    key: 1,
  },
  {
    name: "EUR",
    key: 2,
  },
];
