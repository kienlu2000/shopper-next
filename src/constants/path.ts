const profile = "/profile";

export const PATH = {
  home: "/",
  deliveryRegulations: "/delivery-regulations",
  FAQ: "/FAQ",
  contactUs: "/contact-us",
  authentication: "/auth",
  products: "/products",
  myCart: "/my-cart",
  checkout: "/checkout",
  orderComplete: "/order-complete",

  profile: {
    index: profile,
    order: profile + "/order",
    collection: profile + "/collection",
    address: profile + "/address",
    newAddress: profile + "/address/new",
    editAddress: profile + "/address/edit",
    payment: profile + "/payment",
    actionPayment: "payment/new",
    editPayment: "payment/edit",
  },
};
