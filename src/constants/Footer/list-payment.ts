interface TypeFooterPayment {
  img: string;
  id: number;
}
import IconAmex from "public/static/img/payment/amex.svg";
import IconKlarna from "public/static/img/payment/klarna.svg";
import IconMaestro from "public/static/img/payment/maestro.svg";
import IconMasterCard from "public/static/img/payment/mastercard.svg";
import IconPayPal from "public/static/img/payment/paypal.svg";
import IconVisa from "public/static/img/payment/visa.svg";

export const LIST_PAYMENT: TypeFooterPayment[] = [
  {
    img: IconMasterCard,
    id: 1,
  },
  {
    img: IconVisa,
    id: 2,
  },
  {
    img: IconAmex,
    id: 3,
  },
  {
    img: IconPayPal,
    id: 4,
  },
  {
    img: IconMaestro,
    id: 5,
  },
  {
    img: IconKlarna,
    id: 6,
  },
];
