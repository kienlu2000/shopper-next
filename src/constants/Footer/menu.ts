import { PATH } from "../path";

interface TypeMenuItem {
  name: string;
  link: string;
}

interface TypeListMenu {
  title: string;
  data: TypeMenuItem[];
  key: number;
}

export const LIST_MENU_FOOTER: TypeListMenu[] = [
  {
    title: "Support",
    data: [
      {
        name: "Contact Us",
        link: PATH.contactUs,
      },
      {
        name: "FAQs",
        link: PATH.FAQ,
      },
      {
        name: "Size Guide",
        link: "",
      },
      {
        name: "Shipping & Returns",
        link: "",
      },
    ],
    key: 1,
  },
  {
    title: "Shop",
    data: [
      {
        name: "Men's Shopping",
        link: "",
      },
      {
        name: "Women's Shopping",
        link: "",
      },
      {
        name: "Kids Shopping",
        link: "",
      },
      {
        name: "Discounts",
        link: "",
      },
    ],
    key: 2,
  },
  {
    title: "Company",
    data: [
      {
        name: "Our Story",
        link: "",
      },
      {
        name: "Careers",
        link: "",
      },
      {
        name: "Terms & Conditions",
        link: "",
      },
      {
        name: "Privacy & Cookie policy",
        link: "",
      },
    ],
    key: 3,
  },
  {
    title: "Contact",
    data: [
      {
        name: "1-202-555-0105",
        link: "",
      },
      {
        name: "1-202-555-0106",
        link: "",
      },
      {
        name: "help@shopper.com",
        link: "",
      },
    ],
    key: 4,
  },
];
