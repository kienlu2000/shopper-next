interface TypePolicy {
  icon: string;
  title: string;
  describe: string;
  key: number;
}

export const LIST_POLICY: TypePolicy[] = [
  {
    title: "Free Shipping",
    icon: "fe fe-truck",
    describe: "From all orders over $100",
    key: 1,
  },
  {
    title: "Free returns",
    icon: "fe fe-repeat",
    describe: "Return money within 30 days",
    key: 2,
  },
  {
    title: "Secure shopping",
    icon: "fe fe-lock",
    describe: "You&apos;re in safe hands",
    key: 3,
  },
  {
    title: "Over 10,000 Styles",
    icon: "fe fe-tag",
    describe: "FWe have everything you need",
    key: 4,
  },
];
