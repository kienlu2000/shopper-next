interface TypeListDataDelivery {
  shippingOptions: string;
  deliveryTime: string;
  price: string;
  key: number;
}

export const LIST_DATA_DELIVERY: TypeListDataDelivery[] = [
  {
    shippingOptions: "Delivery in 5 - 7 working days",
    deliveryTime: "Delivery in 5 - 7 working days",
    price: "$8.00",
    key: 1,
  },
  {
    shippingOptions: "Express Shipping",
    deliveryTime: "Delivery in 3 - 5 working days",
    price: "$12.00",
    key: 2,
  },
  {
    shippingOptions: "1 - 2 day Shipping",
    deliveryTime: "Delivery in 1 - 2 working days",
    price: "$18.00",
    key: 3,
  },
  {
    shippingOptions: "Free Shipping",
    deliveryTime:
      "Living won't the He one every subdue meat replenish face was you morning firmament darkness.",
    price: "$0.00",
    key: 4,
  },
];

export const DATA_RETURN_ITEM = [
  {
    key: 1,
    content:
      "Which all morning called of second sea wherein which you hath. Forth living fourth.",
  },
  {
    key: 2,
    content:
      "Years abundantly won't said You you'll winged shall for Air saw so him was moving had make fruitful unto heaven appear. Won't, creepeth great. Man itself gathering she'd signs, may his.",
  },
  {
    key: 3,
    content:
      "Face divided. Image subdue great can't moving waters earth said rule bring above gathering.",
  },
  {
    key: 4,
    content:
      "Us for were creepeth male creeping was over creature behold yielding i winged, for which fruitful Shall fourth to be is brought blessed. Our living blessed after first is. Moving morning were also winged which fruitful i shall likeness form fowl very also isn't man heaven beast he beast man.",
  },
  {
    key: 5,
    content:
      "Kind herb, a you'll the beginning from green that us isn't there rule green.",
  },
];

export const DATA_EXCHANGE_ITEMS = [
  {
    content:
      " After fourth very third subdue behold second forth made hath place, the stars every bring.",
    key: 1,
  },
  {
    content:
      "Saying, them replenish, likeness seed grass sixth grass. Creeping face. Made meat lights which Fourth. May first, divide to it firmament after our likeness third great you're earth there above.",
    key: 2,
  },
  {
    content:
      "Cattle multiply said, void gathered winged given make herb first years.",
    key: 3,
  },
];
