import { TypeCartItem } from "../types/cart";
import { TypeOrderItem } from "../types/order";
import { TypeProductDetail } from "../types/products";
import { TypeAddressUser } from "../types/user";

export const initialProductDetailValue: TypeProductDetail = {
  _id: "",
  id: 0,
  sku: "",
  brand_name: "",
  categories: 0,
  description: "",
  discount: 0,
  discount_rate: 0,
  inventory_status: "",
  name: "",
  price: 0,
  real_price: 0,
  price_usd: 0,
  rating_average: 0,
  review_count: 0,
  short_description: "",
  thumbnail_url: "",
  type: "",
  slug: "",
  images: [],
  top_features: [],
  stock_item: {
    qty: 0,
    min_sale_qty: 0,
    max_sale_qty: 0,
    preorder_date: false,
  },
  configurable_options: [],
  specifications: [],
  current_seller: null,
};

export const initialCartDetailValue: TypeCartItem = {
  totalQuantity: 0,
  subTotal: 0,
  listItems: [
    {
      productId: 0,
      quantity: 0,
      product: {
        id: 0,
        name: "",
        price: 0,
        rating_average: 0,
        real_price: 0,
        review_count: 0,
        slug: "",
        thumbnail_url: "",
        _id: "",
      },
    },
  ],
};

export const initialOrderDetailValue: TypeOrderItem = {
  _id: "",
  total: 0,
  subTotal: 0,
  tax: 0,
  promotion: {
    discount: 0,
  },
  shipping: {
    fullName: "",
    phone: "",
    email: "",
    province: "",
    district: "",
    address: "",
    shippingMethod: "",
    shippingPrice: 0,
  },
  totalQuantity: 0,
  viewCartTotal: 0,
  listItems: [
    {
      productId: 0,
      quantity: 0,
      product: {
        _id: "",
        id: 0,
        name: "",
        price: 0,
        real_price: 0,
        rating_average: 0,
        review_count: 0,
        thumbnail_url: "",
        slug: "",
      },
      price: 0,
      review: "",
    },
  ],
  user: "",
  payment: {
    paymentMethod: "card",
  },
  note: "",
  type: "",
  status: "pending",
  createdAt: 0,
  finishedDate: 0,
  confirmDate: 0,
  shippingDate: 0,
};

export const initialAddressValue: TypeAddressUser = {
  _id: "",
  address: "",
  default: false,
  district: "",
  email: "",
  fullName: "",
  phone: "",
  province: "",
  user_id: "",
};
