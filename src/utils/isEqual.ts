import _ from "lodash";

type ObjectType = Record<string, any>;

export const isEqual = (
  obj1: ObjectType,
  obj2: ObjectType,
  ...field: string[]
): boolean => {
  if (field.length > 0)
    return _.isEqual(_.pick(obj1, ...field), _.pick(obj2, ...field));

  return _.isEqual(obj1, obj2);
};
