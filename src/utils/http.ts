import axios, { AxiosResponse } from "axios";
import { getCookie, setCookie } from "cookies-next";

import { authService } from "../service/auth";
import { TypeTokenResponse } from "../types/auth";

export const http = axios.create();

interface InternalAxiosRequestConfig {
  method?: string;
  url?: string;
  headers: { [key: string]: string };
}

http.interceptors.response.use(
  (response: AxiosResponse<any, any>) => {
    return response.data;
  },
  async (error) => {
    try {
      const refreshToken = getCookie("refreshToken");
      if (
        error.response?.status === 403 &&
        error.response.data.error_code === "TOKEN_EXPIRED"
      ) {
        const response: TypeTokenResponse = await authService.refreshToken({
          refreshToken: refreshToken,
        });

        setCookie("token", response.data.accessToken);

        return http(error.config);
      }
    } catch (err) {
      console.error(err);
    }

    throw error;
  },
);

http.interceptors.request.use((config: InternalAxiosRequestConfig): any => {
  const token = getCookie("token");
  if (token) {
    config.headers["Authorization"] = `Bearer ${token}`;
  }
  return config;
});
