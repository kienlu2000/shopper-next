import { message } from "antd";

import { KEYS_MESSAGE } from "../constants/keys-message";
import { TypeError } from "../types";

export const handleError = (error: TypeError) => {
  if (error.response?.data?.message || error?.message) {
    message.error({
      key: KEYS_MESSAGE.LOGIN,
      content: error?.response?.data?.message || error?.message,
    });
    throw new Error(error?.response?.data?.message || error?.message);
  }
};
