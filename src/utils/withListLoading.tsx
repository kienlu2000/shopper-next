import React from "react";

import { TypeOrderItem } from "../types/order";
import { TypeProductDetail } from "../types/products";
import { TypeReviewItem } from "../types/review";
import { array } from "./array";

interface Props {
  loading?: boolean;
  loadingCount?: number;
  wrapperClass?: string;
  Component: React.ElementType;
  ComponentLoading: React.ElementType;
  data?: TypeReviewItem[] | TypeProductDetail[] | TypeOrderItem[];
  empty?: React.ReactNode;
}

const WithListLoading = ({
  loading,
  loadingCount = 3,
  wrapperClass = "col-12",
  data = [],
  Component,
  ComponentLoading,
  empty,
}: Props) => {
  return (
    <>
      {loading
        ? array(loadingCount).map((_, i) => (
            <div key={i} className={wrapperClass}>
              <ComponentLoading loading={true} />
            </div>
          ))
        : data?.length > 0
        ? data?.map(
            (
              e: TypeReviewItem | TypeProductDetail | TypeOrderItem,
              index: number,
            ) => (
              <div key={index} className={wrapperClass}>
                <Component {...e} />
              </div>
            ),
          )
        : empty || (
            <div className="col-12">
              <p className="text-xl border p-5 text-center mb-5 max-sm:!text-lg">
                Không tìm thấy dữ liệu 😞
              </p>
            </div>
          )}
    </>
  );
};

export default WithListLoading;
