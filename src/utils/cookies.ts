import { deleteCookie, getCookie } from "cookies-next";

import { REFRESH_TOKEN, TOKEN } from "../constants";

export const getToken = () => {
  return getCookie(TOKEN);
};
export const deleteToken = () => {
  return deleteCookie(TOKEN);
};

export const deleteRefreshToken = () => {
  deleteCookie(REFRESH_TOKEN);
};
