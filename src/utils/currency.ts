export const currency = (money: number) => {
  return new Intl.NumberFormat().format(money);
};
