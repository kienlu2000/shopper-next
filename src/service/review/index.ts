import {
  TypeAddReviewForm,
  TypeAddReviewResponse,
  TypeReviewResponse,
  TypeTopReviewResponse,
} from "src/types/review";
import { http } from "src/utils/http";

import { REVIEW_API } from "..";

export const reviewService = {
  getReview(productId: number, query = ""): Promise<TypeReviewResponse> {
    return http.get(`${REVIEW_API}/${productId}${query}`);
  },
  newReview(
    productId: number,
    form: TypeAddReviewForm,
  ): Promise<TypeAddReviewResponse> {
    return http.post(`${REVIEW_API}/${productId}`, form);
  },
  getTopReview(): Promise<TypeTopReviewResponse> {
    return http.get(`${REVIEW_API}?include=product`);
  },
};
