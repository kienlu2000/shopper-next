import { TypeContactForm } from "src/types/organization";
import { http } from "src/utils/http";

import { ORGANIZATION_API } from "..";

export const organizationService = {
  contact(form: TypeContactForm) {
    return http.post(`${ORGANIZATION_API}/contact`, form);
  },
};
