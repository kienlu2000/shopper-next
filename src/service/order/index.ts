import {
  TypeActionChangeStatusOrder,
  TypeCountOrderResponse,
  TypeFormChangeStatusOrderResponse,
  TypeOrderDetailResponse,
  TypeOrderResponse,
} from "src/types/order";
import { http } from "src/utils/http";

import { ORDER_API } from "..";

export const orderService = {
  getOrder(query = ""): Promise<TypeOrderResponse> {
    return http.get(`${ORDER_API}${query}`);
  },
  getOrderDetail(id: string): Promise<TypeOrderDetailResponse> {
    return http.get(`${ORDER_API}/${id}`);
  },
  count(query = ""): Promise<TypeCountOrderResponse> {
    return http.get(`${ORDER_API}/count${query}`);
  },
  changeOrderStatus(
    id: string,
    action: TypeActionChangeStatusOrder,
  ): Promise<TypeFormChangeStatusOrderResponse> {
    return http.post(`${ORDER_API}/${id}`, action);
  },
};
