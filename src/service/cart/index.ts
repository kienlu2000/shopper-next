import {
  TypeCartItemResponse,
  TypeListItemPreCheckoutResponse,
  TypePreCheckoutDataForm,
  TypeShippingMethodResponse,
} from "src/types/cart";
import { TypeCheckoutResponse, TypeFormCheckout } from "src/types/checkout";
import { TypePromotionResponse } from "src/types/promotion";
import { http } from "src/utils/http";

import { CART_API } from "..";

export const cartService = {
  getCart(): Promise<TypeCartItemResponse> {
    return http.get(`${CART_API}`);
  },
  updateCart(productId: number, quantity: number) {
    return http.patch(`${CART_API}/${productId}`, { quantity });
  },
  removeItem(productId: number) {
    return http.delete(`${CART_API}/${productId}`);
  },
  getMethodShipping(): Promise<TypeShippingMethodResponse> {
    return http.get(`${CART_API}/shipping-method`);
  },
  preCheckout(
    data: TypePreCheckoutDataForm,
  ): Promise<TypeListItemPreCheckoutResponse> {
    return http.post(`${CART_API}/pre-checkout`, data);
  },
  getPromotion(code: string): Promise<TypePromotionResponse> {
    return http.get(`${CART_API}/promotion/${code}`);
  },
  checkout(data: TypeFormCheckout): Promise<TypeCheckoutResponse> {
    return http.post(`${CART_API}/checkout`, data);
  },
};
