import { TypeUploadFileResponse } from "src/types/file";
import { http } from "src/utils/http";

import { FILE_API } from "..";

export const fileService = {
  uploadFile(file: File): Promise<TypeUploadFileResponse> {
    const formData = new FormData();
    formData.append("file", file);
    return http.post(`${FILE_API}`, formData);
  },
};
