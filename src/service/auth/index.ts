import { TypeLoginForm, TypeRefreshToken } from "src/types/auth";
import { http } from "src/utils/http";

import { AUTH_API } from "..";

export const authService = {
  login(form: TypeLoginForm) {
    return http.post(`${AUTH_API}/login`, form);
  },
  refreshToken(data: TypeRefreshToken) {
    return http.post(`${AUTH_API}/refresh-token`, data);
  },
};
