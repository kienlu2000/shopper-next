import { TypeTokenResponse } from "src/types/auth";
import {
  TypeFormEditPaymentUser,
  TypePaymentDetailUserResponse,
  TypePaymentResponse,
} from "src/types/payment";
import {
  TypeAddressDetailUserResponse,
  TypeAddressUserResponse,
  TypeChangePasswordByCodeForm,
  TypeChangePasswordForm,
  TypeFormEditAddressUser,
  TypeProfileUserResponse,
  TypeRegisterForm,
  TypeSendEmailResetPasswordForm,
  TypeUpdateProfileForm,
} from "src/types/user";
import { http } from "src/utils/http";

import { USER_API } from "..";

export const userService = {
  signup(form: TypeRegisterForm) {
    return http.post(`${USER_API}/register`, form);
  },

  // resendEmail(data) {
  //   return http.post(`${USER_API}/resend-email`, data);
  // },

  getProfile(): Promise<TypeProfileUserResponse> {
    return http.get(`${USER_API}`);
  },

  updateInfo(form: TypeUpdateProfileForm) {
    return http.patch(`${USER_API}`, form);
  },
  sendEmailResetPassword(form: TypeSendEmailResetPasswordForm) {
    return http.post(`${USER_API}/reset-password`, form);
  },
  changePasswordByCode(
    form: TypeChangePasswordByCodeForm,
  ): Promise<TypeTokenResponse> {
    return http.post(`${USER_API}/change-password-by-code`, form);
  },

  changePassword(form: TypeChangePasswordForm) {
    return http.post(`${USER_API}/change-password`, form);
  },

  getAddressUser(query: string): Promise<TypeAddressUserResponse> {
    return http.get(`${USER_API}/address${query}`);
  },
  addAddressUser(data: TypeFormEditAddressUser) {
    return http.post(`${USER_API}/address`, data);
  },
  getAddressDetailUser(id: string): Promise<TypeAddressDetailUserResponse> {
    return http.get(`${USER_API}/address/${id}`);
  },
  editAddressUser(id: string, data: TypeFormEditAddressUser) {
    return http.patch(`${USER_API}/address/${id}`, data);
  },
  deleteAddressUser(id: string) {
    return http.delete(`${USER_API}/address/${id}`);
  },

  getPaymentUser(query = ""): Promise<TypePaymentResponse> {
    return http.get(`${USER_API}/payment${query}`);
  },
  getPaymentDetail(id: string): Promise<TypePaymentDetailUserResponse> {
    return http.get(`${USER_API}/payment/${id}`);
  },
  addPaymentUser(data: TypeFormEditPaymentUser) {
    return http.post(`${USER_API}/payment`, data);
  },
  editPaymentUser(id: string, data: TypeFormEditPaymentUser) {
    return http.patch(`${USER_API}/payment/${id}`, data);
  },
  deletePaymentUser(id: string) {
    return http.delete(`${USER_API}/payment/${id}`);
  },
};
