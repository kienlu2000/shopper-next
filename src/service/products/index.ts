import {
  TypeCategoriesDetailResponse,
  TypeCategoriesResponse,
} from "src/types/categories";
import { TypeProductDetail, TypeProductResponse } from "src/types/products";
import { http } from "src/utils/http";

import { PRODUCT_API } from "..";

export const productService = {
  getProduct(query = ""): Promise<TypeProductResponse> {
    return http.get(`${PRODUCT_API}/?${query}`);
  },
  getProductDetail(id: number) {
    return http.get<TypeProductDetail>(`${PRODUCT_API}/${id}`);
  },
  getCategories(): Promise<TypeCategoriesResponse> {
    return http.get(`${PRODUCT_API}/categories`);
  },
  getCategoriesDetail(id: number): Promise<TypeCategoriesDetailResponse> {
    return http.get(`${PRODUCT_API}/categories/${id}`);
  },
  getWishList(query = ""): Promise<TypeProductResponse> {
    return http.get(`${PRODUCT_API}/wishlist/?${query}`);
  },
  addWishList(productId: number) {
    return http.post(`${PRODUCT_API}/wishlist/${productId}`);
  },
  deleteWishList(productId: number) {
    return http.delete(`${PRODUCT_API}/wishlist/${productId}`);
  },
};
