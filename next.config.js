/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    DEVELOPMENT: "development",
    AUTH_SECRET: "adCwRZskK7oJGAAEgz8wke01e6eXgBL+e1xG+h9duNo=",
    NEXTAUTH_URL: "https://e-shopper.site/",
    USER_API: "https://course.spacedev.vn/users",
    PRODUCT_API: "https://course.spacedev.vn/product",
    AUTH_API: "https://course.spacedev.vn/authentication/v2",
    ORGANIZATION_API: "https://course.spacedev.vn/organization",
    CART_API: "https://course.spacedev.vn/cart/v2",
    FILE_API: "https://course.spacedev.vn/file",
    REVIEW_API: "https://course.spacedev.vn/review",
    ORDER_API: "https://course.spacedev.vn/order/v2",
    DOMAIN: "https://e-shopper.site/",
  },
  compiler: {
    styledComponents: true,
  },
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "firebasestorage.googleapis.com",
        pathname: "**",
      },
    ],
  },
};

module.exports = nextConfig;
